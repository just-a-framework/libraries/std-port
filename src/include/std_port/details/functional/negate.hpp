#pragma once

#include <std_port/details/utility/forward.hpp>

namespace jaf::std_port
{
    template<class T = void>
    struct negate
    {
        using result_type = T;
        using argument_type = T;

        constexpr T operator()(const T& arg) const
        {
            return -arg;
        }
    };

    template<>
    struct negate<void>
    {
        using is_transparent = void;
        
        template<class T>
        constexpr auto operator()(T&& arg) const -> decltype(-forward<T>(arg))
        {
            return -forward<T>(arg);
        }
    };
}
