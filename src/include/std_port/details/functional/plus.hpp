#pragma once

#include <std_port/details/utility/forward.hpp>

namespace jaf::std_port
{
    template<class T = void>
    struct plus
    {
        using result_type = T;
        using first_argument_type = T;
        using second_argument_type = T;

        constexpr T operator()(const T& lhs, const T& rhs) const
        {
            return lhs + rhs;
        }
    };

    template<>
    struct plus<void>
    {
        using is_transparent = void;
        
        template<class T, class U>
        constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(forward<T>(lhs) + forward<U>(rhs))
        {
            return forward<T>(lhs) + forward<U>(rhs);
        }
    };
}
