#pragma once

#include <std_port/details/type_traits/is_base_of.hpp>
#include <std_port/details/type_traits/decay.hpp>
#include <std_port/details/type_traits/is_reference_wrapper.hpp>
#include <std_port/details/type_traits/enable_if.hpp>
#include <std_port/details/type_traits/is_member_function_pointer.hpp>
#include <std_port/details/type_traits/is_member_object_pointer.hpp>
#include <std_port/details/type_traits/member_pointer_class_type.hpp>
#include <std_port/details/utility/forward.hpp>

namespace jaf::std_port::details
{
    #define _INVOKE_RETURN(...) \
        noexcept(noexcept(__VA_ARGS__)) -> decltype(__VA_ARGS__) \
        { \
            return __VA_ARGS__; \
        }

    template<
        class F,
        class A0,
        class... Args,
        class = enable_if_t<
            is_member_function_pointer_v<decay_t<F>> &&
            is_base_of_v<
                member_pointer_class_type_t<decay_t<F>>,
                decay_t<A0>
            >
        >
    >
    constexpr auto INVOKE(F&& f, A0&& a0, Args&&... args)
    _INVOKE_RETURN((::jaf::std_port::forward<A0>(a0).*f)(::jaf::std_port::forward<Args>(args)...))

    template<
        class F,
        class A0,
        class... Args,
        class = enable_if_t<
            is_member_function_pointer_v<decay_t<F>> &&
            details::is_reference_wrapper_v<decay_t<A0>>
        >
    >
    constexpr auto INVOKE(F&& f, A0&& a0, Args&&... args)
    _INVOKE_RETURN((a0.get().*f)(::jaf::std_port::forward<Args>(args)...))

    template<
        class F,
        class A0,
        class... Args,
        class = enable_if_t<
            is_member_function_pointer_v<decay_t<F>> &&
            !is_base_of_v<member_pointer_class_type_t<decay_t<F>>, decay_t<A0>> &&
            !details::is_reference_wrapper_v<decay_t<A0>>
        >
    >
    constexpr auto INVOKE(F&& f, A0&& a0, Args&&... args)
    _INVOKE_RETURN(((*::jaf::std_port::forward<A0>(a0)).*f)(::jaf::std_port::forward<Args>(args)...))

    template<
        class F,
        class A0,
        class = enable_if_t<
            is_member_object_pointer_v<decay_t<F>> &&
            is_base_of_v<member_pointer_class_type_t<decay_t<F>>, decay_t<A0>>
        >
    >
    constexpr auto INVOKE(F&& f, A0&& a0)
    _INVOKE_RETURN(::jaf::std_port::forward<A0>(a0).*f)

    template<
        class F,
        class A0,
        class = enable_if_t<
            is_member_object_pointer_v<decay_t<F>> &&
            details::is_reference_wrapper_v<decay_t<A0>>
        >
    >
    constexpr auto INVOKE(F&& f, A0&& a0)
    _INVOKE_RETURN(a0.get().*f)

    template<
        class F,
        class A0,
        class = enable_if_t<
            is_member_object_pointer_v<decay_t<F>> &&
            !is_base_of_v<member_pointer_class_type_t<decay_t<F>>, decay_t<A0>> &&
            !details::is_reference_wrapper_v<decay_t<A0>>
        >
    >
    constexpr auto INVOKE(F&& f, A0&& a0)
    _INVOKE_RETURN((*::jaf::std_port::forward<A0>(a0)).*f)

    template<class F, class... Args>
    constexpr auto INVOKE(F&& f, Args&&... args)
    _INVOKE_RETURN(::jaf::std_port::forward<F>(f)(::jaf::std_port::forward<Args>(args)...))

    #undef _INVOKE_RETURN
}
