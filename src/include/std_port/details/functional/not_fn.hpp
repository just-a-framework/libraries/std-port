#pragma once

#include <std_port/details/utility/forward.hpp>
#include <std_port/details/type_traits/invoke_result.hpp>
#include <std_port/details/type_traits/decay.hpp>
#include <std_port/details/functional/invoke.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class Func>
        struct not_fn_t
        {
            Func f;

            template<class... Args>
            auto operator()(Args&&... args) & -> decltype(!declval<invoke_result_t<decay_t<Func>&, Args...>>())
            {
                return !invoke(f, forward<Args>(args)...);
            }

            template<class... Args>
            auto operator()(Args&&... args) const& -> decltype(!declval<invoke_result_t<decay_t<Func> const&, Args...>>())
            {
                return !invoke(f, forward<Args>(args)...);
            }

            template<class... Args>
            auto operator()(Args&&... args) && -> decltype(!declval<invoke_result_t<decay_t<Func>, Args...>>())
            {
                return !invoke(move(f), forward<Args>(args)...);
            }

            template<class... Args>
            auto operator()(Args&&... args) const&& -> decltype(!declval<invoke_result_t<decay_t<Func> const, Args...>>())
            {
                return !invoke(move(f), forward<Args>(args)...);
            }
        };
    }

    template<class Func>
    details::not_fn_t<decay_t<Func>> not_fn(Func&& f)
    {
        return { forward<Func>(f) };
    }
}
