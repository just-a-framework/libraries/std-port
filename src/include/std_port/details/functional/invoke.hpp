#pragma once

#include <std_port/details/functional/invoke_.hpp>
#include <std_port/details/type_traits/invoke_result.hpp>
#include <std_port/details/type_traits/is_nothrow_invocable.hpp>

namespace jaf::std_port
{
    template<class Func, class... Args>
    invoke_result_t<Func, Args...> invoke(Func&& f, Args&&... args) noexcept(is_nothrow_invocable_v<Func, Args...>)
    {
        return details::INVOKE(forward<Func>(f), forward<Args>(args)...);
    }
}
