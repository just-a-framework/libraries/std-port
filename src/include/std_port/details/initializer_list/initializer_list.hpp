#pragma once

#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class T>
    struct initializer_list
    {
        using value_type = T;
        using reference = const T&;
        using const_reference = const T&;
        using size_type = size_t;
        using iterator = const T*;
        using const_iterator = const T*;

        constexpr initializer_list() noexcept
            : b_(nullptr)
            , s_(0)
        {
        }

        constexpr size_type size() const noexcept
        {
            return s_;
        }

        constexpr const T* begin() const noexcept
        {
            return b_;
        }

        constexpr const T* end() const noexcept
        {
            return b_ + s_;
        }
        
    private:
        constexpr initializer_list(const T* b, size_t s) noexcept
            : b_(b)
            , s_(s)
        {
        }

        const T* b_;
        size_t s_;
    };

    template<class T>
    constexpr const T* begin(initializer_list<T> il) noexcept
    {
        return il.begin();
    }

    template<class T>
    constexpr const T* end(initializer_list<T> il) noexcept
    {
        return il.end();
    }
}
