#pragma once

namespace jaf::std_port
{
    template<class T>
    constexpr T* addressof(T& arg) noexcept
    {
        return reinterpret_cast<T*>(&const_cast<char&>(reinterpret_cast<const volatile char&>(arg)));
    }

    template<class T>
    const T* addressof(const T&&) = delete;
}