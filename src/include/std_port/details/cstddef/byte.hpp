#pragma once

#include <std_port/details/type_traits/is_integral.hpp>
#include <std_port/details/type_traits/enable_if.hpp>

namespace jaf::std_port
{
    enum class byte : unsigned char
    {
    };

    template<class T>
    constexpr enable_if_t<is_integral_v<T>, T> to_integer(byte b) noexcept
    {
        return T(b);
    }

    template<class T>
    constexpr enable_if_t<is_integral_v<T>, byte&> operator<<=(byte& b, T shift) noexcept
    {
        return b = b << shift;
    }

    template<class T>
    constexpr enable_if_t<is_integral_v<T>, byte&> operator>>=(byte& b, T shift) noexcept
    {
        return b = b >> shift;
    }

    template<class T>
    constexpr enable_if_t<is_integral_v<T>, byte> operator<<(byte b, T shift) noexcept
    {
        return static_cast<byte>(static_cast<unsigned int>(b) << shift);
    }

    template<class T>
    constexpr enable_if_t<is_integral_v<T>, byte> operator>>(byte b, T shift) noexcept
    {
        return static_cast<byte>(static_cast<unsigned int>(b) >> shift);
    }

    constexpr byte operator|(byte l, byte r) noexcept
    {
        return static_cast<byte>(static_cast<unsigned int>(l) | static_cast<unsigned int>(r));
    }

    constexpr byte operator&(byte l, byte r) noexcept
    {
        return static_cast<byte>(static_cast<unsigned int>(l) & static_cast<unsigned int>(r));
    }

    constexpr byte operator^(byte l, byte r) noexcept
    {
        return static_cast<byte>(static_cast<unsigned int>(l) ^ static_cast<unsigned int>(r));
    }

    constexpr byte operator~(byte b) noexcept
    {
        return static_cast<byte>(~static_cast<unsigned int>(b));
    }

    constexpr byte& operator|=(byte& l, byte r) noexcept
    {
        return l = l | r;
    }

    constexpr byte& operator&=(byte& l, byte r) noexcept
    {
        return l = l & r;
    }

    constexpr byte& operator^=(byte& l, byte r) noexcept
    {
        return l = l ^ r;
    }
}
