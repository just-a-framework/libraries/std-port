#pragma once

namespace jaf::std_port
{
    using nullptr_t = decltype(nullptr);
}
