#pragma once

#include <std_port/details/type_traits/enable_if.hpp>
#include <std_port/details/type_traits/is_move_constructible.hpp>
#include <std_port/details/type_traits/is_move_assignable.hpp>
#include <std_port/details/type_traits/is_nothrow_move_constructible.hpp>
#include <std_port/details/type_traits/is_nothrow_move_assignable.hpp>
#include <std_port/details/utility/move.hpp>

namespace jaf::std_port
{
    template<class T>
    enable_if_t<
        is_move_constructible_v<T> &&
        is_move_assignable_v<T>
    >
    swap(T& a, T& b) noexcept(is_nothrow_move_constructible_v<T> && is_nothrow_move_assignable_v<T>)
    {
        T temp = move(a);
        a = move(b);
        b = move(temp);
    }
}
