#pragma once

#include <std_port/details/utility/swap_fwd.hpp>
#include <std_port/details/type_traits/enable_if.hpp>
#include <std_port/details/type_traits/is_swappable.hpp>
#include <std_port/details/type_traits/is_nothrow_swappable.hpp>
#include <std_port/details/algorithm/swap_ranges.hpp>
#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class T, size_t N>
    enable_if_t<is_swappable_v<T>> swap(T (&a)[N], T (&b)[N]) noexcept(is_nothrow_swappable_v<T>)
    {
        swap_ranges(a, a + N, b);
    }
}
