#pragma once

#include <std_port/details/type_traits/conditional.hpp>
#include <std_port/details/type_traits/is_nothrow_move_constructible.hpp>
#include <std_port/details/type_traits/is_copy_constructible.hpp>
#include <std_port/details/utility/move.hpp>

namespace jaf::std_port
{
    template<class T>
    constexpr conditional_t<
        !is_nothrow_move_constructible_v<T> && is_copy_constructible_v<T>,
        const T&,
        T&&
    >
    move_if_noexcept(T& x) noexcept
    {
        return move(x);
    }
}

