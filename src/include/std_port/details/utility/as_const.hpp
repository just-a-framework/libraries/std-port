#pragma once

#include <std_port/details/type_traits/add_const.hpp>

namespace jaf::std_port
{
    template <class T>
    constexpr add_const_t<T>& as_const(T& t) noexcept
    {
        return t;
    }

    template <class T>
    void as_const(const T&&) = delete;
}
