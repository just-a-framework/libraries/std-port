#pragma once

#include <std_port/details/type_traits/remove_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    constexpr remove_reference_t<T>&& move(T&& t) noexcept
    {
        return static_cast<remove_reference_t<T>&&>(t);
    }
}

