#pragma once

#include <std_port/details/utility/move.hpp>
#include <std_port/details/utility/forward.hpp>

namespace jaf::std_port
{
    template<class T, class U = T>
    T exchange(T& obj, U&& new_value)
    {
        T old_value = move(obj);
        obj = forward<U>(new_value);
        return old_value;
    }
}

