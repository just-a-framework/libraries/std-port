#pragma once

#include <std_port/details/type_traits/remove_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    constexpr T&& forward(remove_reference_t<T>& t) noexcept
    {
        return static_cast<T&&>(t);
    }

    template<class T>
    constexpr T&& forward(remove_reference_t<T>&& t) noexcept
    {
        return static_cast<T&&>(t);
    }
}

