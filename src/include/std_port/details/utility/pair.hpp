#pragma once

#include <std_port/details/type_traits/is_default_constructible.hpp>
#include <std_port/details/type_traits/is_copy_constructible.hpp>
#include <std_port/details/type_traits/is_convertible.hpp>
#include <std_port/details/type_traits/is_constructible.hpp>
#include <std_port/details/type_traits/is_copy_assignable.hpp>
#include <std_port/details/type_traits/is_nothrow_swappable.hpp>
#include <std_port/details/type_traits/is_swappable.hpp>
#include <std_port/details/type_traits/enable_if.hpp>
#include <std_port/details/type_traits/is_implicitly_default_constructible.hpp>
#include <std_port/details/type_traits/decay.hpp>
#include <std_port/details/utility/forward.hpp>
#include <std_port/details/tuple/tuple_element.hpp>
#include <std_port/details/tuple/tuple_size.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T1, class T2>
        struct pair_traits
        {
            template<class U1 = T1, class U2 = T2>
            static constexpr bool is_default_constructible() noexcept
            {
                return is_default_constructible_v<U1> && is_default_constructible_v<U2>;
            }

            template<class U1 = T1, class U2 = T2>
            static constexpr bool is_copy_constructible() noexcept
            {
                return is_copy_constructible_v<U1> && is_copy_constructible_v<U2>;
            }

            template<class U1, class U2>
            static constexpr bool is_constructible() noexcept
            {
                return is_constructible_v<T1, U1> && is_constructible_v<T2, U2>;
            }

            template<class U1, class U2>
            static constexpr bool is_implicitly_default_constructible() noexcept
            {
                return is_implicitly_default_constructible_v<U1> && is_implicitly_default_constructible_v<U2>;
            }

            template<class U1, class U2>
            static constexpr bool is_implicitly_constructible() noexcept
            {
                return is_convertible_v<U1, T1> && is_convertible_v<U2, T2>;
            }
        };
    }

    template<class T1, class T2>
    struct pair
    {      
        using first_type = T1;
        using second_type = T2;

        T1 first;
        T2 second;

        template<
            class U1 = T1,
            class U2 = T2,
            class = void,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_default_constructible<U1, U2>() &&
                !details::pair_traits<T1, T2>::template is_implicitly_default_constructible<U1, U2>()
            >
        >
        explicit constexpr pair()
            : first{}
            , second{}
        {
        }

        template<
            class U1 = T1,
            class U2 = T2,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_default_constructible<U1, U2>() &&
                details::pair_traits<T1, T2>::template is_implicitly_default_constructible<U1, U2>()
            >
        >
        constexpr pair()
            : first{}
            , second{}
        {
        }

        template<
            class U1 = T1,
            class U2 = T2,
            class = void,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_copy_constructible<U1, U2>() &&
                !details::pair_traits<T1, T2>::template is_implicitly_constructible<const U1&, const U2&>()
            >
        >
        explicit constexpr pair(const T1& x, const T2& y)
            : first{ x }
            , second{ y }
        {
        }

        template<
            class U1 = T1,
            class U2 = T2,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_copy_constructible<U1, U2>() &&
                details::pair_traits<T1, T2>::template is_implicitly_constructible<const U1&, const U2&>()
            >
        >
        constexpr pair(const T1& x, const T2& y)
            : first{ x }
            , second{ y }
        {
        }

        template<
            class U1,
            class U2,
            class = void,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_constructible<U1&&, U2&&>() &&
                !details::pair_traits<T1, T2>::template is_implicitly_constructible<U1&&, U2&&>()
            >
        >
        explicit constexpr pair(U1&& x, U2&& y)
            : first{ forward<U1>(x) }
            , second{ forward<U2>(y) }
        {
        }

        template<
            class U1,
            class U2,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_constructible<U1&&, U2&&>() &&
                details::pair_traits<T1, T2>::template is_implicitly_constructible<U1&&, U2&&>()
            >
        >
        constexpr pair(U1&& x, U2&& y)
            : first{ forward<U1>(x) }
            , second{ forward<U2>(y) }
        {
        }

        template<
            class U1,
            class U2,
            class = void,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_constructible<const U1&, const U2&>() &&
                !details::pair_traits<T1, T2>::template is_implicitly_constructible<const U1&, const U2&>()
            >
        >
        explicit constexpr pair(const pair<U1, U2>& p)
            : first{ p.first }
            , second{ p.second }
        {
        }

        template<
            class U1,
            class U2,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_constructible<const U1&, const U2&>() &&
                details::pair_traits<T1, T2>::template is_implicitly_constructible<const U1&, const U2&>()
            >
        >
        constexpr pair(const pair<U1, U2>& p)
            : first{ p.first }
            , second{ p.second }
        {
        }

        template<
            class U1,
            class U2,
            class = void,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_constructible<U1&&, U2&&>() &&
                !details::pair_traits<T1, T2>::template is_implicitly_constructible<U1&&, U2&&>()
            >
        >
        explicit constexpr pair(pair<U1, U2>&& p)
            : first{ forward<U1>(p.first) }
            , second{ forward<U2>(p.second) }
        {
        }

        template<
            class U1,
            class U2,
            class = enable_if_t<
                details::pair_traits<T1, T2>::template is_constructible<U1&&, U2&&>() &&
                details::pair_traits<T1, T2>::template is_implicitly_constructible<U1&&, U2&&>()
            >
        >
        constexpr pair(pair<U1, U2>&& p)
            : first{ forward<U1>(p.first) }
            , second{ forward<U2>(p.second) }
        {
        }

//      template<class... Args1, class... Args2>
//      pair(piecewise_construct_t, tuple<Args1...> first_args, tuple<Args2...> second_args);
        
        pair(const pair& p) = default;
        
        pair(pair&& p) = default;

        enable_if_t<is_copy_assignable_v<first_type> && is_copy_assignable_v<second_type>, pair&> operator=(const pair& other)
        {
            first = other.first;
            second = other.second;
            return *this;
        }

        template<
            class U1,
            class U2
        >
        enable_if_t<is_assignable_v<first_type&, const U1&> && is_assignable_v<second_type&, const U2&>, pair&> operator=(const pair<U1,U2>& other)
        {
            first = other.first;
            second = other.second;
            return *this;
        }

        enable_if_t<is_move_assignable_v<first_type> && is_move_assignable_v<second_type>, pair&> operator=(pair&& other)
        noexcept(is_nothrow_move_assignable_v<first_type> && is_nothrow_move_assignable_v<second_type>)
        {
            first = forward<first_type>(other.first);
            second = forward<second_type>(other.second);
            return *this;
        }

        template<
            class U1,
            class U2
        >
        enable_if_t<is_assignable_v<first_type&, U1> && is_assignable_v<second_type&, U2>, pair&> operator=(pair<U1,U2>&& other)
        {
            first = forward<U1>(other.first);
            second = forward<U2>(other.second);
            return *this;
        }

        void swap(pair& other) noexcept(is_nothrow_swappable_v<first_type> && is_nothrow_swappable_v<second_type>)
        {
            using jaf::std_port::swap;

            swap(first, other.first);
            swap(second, other.second);
        }
    };

    template<class T1, class T2>
    constexpr pair<decay_t<T1>,decay_t<T2>> make_pair(T1&& x, T2&& y)
    {
        return pair<decay_t<T1>,decay_t<T2>>(forward<T1>(x), forward<T2>(y));
    }

    template<class T1, class T2>
    constexpr bool operator==(const pair<T1,T2>& lhs, const pair<T1,T2>& rhs)
    {
        return lhs.first == rhs.first && lhs.second == rhs.second;
    }

    template<class T1, class T2>
    constexpr bool operator!=(const pair<T1,T2>& lhs, const pair<T1,T2>& rhs)
    {
        return !(lhs == rhs);
    }

    template<class T1, class T2>
    constexpr bool operator<(const pair<T1,T2>& lhs, const pair<T1,T2>& rhs)
    {
        return lhs.first < rhs.first || (!(rhs.first < lhs.first) && lhs.second < rhs.second);
    }
    
    template<class T1, class T2>
    constexpr bool operator<=(const pair<T1,T2>& lhs, const pair<T1,T2>& rhs)
    {
        return !(rhs < lhs);
    }
    
    template<class T1, class T2>
    constexpr bool operator>(const pair<T1,T2>& lhs, const pair<T1,T2>& rhs)
    {
        return rhs < lhs;
    }

    template<class T1, class T2>
    constexpr bool operator>=(const pair<T1,T2>& lhs, const pair<T1,T2>& rhs)
    {
        return !(lhs < rhs);
    }

    template<
        class T1,
        class T2,
        class = enable_if_t<is_swappable_v<T1> && is_swappable_v<T2>>
    >
    void swap(pair<T1,T2>& x, pair<T1,T2>& y) noexcept(noexcept(x.swap(y)))
    {
        x.swap(y);
    }

    namespace details
    {
        template <size_t I>
        struct get_pair;

        template<>
        struct get_pair<0>
        {
            template<class T1, class T2>
            static constexpr T1& get(pair<T1, T2>& p) noexcept
            {
                return p.first;
            }

            template<class T1, class T2>
            static constexpr const T1& get(const pair<T1, T2>& p) noexcept
            {
                return p.first;
            }

            template<class T1, class T2>
            static constexpr T1&& get(pair<T1, T2>&& p) noexcept
            {
                return forward<T1>(p.first);
            }

            template<class T1, class T2>
            static constexpr const T1&& get(const pair<T1, T2>&& p) noexcept
            {
                return forward<const T1>(p.first);
            }
        };

        template<>
        struct get_pair<1>
        {
            template<class T1, class T2>
            static constexpr T2& get(pair<T1, T2>& p) noexcept
            {
                return p.second;
            }

            template<class T1, class T2>
            static constexpr const T2& get(const pair<T1, T2>& p) noexcept
            {
                return p.second;
            }

            template<class T1, class T2>
            static constexpr T2&& get(pair<T1, T2>&& p) noexcept
            {
                return forward<T2>(p.second);
            }

            template<class T1, class T2>
            static constexpr const T2&& get(const pair<T1, T2>&& p) noexcept
            {
                return forward<const T2>(p.second);
            }
        };
    }

    template<size_t I, class T1, class T2>
    constexpr tuple_element_t<I, pair<T1,T2>>& get(pair<T1, T2>& p) noexcept
    {
        return details::get_pair<I>::get(p);
    }

    template<size_t I, class T1, class T2>
    constexpr const tuple_element_t<I, pair<T1,T2>>& get(const pair<T1,T2>& p) noexcept
    {
        return details::get_pair<I>::get(p);
    }

    template<size_t I, class T1, class T2>
    constexpr tuple_element_t<I, pair<T1,T2>>&& get(pair<T1,T2>&& p) noexcept
    {
        return details::get_pair<I>::get(move(p));
    }

    template<size_t I, class T1, class T2>
    constexpr const tuple_element_t<I, pair<T1,T2>>&& get(const pair<T1,T2>&& p) noexcept
    {
        return details::get_pair<I>::get(move(p));
    }

    template<class T, class U>
    constexpr T& get(pair<T, U>& p) noexcept
    {
        return details::get_pair<0>::get(p);
    }

    template<class T, class U>
    constexpr const T& get(const pair<T, U>& p) noexcept
    {
        return details::get_pair<0>::get(p);
    }

    template<class T, class U>
    constexpr T&& get(pair<T, U>&& p) noexcept
    {
        return details::get_pair<0>::get(move(p));
    }

    template<class T, class U>
    constexpr const T&& get(const pair<T, U>&& p) noexcept
    {
        return details::get_pair<0>::get(move(p));
    }

    template<class T, class U>
    constexpr T& get(pair<U, T>& p) noexcept
    {
        return details::get_pair<1>::get(p);
    }

    template<class T, class U>
    constexpr const T& get(const pair<U, T>& p) noexcept
    {
        return details::get_pair<1>::get(p);
    }

    template<class T, class U>
    constexpr T&& get(pair<U, T>&& p) noexcept
    {
        return details::get_pair<1>::get(move(p));
    }

    template<class T, class U>
    constexpr const T&& get(const pair<U, T>&& p) noexcept
    {
        return details::get_pair<1>::get(move(p));
    }

    template <class T1, class T2>
    struct tuple_size<pair<T1, T2>>
        : integral_constant<size_t, 2>
    {
    };

    template<size_t I, class T1, class T2>
    struct tuple_element<I, pair<T1,T2>>
    {
        static_assert(I < 2, "pair has only 2 elements!");
    };
    
    template<class T1, class T2>
    struct tuple_element<0, pair<T1,T2>>
    {
        using type = T1;
    };
    
    template<class T1, class T2>
    struct tuple_element<1, pair<T1,T2>>
    {
        using type = T2;
    };

    template<class T1, class T2>
    pair(T1, T2) -> pair<T1, T2>;
}
