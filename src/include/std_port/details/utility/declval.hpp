#pragma once

#include <std_port/details/type_traits/add_rvalue_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    add_rvalue_reference_t<T> declval() noexcept;
}
