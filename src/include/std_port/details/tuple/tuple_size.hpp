#pragma once

namespace jaf::std_port
{
    template<class T>
    struct tuple_size;

    template<class T>
    inline constexpr size_t tuple_size_v = tuple_size<T>::value;
}
