#pragma once

namespace jaf::std_port
{
    template<size_t I, class T>
    class tuple_element;

    template<size_t I, class T>
    using tuple_element_t = typename tuple_element<I, T>::type;
}
