#pragma once

#include <std_port/details/iterator/rbegin.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto crbegin(const C& c) -> decltype(rbegin(c))
    {
        return rbegin(c);
    }
}
