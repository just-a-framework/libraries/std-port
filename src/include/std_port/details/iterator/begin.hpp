#pragma once

#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto begin(C& c) -> decltype(c.begin())
    {
        return c.begin();
    }
    
    template<class C>
    constexpr auto begin(const C& c) -> decltype(c.begin())
    {
        return c.begin();
    }

    template<class T, size_t N>
    constexpr T* begin(T (&array)[N]) noexcept
    {
        return array;
    }
}
