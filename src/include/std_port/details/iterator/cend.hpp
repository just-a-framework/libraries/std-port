#pragma once

#include <std_port/details/iterator/end.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto cend(const C& c) noexcept(noexcept(end(c))) -> decltype(end(c))
    {
        return end(c);
    }
}
