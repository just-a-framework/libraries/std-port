#pragma once

#include <std_port/details/cstddef/ptrdiff_t.hpp>
#include <std_port/details/iterator/iterator_tags.hpp>

namespace jaf::std_port
{
    struct input_iterator_tag
    {
    };

    struct output_iterator_tag
    {
    };

    struct forward_iterator_tag
        : input_iterator_tag
    {
    };

    struct bidirectional_iterator_tag
        : forward_iterator_tag
    {
    };

    struct random_access_iterator_tag
        : bidirectional_iterator_tag
    {
    };
}
