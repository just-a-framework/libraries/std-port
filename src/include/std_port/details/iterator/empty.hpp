#pragma once

#include <std_port/details/initializer_list/initializer_list.hpp>
#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto empty(const C& c) -> decltype(c.empty())
    {
        return c.empty();
    }

    template <class T, size_t N>
    constexpr bool empty(const T (&array)[N]) noexcept
    {
        return false;
    }

    template <class E>
    constexpr bool empty(initializer_list<E> il) noexcept
    {
        return il.size() == 0;
    }
}
