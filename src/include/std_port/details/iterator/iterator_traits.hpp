#pragma once

#include <std_port/details/cstddef/ptrdiff_t.hpp>
#include <std_port/details/iterator/iterator_tags.hpp>

namespace jaf::std_port
{
    template<class Iter>
    struct iterator_traits
    {
        using difference_type = typename Iter::difference_type;
        using value_type = typename Iter::value_type;
        using pointer = typename Iter::pointer;
        using reference = typename Iter::reference;
        using iterator_category = typename Iter::iterator_category;
    };

    template<class T>
    struct iterator_traits<T*>
    {
        using difference_type = ptrdiff_t;
        using value_type = T;
        using pointer = T*;
        using reference = T&;
        using iterator_category = random_access_iterator_tag;
    };

    template<class T>
    struct iterator_traits<const T*>
    {
        using difference_type = ptrdiff_t;
        using value_type = T;
        using pointer = const T*;
        using reference = const T&;
        using iterator_category = random_access_iterator_tag;
    };
}
