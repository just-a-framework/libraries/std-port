#pragma once

#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto end(C& c) -> decltype(c.end())
    {
        return c.end();
    }
    
    template<class C>
    constexpr auto end(const C& c) -> decltype(c.end())
    {
        return c.end();
    }

    template<class T, size_t N>
    constexpr T* end(T (&array)[N]) noexcept
    {
        return array + N;
    }
}
