#pragma once

#include <std_port/details/iterator/rend.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto crend(const C& c) -> decltype(rend(c))
    {
        return rend(c);
    }
}
