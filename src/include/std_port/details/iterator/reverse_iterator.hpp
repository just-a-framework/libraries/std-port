#pragma once

#include <std_port/details/iterator/iterator_traits.hpp>
#include <std_port/details/memory/addressof.hpp>

namespace jaf::std_port
{
    template<class Iter>
    struct reverse_iterator
    {
        using iterator_type = Iter;
        using iterator_category = typename iterator_traits<Iter>::iterator_category;
        using value_type = typename iterator_traits<Iter>::value_type;
        using difference_type = typename iterator_traits<Iter>::difference_type;
        using pointer = typename iterator_traits<Iter>::pointer;
        using reference = typename iterator_traits<Iter>::reference;

        constexpr reverse_iterator()
            : current{}
        {
        }

        constexpr explicit reverse_iterator(iterator_type x)
            : current{x}
        {
        }

        template<class U>
        constexpr reverse_iterator(const reverse_iterator<U>& other)
            : current{other.base()}
        {
        }

        template<class U>
        constexpr reverse_iterator& operator=(const reverse_iterator<U>& other)
        {
            current = other.base();
            return *this;
        }

        constexpr iterator_type base() const
        {
            return current;
        }

        constexpr reference operator*() const
        {
            Iter tmp = current;
            return *--tmp;
        }

        constexpr pointer operator->() const
        {
            return addressof(operator*());
        }

        constexpr reference operator[](difference_type n) const
        {
            return base()[-n-1];
        }

        constexpr reverse_iterator& operator++()
        {
            --current;
            return *this;
        }

        constexpr reverse_iterator& operator--()
        {
            ++current;
            return *this;
        }
        
        constexpr reverse_iterator operator++(int)
        {
            reverse_iterator tmp(*this);
            ++current;
            return tmp;
        }
        
        constexpr reverse_iterator operator--(int)
        {
            reverse_iterator tmp(*this);
            --current;
            return tmp;
        }
        
        constexpr reverse_iterator operator+(difference_type n) const
        {
            return reverse_iterator(current - n);
        }
        
        constexpr reverse_iterator operator-(difference_type n) const
        {
            return reverse_iterator(current + n);
        }
        
        constexpr reverse_iterator& operator+=(difference_type n)
        {
            current -= n;
            return *this;
        }
        
        constexpr reverse_iterator& operator-=(difference_type n)
        {
            current += n;
            return *this;
        }
        
    protected:
        iterator_type current;
    };

    template<class Iter>
    constexpr reverse_iterator<Iter> make_reverse_iterator(Iter i)
    {
        return reverse_iterator<Iter>(i);
    }

    template<class It1, class It2>
    constexpr bool operator==(const reverse_iterator<It1>& lhs, const reverse_iterator<It2>& rhs )
    {
        return lhs.base() == rhs.base();
    }

    template<class It1, class It2>
    constexpr bool operator!=(const reverse_iterator<It1>& lhs, const reverse_iterator<It2>& rhs )
    {
        return lhs.base() != rhs.base();
    }

    template<class It1, class It2>
    constexpr bool operator<(const reverse_iterator<It1>& lhs, const reverse_iterator<It2>& rhs)
    {
        return lhs.base() > rhs.base();
    }

    template<class It1, class It2>
    constexpr bool operator<=(const reverse_iterator<It1>& lhs, const reverse_iterator<It2>& rhs)
    {
        return lhs.base() >= rhs.base();
    }

    template<class It1, class It2>
    constexpr bool operator>(const reverse_iterator<It1>& lhs, const reverse_iterator<It2>& rhs)
    {
        return lhs.base() < rhs.base();
    }

    template<class It1, class It2>
    constexpr bool operator>=(const reverse_iterator<It1>& lhs, const reverse_iterator<It2>& rhs)
    {
        return lhs.base() <= rhs.base();
    }

    template<class Iter>
    constexpr reverse_iterator<Iter> operator+(typename reverse_iterator<Iter>::difference_type n, const reverse_iterator<Iter>& it)
    {
        return reverse_iterator<Iter>(it.base() - n);
    }

    template<class It1, class It2>
    constexpr auto operator-(const reverse_iterator<It1>& lhs, const reverse_iterator<It2>& rhs) -> decltype(rhs.base() - lhs.base())
    {
        return rhs.base() - lhs.base();
    }
}
