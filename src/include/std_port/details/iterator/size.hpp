#pragma once

#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto size(const C& c) -> decltype(c.size())
    {
        return c.size();
    }

    template <class T, size_t N>
    constexpr size_t size(const T (&array)[N]) noexcept
    {
        return N;
    }
}
