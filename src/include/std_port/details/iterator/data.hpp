#pragma once

#include <std_port/details/initializer_list/initializer_list.hpp>
#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto data(C& c) -> decltype(c.data())
    {
        return c.data();
    }

    template<class C>
    constexpr auto data(const C& c) -> decltype(c.data())
    {
        return c.data();
    }

    template<class T, size_t N>
    constexpr T* data(T (&array)[N]) noexcept
    {
        return array;
    }

    template<class T>
    constexpr const T* data(initializer_list<T> il) noexcept
    {
        return il.begin();
    }
}
