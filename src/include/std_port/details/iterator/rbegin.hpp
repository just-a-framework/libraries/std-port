#pragma once

#include <std_port/details/iterator/reverse_iterator.hpp>
#include <std_port/details/initializer_list/initializer_list.hpp>
#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto rbegin(C& c) -> decltype(c.rbegin())
    {
        return c.rbegin();
    }
    
    template<class C>
    constexpr auto rbegin(const C& c) -> decltype(c.rbegin())
    {
        return c.rbegin();
    }

    template<class T, size_t N>
    constexpr reverse_iterator<T*> rbegin(T (&array)[N])
    {
        return reverse_iterator<T*>(array + N);
    }

    template<class T>
    reverse_iterator<const T*> rbegin(initializer_list<T> il)
    {
        return reverse_iterator<const T*>(il.end());
    }
}
