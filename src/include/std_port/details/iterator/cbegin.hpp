#pragma once

#include <std_port/details/iterator/begin.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto cbegin(const C& c) noexcept(noexcept(begin(c))) -> decltype(begin(c))
    {
        return begin(c);
    }
}
