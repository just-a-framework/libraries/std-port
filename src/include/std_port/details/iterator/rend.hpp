#pragma once

#include <std_port/details/iterator/reverse_iterator.hpp>
#include <std_port/details/initializer_list/initializer_list.hpp>
#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class C>
    constexpr auto rend(C& c) -> decltype(c.rend())
    {
        return c.rend();
    }
    
    template<class C>
    constexpr auto rend(const C& c) -> decltype(c.rend())
    {
        return c.rend();
    }

    template<class T, size_t N>
    constexpr reverse_iterator<T*> rend(T (&array)[N])
    {
        return reverse_iterator<T*>(array);
    }

    template<class T>
    reverse_iterator<const T*> rend(initializer_list<T> il)
    {
        return reverse_iterator<const T*>(il.begin());
    }
}
