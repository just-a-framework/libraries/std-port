#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T, class U>
    struct is_nothrow_assignable
        : bool_constant<__is_nothrow_assignable(T, U)>
    {
    };

    template<class T, class U>
    inline constexpr bool is_nothrow_assignable_v = is_nothrow_assignable<T, U>::value;
}
