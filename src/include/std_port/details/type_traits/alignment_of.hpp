#pragma once

#include <std_port/details/cstddef/size_t.hpp>
#include <std_port/details/type_traits/integral_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct alignment_of
        : integral_constant<size_t, alignof(T)>
    {
    };

    template<class T>
    inline constexpr auto alignment_of_v = alignment_of<T>::value;
}
