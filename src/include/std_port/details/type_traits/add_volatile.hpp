#pragma once

namespace jaf::std_port
{
    template<class T>
    struct add_volatile
    {
        using type = volatile T;
    };

    template<class T>
    using add_volatile_t = typename add_volatile<T>::type;
}
