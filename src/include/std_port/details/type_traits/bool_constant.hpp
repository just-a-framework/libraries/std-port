#pragma once

#include <std_port/details/type_traits/integral_constant.hpp>

namespace jaf::std_port
{
    template <bool B>
    using bool_constant = integral_constant<bool, B>;
}
