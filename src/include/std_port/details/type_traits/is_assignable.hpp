#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T, class U>
    struct is_assignable
        : bool_constant<__is_assignable(T, U)>
    {
    };

    template<class T, class U>
    inline constexpr bool is_assignable_v = is_assignable<T, U>::value;
}
