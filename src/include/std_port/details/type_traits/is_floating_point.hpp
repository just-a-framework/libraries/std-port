#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        struct is_floating_point
            : false_type
        {
        };

        template<>
        struct is_floating_point<float>
            : true_type
        {
        };

        template<>
        struct is_floating_point<double>
            : true_type
        {
        };

        template<>
        struct is_floating_point<long double>
            : true_type
        {
        };
    }

    template<class T>
    struct is_floating_point
        : details::is_floating_point<remove_cv_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_floating_point_v = is_floating_point<T>::value;
}
