#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_member_pointer.hpp>
#include <std_port/details/type_traits/is_member_function_pointer.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_member_object_pointer
        : bool_constant<is_member_pointer_v<T> && !is_member_function_pointer_v<T>>
    {
    };

    template<class T>
    inline constexpr bool is_member_object_pointer_v = is_member_object_pointer<T>::value;
}
