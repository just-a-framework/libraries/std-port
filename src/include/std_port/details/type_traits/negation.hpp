#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct negation
        : bool_constant<!T::value>
    {
    };

    template<class T>
    inline constexpr bool negation_v = negation<T>::value;
}
