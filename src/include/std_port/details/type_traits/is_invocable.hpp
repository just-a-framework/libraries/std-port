#pragma once

#include <std_port/details/functional/invoke_.hpp>
#include <std_port/details/utility/declval.hpp>
#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/false_type.hpp>

namespace jaf::std_port
{
    namespace details
    {
        struct is_invocable_tester
        {
            template<class F, class... Args, class = decltype(INVOKE(declval<F>(), declval<Args>()...))>
            static true_type test(int);

            template<class F, class... Args>
            static false_type test(...);
        };

        template<class F, class... Args>
        struct is_invocable
            : is_invocable_tester
        {
            using type = decltype(test<F, Args...>(0));
        };
    }

    template<class F, class... Args>
    struct is_invocable
        : details::is_invocable<F, Args...>::type
    {
    };

    template <class F, class... Args>
    inline constexpr bool is_invocable_v = is_invocable<F, Args...>::value;
}
