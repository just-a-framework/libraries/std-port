#pragma once

#include <std_port/details/cstddef/nullptr_t.hpp>
#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        struct is_null_pointer
            : false_type
        {
        };

        template<>
        struct is_null_pointer<nullptr_t>
            : true_type
        {
        };
    }

    template<class T>
    struct is_null_pointer
        : details::is_null_pointer<remove_cv_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_null_pointer_v = is_null_pointer<T>::value;
}
