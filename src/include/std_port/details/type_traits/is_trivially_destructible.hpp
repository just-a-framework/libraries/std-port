#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_destructible.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_trivially_destructible
        : bool_constant<is_destructible_v<T> && __has_trivial_destructor(T)>
    {
    };

    template<class T>
    inline constexpr bool is_trivially_destructible_v = is_trivially_destructible<T>::value;
}
