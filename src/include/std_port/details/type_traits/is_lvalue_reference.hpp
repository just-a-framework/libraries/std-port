#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_lvalue_reference
        : false_type
    {
    };

    template<class T>
    struct is_lvalue_reference<T&>
        : true_type
    {
    };

    template<class T>
    inline constexpr bool is_lvalue_reference_v = is_lvalue_reference<T>::value;
}
