#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_union
        : bool_constant<__is_union(T)>
    {
    };

    template<class T>
    inline constexpr bool is_union_v = is_union<T>::value;
}
