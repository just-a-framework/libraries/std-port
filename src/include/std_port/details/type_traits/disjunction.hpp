#pragma once

#include <std_port/details/type_traits/conditional.hpp>
#include <std_port/details/type_traits/false_type.hpp>

namespace jaf::std_port
{
    template<class...>
    struct disjunction
        : false_type
    {
    };

    template<class T1>
    struct disjunction<T1>
        : T1
    {
    };

    template<class T1, class... Tn>
    struct disjunction<T1, Tn...> 
        : conditional_t<bool(T1::value), T1, disjunction<Tn...>>
    {
    };

    template<class... T>
    using disjunction_t = typename disjunction<T...>::type;
}
