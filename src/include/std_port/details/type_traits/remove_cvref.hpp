#pragma once

#include <std_port/details/type_traits/remove_cv.hpp>
#include <std_port/details/type_traits/remove_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    struct remove_cvref
    {
        using type = remove_cv_t<remove_reference_t<T>>;
    };

    template<class T>
    using remove_cvref_t = typename remove_cvref<T>::type;
}
