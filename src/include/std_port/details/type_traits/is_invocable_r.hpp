#pragma once

#include <std_port/details/type_traits/invoke_result.hpp>
#include <std_port/details/type_traits/is_convertible.hpp>
#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/is_void.hpp>

namespace jaf::std_port
{
    namespace details
    {
        struct is_invocable_r_tester
        {
            template<class R, class F, class... Args, class Ret = invoke_result_t<F, Args...>>
            static bool_constant<is_void_v<R> || is_convertible_v<Ret, R>> test(int);

            template<class R, class F, class... Args>
            static false_type test(...);
        };

        template<class R, class F, class... Args>
        struct is_invocable_r
            : is_invocable_r_tester
        {
            using type = decltype(test<R, F, Args...>(0));
        };
    }

    template<class R, class F, class... Args>
    struct is_invocable_r
        : details::is_invocable_r<R, F, Args...>::type
    {
    };

    template <class R, class F, class... Args>
    inline constexpr bool is_invocable_r_v = is_invocable_r<R, F, Args...>::value;
}
