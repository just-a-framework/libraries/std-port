#pragma once

#include <std_port/details/type_traits/type_identity.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        auto try_add_lvalue_reference(int) -> type_identity<T&>;

        template<class T>
        auto try_add_lvalue_reference(...) -> type_identity<T>;
    }
    
    template<class T>
    struct add_lvalue_reference
        : decltype(details::try_add_lvalue_reference<T>(0))
    {
    };

    template<class T>
    using add_lvalue_reference_t = typename add_lvalue_reference<T>::type;
}
