#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/utility/declval.hpp>
#include <std_port/details/utility/swap_fwd.hpp>

namespace jaf::std_port
{
    namespace details
    {
        using jaf::std_port::swap;

        struct is_swappable_with_tester
        {
            template<
                class T,
                class U,
                class = decltype(swap(declval<T>(), declval<U>())),
                class = decltype(swap(declval<U>(), declval<T>()))
            >
            static true_type test(int);

            template<class, class>
            static false_type test(...);
        };

        template<class T, class U>
        struct is_swappable_with
            : is_swappable_with_tester
        {
            using type = decltype(is_swappable_with_tester::test<T, U>(0));
        };

        template<class T, class U>
        using is_swappable_with_t = typename is_swappable_with<T, U>::type;
    }

    template<class T, class U>
    struct is_swappable_with
        : details::is_swappable_with_t<T, U>
    {
    };

    template<class T, class U>
    inline constexpr bool is_swappable_with_v = is_swappable_with<T, U>::value;
}
