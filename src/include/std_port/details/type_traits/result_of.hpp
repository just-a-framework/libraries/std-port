#pragma once

#include <std_port/details/type_traits/invoke_result.hpp>

namespace jaf::std_port
{
    template<class>
    struct result_of;

    template<class F, class... Args>
    struct result_of<F(Args...)>
        : details::invoke_result<void, F, Args...>
    {
    };

    template<class T>
    using result_of_t = typename result_of<T>::type;
}
