#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        struct is_pointer
            : false_type
        {
        };

        template<class T>
        struct is_pointer<T*>
            : true_type
        {
        };
    }

    template<class T>
    struct is_pointer
        : details::is_pointer<remove_cv_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_pointer_v = is_pointer<T>::value;
}
