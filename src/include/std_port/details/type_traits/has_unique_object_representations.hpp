#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>
#include <std_port/details/type_traits/remove_all_extents.hpp>

namespace jaf::std_port
{
    template<class T>
    struct has_unique_object_representations
        : bool_constant<__has_unique_object_representations(remove_cv_t<remove_all_extents_t<T>>)>
    {
    };
    
    template<class T>
    inline constexpr bool has_unique_object_representations_v = has_unique_object_representations<T>::value;
}
