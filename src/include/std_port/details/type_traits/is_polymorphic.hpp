#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_polymorphic
        : bool_constant<__is_polymorphic(T)>
    {
    };

    template<class T>
    inline constexpr bool is_polymorphic_v = is_polymorphic<T>::value;
}
