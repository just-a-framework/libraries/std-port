#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_abstract.hpp>

namespace jaf::std_port
{
    template<class T, class U>
    struct is_convertible
        : bool_constant<__is_convertible_to(T, U)&& !is_abstract_v<U>>
    {
    };

    template<class T, class U>
    inline constexpr bool is_convertible_v = is_convertible<T, U>::value;
}
