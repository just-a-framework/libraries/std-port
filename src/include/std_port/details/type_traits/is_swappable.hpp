#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_swappable_with.hpp>
#include <std_port/details/type_traits/is_void.hpp>
#include <std_port/details/type_traits/add_lvalue_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_swappable
        : bool_constant<!is_void_v<T> && is_swappable_with_v<add_lvalue_reference_t<T>, add_lvalue_reference_t<T>>>
    {
    };

    template<class T>
    inline constexpr bool is_swappable_v = is_swappable<T>::value;
}
