#pragma once

#include <std_port/details/type_traits/is_nothrow_constructible.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_nothrow_default_constructible
        : is_nothrow_constructible<T>
    {
    };

    template<class T>
    inline constexpr bool is_nothrow_default_constructible_v = is_nothrow_default_constructible<T>::value;
}
