#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_arithmetic.hpp>
#include <std_port/details/type_traits/is_enum.hpp>
#include <std_port/details/type_traits/is_pointer.hpp>
#include <std_port/details/type_traits/is_member_pointer.hpp>
#include <std_port/details/type_traits/is_null_pointer.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_scalar
        : bool_constant<
            is_arithmetic_v<T> ||
            is_enum_v<T> ||
            is_pointer_v<T> ||
            is_member_pointer_v<T> ||
            is_null_pointer_v<T>
        >
    {
    };

    template<class T>
    inline constexpr bool is_scalar_v = is_scalar<T>::value;
}
