#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct has_virtual_destructor
        : bool_constant<__has_virtual_destructor(T)>
    {
    };

    template<class T>
    inline constexpr bool has_virtual_destructor_v = has_virtual_destructor<T>::value;
}
