#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_arithmetic.hpp>
#include <std_port/details/type_traits/is_void.hpp>
#include <std_port/details/type_traits/is_null_pointer.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_fundamental
        : bool_constant<is_arithmetic_v<T> || is_void_v<T> || is_null_pointer_v<T>>
    {
    };

    template<class T>
    inline constexpr bool is_fundamental_v = is_fundamental<T>::value;
}
