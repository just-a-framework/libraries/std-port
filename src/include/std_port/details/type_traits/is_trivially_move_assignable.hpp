#pragma once

#include <std_port/details/type_traits/is_trivially_assignable.hpp>
#include <std_port/details/type_traits/add_lvalue_reference.hpp>
#include <std_port/details/type_traits/add_rvalue_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_trivially_move_assignable
        : is_trivially_assignable<add_lvalue_reference_t<T>, add_rvalue_reference_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_trivially_move_assignable_v = is_trivially_move_assignable<T>::value;
}
