#pragma once

#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/true_type.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_const
        : false_type
    {
    };

    template<class T>
    struct is_const<const T>
        : true_type
    {
    };

    template<class T>
    inline constexpr bool is_const_v = is_const<T>::value;
}
