#pragma once

#include <std_port/details/type_traits/is_nothrow_assignable.hpp>
#include <std_port/details/type_traits/add_lvalue_reference.hpp>
#include <std_port/details/type_traits/add_const.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_nothrow_copy_assignable
        : is_nothrow_assignable<add_lvalue_reference_t<T>, add_lvalue_reference_t<add_const_t<T>>>
    {
    };

    template<class T>
    inline constexpr bool is_nothrow_copy_assignable_v = is_nothrow_copy_assignable<T>::value;
}
