#pragma once

#include <std_port/details/type_traits/remove_const.hpp>
#include <std_port/details/type_traits/remove_volatile.hpp>

namespace jaf::std_port
{
    template<class T>
    struct remove_cv
    {
        using type = remove_const_t<remove_volatile_t<T>>;
    };

    template<class T>
    using remove_cv_t = typename remove_cv<T>::type;
}
