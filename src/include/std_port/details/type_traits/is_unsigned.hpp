#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/is_arithmetic.hpp>
#include <std_port/details/type_traits/is_integral.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T, bool = is_arithmetic_v<T> && is_integral_v<T>>
        struct is_unsigned
            : bool_constant<T(0) < T(-1)>
        {
        };

        template<class T>
        struct is_unsigned<T, false>
            : false_type
        {
        };
    }

    template<class T>
    struct is_unsigned
        : details::is_unsigned<T>
    {
    };

    template<class T>
    inline constexpr bool is_unsigned_v = is_unsigned<T>::value;
}
