#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_aggregate
        : bool_constant<__is_aggregate(T)>
    {
    };

    template<class T>
    inline constexpr bool is_aggregate_v = is_aggregate<T>::value;
}
