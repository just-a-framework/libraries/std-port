#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_integral.hpp>
#include <std_port/details/type_traits/is_floating_point.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_arithmetic
        : bool_constant<is_integral_v<T> || is_floating_point_v<T>>
    {
    };

    template<class T>
    inline constexpr bool is_arithmetic_v = is_arithmetic<T>::value;
}
