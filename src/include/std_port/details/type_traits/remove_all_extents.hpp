#pragma once

#include <std_port/details/cstddef/size_t.hpp>

namespace jaf::std_port
{
    template<class T>
    struct remove_all_extents
    {
        using type = T;
    };
 
    template<class T>
    struct remove_all_extents<T[]>
    {
        using type = typename remove_all_extents<T>::type;
    };
    
    template<class T, size_t N>
    struct remove_all_extents<T[N]>
    {
        using type = typename remove_all_extents<T>::type;
    };

    template<class T>
    using remove_all_extents_t = typename remove_all_extents<T>::type;
}
