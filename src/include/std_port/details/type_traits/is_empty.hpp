#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_empty
        : bool_constant<__is_empty(T)>
    {
    };

    template<class T>
    inline constexpr bool is_empty_v = is_empty<T>::value;
}
