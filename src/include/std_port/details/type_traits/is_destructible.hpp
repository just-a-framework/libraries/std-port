#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/is_void.hpp>
#include <std_port/details/type_traits/is_unbounded_array.hpp>
#include <std_port/details/type_traits/is_function.hpp>
#include <std_port/details/type_traits/is_reference.hpp>
#include <std_port/details/type_traits/is_scalar.hpp>
#include <std_port/details/type_traits/remove_all_extents.hpp>
#include <std_port/details/utility/declval.hpp>

namespace jaf::std_port
{
    namespace details
    {
        struct is_destructible_tester
        {
            template<class T, class = decltype(declval<T&>().~T())>
            static true_type test(int);

            template<class>
            static false_type test(...);
        };

        template<class T>
        struct is_destructible_impl
            : is_destructible_tester
        {
            using type = decltype(test<T>(0));
        };

        template<class T>
        using is_destructible_impl_t = typename is_destructible_impl<T>::type;

        template<class T,
            bool = is_void_v<T> || is_unbounded_array_v<T> || is_function_v<T>,
            bool = is_reference_v<T> || is_scalar_v<T>
        >
        struct is_destructible;

        template<class T>
        struct is_destructible<T, false, false>
            : is_destructible_impl_t<remove_all_extents_t<T>>
        {
        };

        template<class T>
        struct is_destructible<T, true, false>
            : false_type
        {
        };

        template<class T>
        struct is_destructible<T, false, true>
            : true_type
        {
        };
    }

    template<class T>
    struct is_destructible
        : details::is_destructible<T>
    {
    };

    template<class T>
    inline constexpr bool is_destructible_v = is_destructible<T>::value;
}
