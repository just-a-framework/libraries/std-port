#pragma once

#include <std_port/details/utility/declval.hpp>
#include <std_port/details/type_traits/void_t.hpp>
#include <std_port/details/type_traits/decay.hpp>

namespace jaf::std_port
{
    template<class... T>
    struct common_type;

    namespace details
    {
        template <class T1, class T2, class = void>
        struct common_type_of_two
        {
        };

        template <class T1, class T2>
        struct common_type_of_two<
            T1,
            T2,
            void_t<decltype(false ? declval<T1>() : declval<T2>())>
        >
        {
            using type = decay_t<decltype(false ? declval<T1>() : declval<T2>())>;
        };

        template<class AlwaysVoid, class T1, class T2, class... Args>
        struct common_type_of_more
        {
        };
        
        template<class T1, class T2, class... Args>
        struct common_type_of_more<
            void_t<typename common_type<T1, T2>::type>,
            T1,
            T2,
            Args...
        >
            : common_type<typename common_type<T1, T2>::type, Args...>
        {
        };
    }

    template<>
    struct common_type<>
    {
    };

    template<class T>
    struct common_type<T>
        : common_type<T, T>
    {
    };

    template<class T1, class T2>
    struct common_type<T1, T2>
        : details::common_type_of_two<decay_t<T1>, decay_t<T2>>
    {
    };

    template<class T1, class T2, class... Args>
    struct common_type<T1, T2, Args...>
        : details::common_type_of_more<void, T1, T2, Args...>
    {
    };

    template<class... T>
    using common_type_t = typename common_type<T...>::type;
}
