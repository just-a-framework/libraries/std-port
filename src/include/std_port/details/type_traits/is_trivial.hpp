#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_trivially_copyable.hpp>
#include <std_port/details/type_traits/is_trivially_default_constructible.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_trivial
        : bool_constant<
            is_trivially_copyable_v<T> &&
            is_trivially_default_constructible_v<T>
        >
    {
    };

    template<class T>
    inline constexpr bool is_trivial_v = is_trivial<T>::value;
}
