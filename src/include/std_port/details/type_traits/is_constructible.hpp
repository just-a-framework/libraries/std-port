#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T, class... Args>
    struct is_constructible
        : bool_constant<__is_constructible(T, Args...)>
    {
    };

    template<class T, class... Args>
    inline constexpr bool is_constructible_v = is_constructible<T, Args...>::value;
}
