#pragma once

#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_function.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        struct is_member_function_pointer
            : false_type
        {
        };
        
        template<class T, class U>
        struct is_member_function_pointer<T U::*>
            : bool_constant<is_function_v<T>>
        {
        };
    }

    template<class T>
    struct is_member_function_pointer
        : details::is_member_function_pointer<remove_cv_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_member_function_pointer_v = is_member_function_pointer<T>::value;
}
