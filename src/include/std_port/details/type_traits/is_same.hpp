#pragma once

#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/true_type.hpp>

namespace jaf::std_port
{
    template<class T, class U>
    struct is_same
        : false_type
    {
    };
 
    template<class T>
    struct is_same<T, T>
        : true_type
    {
    };

    template<class T, class U>
    inline constexpr bool is_same_v = is_same<T, U>::value;
}
