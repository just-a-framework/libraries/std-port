#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_array
        : false_type
    {
    };
    
    template<class T>
    struct is_array<T[]>
        : true_type
    {
    };
    
    template<class T, size_t N>
    struct is_array<T[N]>
        : true_type
    {
    };

    template<class T>
    inline constexpr bool is_array_v = is_array<T>::value;
}
