#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_fundamental.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_compound
        : bool_constant<!is_fundamental_v<T>>
    {
    };

    template<class T>
    inline constexpr bool is_compound_v = is_compound<T>::value;
}
