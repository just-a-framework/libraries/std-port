#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_final
        : bool_constant<__is_final(T)>
    {
    };

    template<class T>
    inline constexpr bool is_final_v = is_final<T>::value;
}
