#pragma once

#include <std_port/details/type_traits/type_identity.hpp>
#include <std_port/details/type_traits/remove_reference.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        auto try_add_pointer(int) -> type_identity<remove_reference_t<T>*>;

        template<class T>
        auto try_add_pointer(...) -> type_identity<T>;
    }
    
    template<class T>
    struct add_pointer
        : decltype(details::try_add_pointer<T>(0))
    {
    };

    template<class T>
    using add_pointer_t = typename add_pointer<T>::type;
}
