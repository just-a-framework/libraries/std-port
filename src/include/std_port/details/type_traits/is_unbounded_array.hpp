#pragma once

#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/true_type.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_unbounded_array
        : false_type
    {
    };
    
    template<class T>
    struct is_unbounded_array<T[]>
        : true_type
    {
    };

    template<class T>
    inline constexpr bool is_unbounded_array_v = is_unbounded_array<T>::value;
}
