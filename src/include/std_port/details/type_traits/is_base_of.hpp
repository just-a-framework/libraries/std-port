#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class B, class D>
    struct is_base_of
        : bool_constant<__is_base_of(B, D)>
    {
    };

    template<class T, class U>
    inline constexpr bool is_base_of_v = is_base_of<T, U>::value;
}
