#pragma once

#include <std_port/details/type_traits/is_constructible.hpp>
#include <std_port/details/type_traits/add_lvalue_reference.hpp>
#include <std_port/details/type_traits/add_const.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_copy_constructible
        : is_constructible<T, add_lvalue_reference_t<add_const_t<T>>>
    {
    };

    template<class T>
    inline constexpr bool is_copy_constructible_v = is_copy_constructible<T>::value;
}
