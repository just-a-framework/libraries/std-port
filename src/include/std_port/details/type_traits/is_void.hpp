#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        struct is_void
            : false_type
        {
        };

        template<>
        struct is_void<void>
            : true_type
        {
        };
    }

    template<class T>
    struct is_void
        : details::is_void<remove_cv_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_void_v = is_void<T>::value;
}
