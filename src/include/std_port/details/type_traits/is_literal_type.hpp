#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_literal_type
        : bool_constant<__is_literal_type(T)>
    {
    };

    template<class T>
    inline constexpr bool is_literal_type_v = is_literal_type<T>::value;
}
