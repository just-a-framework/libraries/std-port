#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_enum
        : bool_constant<__is_enum(T)>
    {
    };

    template<class T>
    inline constexpr bool is_enum_v = is_enum<T>::value;
}
