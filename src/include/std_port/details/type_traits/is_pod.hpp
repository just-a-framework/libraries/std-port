#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_pod
        : bool_constant<__is_pod(T)>
    {
    };

    template<class T>
    inline constexpr bool is_pod_v = is_pod<T>::value;
}
