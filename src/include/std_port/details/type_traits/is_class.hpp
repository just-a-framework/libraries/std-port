#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_class
        : bool_constant<__is_class(T)>
    {
    };

    template<class T>
    inline constexpr bool is_class_v = is_class<T>::value;
}
