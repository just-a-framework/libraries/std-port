#pragma once

#include <std_port/details/cstddef/size_t.hpp>
#include <std_port/details/type_traits/integral_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct rank
        : integral_constant<size_t, 0>
    {
    };
    
    template<class T>
    struct rank<T[]>
        : integral_constant<size_t, rank<T>::value + 1>
    {
    };
    
    template<class T, size_t N>
    struct rank<T[N]>
        : integral_constant<size_t, rank<T>::value + 1>
    {
    };

    template<class T>
    inline constexpr auto rank_v = rank<T>::value;
}
