#pragma once

#include <std_port/details/cstddef/size_t.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/true_type.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_bounded_array
        : false_type
    {
    };
    
    template<class T, size_t N>
    struct is_bounded_array<T[N]>
        : true_type
    {
    };

    template<class T>
    inline constexpr bool is_bounded_array_v = is_bounded_array<T>::value;
}
