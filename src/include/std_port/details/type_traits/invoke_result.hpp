#pragma once

#include <std_port/details/functional/invoke_.hpp>
#include <std_port/details/utility/declval.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class, class, class...>
        struct invoke_result
        {
        };

        template<class F, class...Args>
        struct invoke_result<
            decltype(void(INVOKE(declval<F>(), declval<Args>()...))),
            F,
            Args...
        >
        {
            using type = decltype(INVOKE(declval<F>(), declval<Args>()...));
        };
    }

    template<class F, class... Args>
    struct invoke_result : details::invoke_result<void, F, Args...>
    {
    };

    template<class F, class... Args>
    using invoke_result_t = typename invoke_result<F, Args...>::type;   
}
