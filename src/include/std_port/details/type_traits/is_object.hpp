#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_scalar.hpp>
#include <std_port/details/type_traits/is_array.hpp>
#include <std_port/details/type_traits/is_union.hpp>
#include <std_port/details/type_traits/is_class.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_object
        : bool_constant<
            is_scalar_v<T> ||
            is_array_v<T>  ||
            is_union_v<T>  ||
            is_class_v<T>
        >
    {
    };

    template<class T>
    inline constexpr bool is_object_v = is_object<T>::value;
}
