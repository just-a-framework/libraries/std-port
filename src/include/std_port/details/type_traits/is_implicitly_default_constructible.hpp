#pragma once

#include <std_port/details/type_traits/is_default_constructible.hpp>
#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        void test_implicit_default_constructible(T);

        template<class T, class = void, bool = is_default_constructible<T>::value>
        struct is_implicitly_default_constructible
            : false_type
        {
        };

        template<class T>
        struct is_implicitly_default_constructible<T, decltype(test_implicit_default_constructible<T const&>({})), true>
            : true_type
        {
        };

        template<class T>
        struct is_implicitly_default_constructible<T, decltype(test_implicit_default_constructible<T const&>({})), false>
            : false_type
        {
        };

        template<class T>
        inline constexpr bool is_implicitly_default_constructible_v = is_implicitly_default_constructible<T>::value;
    }
}
