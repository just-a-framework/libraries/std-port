#pragma once

#include <std_port/details/type_traits/is_constructible.hpp>
#include <std_port/details/type_traits/add_rvalue_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_move_constructible
        : is_constructible<T, add_rvalue_reference_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_move_constructible_v = is_move_constructible<T>::value;
}
