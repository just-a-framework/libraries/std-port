#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/functional/reference_wrapper_fwd.hpp>

namespace jaf::std_port::details
{
    template<class T>
    struct is_reference_wrapper
        : false_type
    {
    };

    template<class T>
    struct is_reference_wrapper<reference_wrapper<T>>
        : true_type
    {
    };

    template<class T>
    inline constexpr bool is_reference_wrapper_v = is_reference_wrapper<T>::value;
}
