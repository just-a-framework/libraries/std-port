#pragma once

#include <std_port/details/type_traits/true_type.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T>
        struct is_member_pointer
            : false_type
        {
        };

        template<class T, class U>
        struct is_member_pointer<T U::*>
            : true_type
        {
        };
    }

    template<class T>
    struct is_member_pointer
        : details::is_member_pointer<remove_cv_t<T>>
    {
    };

    template<class T>
    inline constexpr bool is_member_pointer_v = is_member_pointer<T>::value;
}
