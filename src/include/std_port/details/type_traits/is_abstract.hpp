#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_abstract
        : bool_constant<__is_abstract(T)>
    {
    };

    template<class T>
    inline constexpr bool is_abstract_v = is_abstract<T>::value;
}
