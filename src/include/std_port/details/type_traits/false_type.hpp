#pragma once

#include <std_port/details/type_traits/integral_constant.hpp>

namespace jaf::std_port
{
    using false_type = integral_constant<bool, false>;
}
