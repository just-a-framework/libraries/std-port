#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/is_const.hpp>
#include <std_port/details/type_traits/is_reference.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_function
        : bool_constant<!is_const_v<const T> && !is_reference_v<T>>
    {
    };

    template<class T>
    inline constexpr bool is_function_v = is_function<T>::value;
}
