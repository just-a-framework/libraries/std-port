#pragma once

#include <std_port/details/type_traits/is_enum.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T, bool>
        struct underlying_type;

        template<class T>
        struct underlying_type<T, false>
        {
        };

        template<class T>
        struct underlying_type<T, true>
        {
            using type = __underlying_type(T);
        };
    }

    template<class T>
    struct underlying_type
        : details::underlying_type<T, is_enum_v<T>>
    {
    };

    template<class T>
    using underlying_type_t = typename underlying_type<T>::type;
}
