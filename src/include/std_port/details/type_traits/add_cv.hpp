#pragma once

#include <std_port/details/type_traits/add_const.hpp>
#include <std_port/details/type_traits/add_volatile.hpp>

namespace jaf::std_port
{
    template<class T>
    struct add_cv
    {
        using type = add_const_t<add_volatile_t<T>>;
    };

    template<class T>
    using add_cv_t = typename add_cv<T>::type;
}
