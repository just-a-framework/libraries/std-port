#pragma once

#include <std_port/details/type_traits/remove_reference.hpp>
#include <std_port/details/type_traits/conditional.hpp>
#include <std_port/details/type_traits/is_array.hpp>
#include <std_port/details/type_traits/remove_extent.hpp>
#include <std_port/details/type_traits/is_function.hpp>
#include <std_port/details/type_traits/add_pointer.hpp>
#include <std_port/details/type_traits/remove_cv.hpp>

namespace jaf::std_port
{
    template<class T>
    struct decay
    {
    private:
        using U = remove_reference_t<T>;
    public:
        using type = conditional_t< 
            is_array_v<U>,
            remove_extent_t<U>*,
            conditional_t< 
                is_function_v<U>,
                add_pointer_t<U>,
                remove_cv_t<U>
            >
        >;
    };

    template<class T>
    using decay_t = typename decay<T>::type;
}
