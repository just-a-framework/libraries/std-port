#pragma once

namespace jaf::std_port::details
{
    template<class>
    struct member_pointer_class_type
    {
    };

    template<class R, class CT>
    struct member_pointer_class_type<R CT::*>
    {
        using type = CT;
    };

    template<class T>
    using member_pointer_class_type_t = typename member_pointer_class_type<T>::type;
}
