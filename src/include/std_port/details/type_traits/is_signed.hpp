#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>
#include <std_port/details/type_traits/false_type.hpp>
#include <std_port/details/type_traits/is_arithmetic.hpp>
#include <std_port/details/type_traits/is_integral.hpp>

namespace jaf::std_port
{
    namespace details
    {
        template<class T, bool = is_arithmetic_v<T> && is_integral_v<T>>
        struct is_signed
            : bool_constant<T(-1) < T(0)>
        {
        };

        template<class T>
        struct is_signed<T, false>
            : false_type
        {
        };
    }

    template<class T>
    struct is_signed
        : details::is_signed<T>
    {
    };

    template<class T>
    inline constexpr bool is_signed_v = is_signed<T>::value;
}
