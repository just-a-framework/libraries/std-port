#pragma once

#include <std_port/details/cstddef/size_t.hpp>
#include <std_port/details/type_traits/integral_constant.hpp>

namespace jaf::std_port
{
    template<class T, unsigned N = 0>
    struct extent
        : integral_constant<size_t, 0>
    {
    };
    
    template<class T>
    struct extent<T[], 0>
        : integral_constant<size_t, 0>
    {
    };
    
    template<class T, unsigned N>
    struct extent<T[], N>
        : extent<T, N - 1>
    {
    };
    
    template<class T, size_t I>
    struct extent<T[I], 0>
        : integral_constant<size_t, I>
    {
    };
    
    template<class T, size_t I, unsigned N>
    struct extent<T[I], N>
        : extent<T, N - 1>
    {
    };

    template<class T, unsigned N = 0>
    inline constexpr auto extent_v = extent<T, N>::value;
}
