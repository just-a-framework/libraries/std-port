#pragma once

namespace jaf::std_port
{
    template<class T>
    struct add_const
    {
        using type = const T;
    };

    template<class T>
    using add_const_t = typename add_const<T>::type;
}
