#pragma once

#include <std_port/details/type_traits/is_constructible.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_default_constructible
        : is_constructible<T>
    {
    };

    template<class T>
    inline constexpr bool is_default_constructible_v = is_default_constructible<T>::value;
}
