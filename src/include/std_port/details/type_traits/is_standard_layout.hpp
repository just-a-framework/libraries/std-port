#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_standard_layout
        : bool_constant<__is_standard_layout(T)>
    {
    };

    template<class T>
    inline constexpr bool is_standard_layout_v = is_standard_layout<T>::value;
}
