#pragma once

#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port
{
    template<class T>
    struct is_trivially_copyable
        : bool_constant<__is_trivially_copyable(T)>
    {
    };

    template<class T>
    inline constexpr bool is_trivially_copyable_v = is_trivially_copyable<T>::value;
}
