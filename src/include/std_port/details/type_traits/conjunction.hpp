#pragma once

#include <std_port/details/type_traits/conditional.hpp>
#include <std_port/details/type_traits/true_type.hpp>

namespace jaf::std_port
{
    template<class...>
    struct conjunction
        : true_type
    {
    };

    template<class T1>
    struct conjunction<T1>
        : T1
    {
    };

    template<class T1, class... Tn>
    struct conjunction<T1, Tn...> 
        : conditional_t<bool(T1::value), conjunction<Tn...>, T1>
    {
    };

    template<class... T>
    using conjunction_t = typename conjunction<T...>::type;
}
