#pragma once

namespace jaf::std_port
{
    template<class...>
    using void_t = void;
}
