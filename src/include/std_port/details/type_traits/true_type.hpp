#pragma once

#include <std_port/details/type_traits/integral_constant.hpp>

namespace jaf::std_port
{
    using true_type = integral_constant<bool, true>;
}
