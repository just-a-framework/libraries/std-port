#pragma once

#include <std_port/details/algorithm/iter_swap.hpp>

namespace jaf::std_port
{
    template<class ForwardIt1, class ForwardIt2>
    ForwardIt2 swap_ranges(ForwardIt1 f1, ForwardIt1 l1, ForwardIt2 f2)
    {
        while (f1 != l1)
        {
            iter_swap(f1++, f2++);
        }
        return f2;
    }
}
