#pragma once

#include <std_port/details/utility/swap_fwd.hpp>

namespace jaf::std_port
{
    template<class ForwardIt1, class ForwardIt2>
    void iter_swap(ForwardIt1 a, ForwardIt2 b)
    {
        using jaf::std_port::swap;
        swap(*a, *b);
    }
}
