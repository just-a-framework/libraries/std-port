function(__gmock_append_debugs _endvar _library)
    if(${_library} AND ${_library}_DEBUG)
        set(_output optimized ${${_library}} debug ${${_library}_DEBUG})
    else()
        set(_output ${${_library}})
    endif()
    set(${_endvar} ${_output} PARENT_SCOPE)
endfunction()

function(__gmock_find_library _name)
    find_library(${_name}
        NAMES ${ARGN}
        HINTS
            ENV GMOCK_ROOT
            ${GMOCK_ROOT}
        PATH_SUFFIXES ${_gmock_libpath_suffixes}
    )
    mark_as_advanced(${_name})
endfunction()

function(__gmock_find_library_configuration _name _lib _cfg_suffix)
    set(_libs ${_lib})
    if(MSVC AND GMOCK_MSVC_SEARCH STREQUAL "MD")
        # The provided /MD project files for Google Mock add -md suffixes to the
        # library names.
        list(INSERT _libs 0 ${_lib}-md)
    endif()
    list(TRANSFORM _libs APPEND "${_cfg_suffix}")

    __gmock_find_library(${_name} ${_libs})
endfunction()

include(SelectLibraryConfigurations)
function(__gmock_find_and_select_library_configurations _basename _lib)
    __gmock_find_library_configuration(${_basename}_LIBRARY_RELEASE ${_lib} "")
    __gmock_find_library_configuration(${_basename}_LIBRARY_DEBUG   ${_lib} "d")

    select_library_configurations(${_basename})
    set(${_basename}_LIBRARY ${${_basename}_LIBRARY} PARENT_SCOPE)
endfunction()

macro(__gmock_determine_windows_library_type _var)
    if(EXISTS "${${_var}}")
        file(TO_NATIVE_PATH "${${_var}}" _lib_path)
        get_filename_component(_name "${${_var}}" NAME_WE)
        file(STRINGS "${${_var}}" _match REGEX "${_name}\\.dll" LIMIT_COUNT 1)
        if(NOT _match STREQUAL "")
            set(${_var}_TYPE SHARED PARENT_SCOPE)
        else()
            set(${_var}_TYPE UNKNOWN PARENT_SCOPE)
        endif()
        return()
    endif()
endmacro()

function(__gmock_determine_library_type _var)
    if(WIN32)
        # For now, at least, only Windows really needs to know the library type
        __gmock_determine_windows_library_type(${_var})
        __gmock_determine_windows_library_type(${_var}_RELEASE)
        __gmock_determine_windows_library_type(${_var}_DEBUG)
    endif()
    # If we get here, no determination was made from the above checks
    set(${_var}_TYPE UNKNOWN PARENT_SCOPE)
endfunction()

function(__gmock_import_library _target _var _config)
    if(_config)
        set(_config_suffix "_${_config}")
    else()
        set(_config_suffix "")
    endif()

    set(_lib "${${_var}${_config_suffix}}")
    if(EXISTS "${_lib}")
        if(_config)
            set_property(TARGET ${_target} APPEND PROPERTY
                IMPORTED_CONFIGURATIONS ${_config})
        endif()
        set_target_properties(${_target} PROPERTIES
            IMPORTED_LINK_INTERFACE_LANGUAGES${_config_suffix} "CXX")
        if(WIN32 AND ${_var}_TYPE STREQUAL SHARED)
            set_target_properties(${_target} PROPERTIES
                IMPORTED_IMPLIB${_config_suffix} "${_lib}")
        else()
            set_target_properties(${_target} PROPERTIES
                IMPORTED_LOCATION${_config_suffix} "${_lib}")
        endif()
    endif()
endfunction()

#

if(NOT DEFINED GMOCK_MSVC_SEARCH)
    set(GMOCK_MSVC_SEARCH MD)
endif()

set(_gmock_libpath_suffixes lib)
if(MSVC)
    if(GMOCK_MSVC_SEARCH STREQUAL "MD")
        list(APPEND _gmock_libpath_suffixes
            msvc/gmock-md/Debug
            msvc/gmock-md/Release
            msvc/x64/Debug
            msvc/x64/Release
            msvc/2010/gmock-md/Win32-Debug
            msvc/2010/gmock-md/Win32-Release
            msvc/2010/gmock-md/x64-Debug
            msvc/2010/gmock-md/x64-Release
            )
    elseif(GMOCK_MSVC_SEARCH STREQUAL "MT")
        list(APPEND _gmock_libpath_suffixes
            msvc/gmock/Debug
            msvc/gmock/Release
            msvc/x64/Debug
            msvc/x64/Release
            msvc/2010/gmock/Win32-Debug
            msvc/2010/gmock/Win32-Release
            msvc/2010/gmock/x64-Debug
            msvc/2010/gmock/x64-Release
            )
    endif()
endif()


find_path(GMOCK_INCLUDE_DIR gmock/gmock.h
    HINTS
        $ENV{GMOCK_ROOT}/include
        ${GMOCK_ROOT}/include
)
mark_as_advanced(GMOCK_INCLUDE_DIR)

# Allow GMOCK_LIBRARY to be set manually, as the
# locations of the gmock library.
if(NOT GMOCK_LIBRARY)
    __gmock_find_and_select_library_configurations(GMOCK gmock)
endif()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GMock DEFAULT_MSG GMOCK_LIBRARY GMOCK_INCLUDE_DIR)

if(GMOCK_FOUND)
    set(GMOCK_INCLUDE_DIRS ${GMOCK_INCLUDE_DIR})
    __gmock_append_debugs(GMOCK_LIBRARIES      GMOCK_LIBRARY)
    set(GMOCK_BOTH_LIBRARIES ${GMOCK_LIBRARIES})

    find_package(Threads QUIET)

    if(NOT TARGET GMock::GMock)
        __gmock_determine_library_type(GMOCK_LIBRARY)
        add_library(GMock::GMock ${GMOCK_LIBRARY_TYPE} IMPORTED)
        if(TARGET Threads::Threads)
            set_target_properties(GMock::GMock PROPERTIES
                INTERFACE_LINK_LIBRARIES Threads::Threads)
        endif()
        if(GMOCK_LIBRARY_TYPE STREQUAL "SHARED")
            set_target_properties(GMock::GMock PROPERTIES
                INTERFACE_COMPILE_DEFINITIONS "GMOCK_LINKED_AS_SHARED_LIBRARY=1")
        endif()
        if(GMOCK_INCLUDE_DIRS)
            set_target_properties(GMock::GMock PROPERTIES
                INTERFACE_INCLUDE_DIRECTORIES "${GMOCK_INCLUDE_DIRS}")
        endif()
        __gmock_import_library(GMock::GMock GMOCK_LIBRARY "")
        __gmock_import_library(GMock::GMock GMOCK_LIBRARY "RELEASE")
        __gmock_import_library(GMock::GMock GMOCK_LIBRARY "DEBUG")
    endif()

    # Add targets mapping the same library names as defined in
    # GMock's CMake package config.
    if(NOT TARGET GTest::gmock)
        add_library(GTest::gmock INTERFACE IMPORTED)
        target_link_libraries(GTest::gmock INTERFACE GMock::GMock)
    endif()
endif()
