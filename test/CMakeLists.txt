cmake_minimum_required(VERSION 3.16)

enable_testing()

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)

set(target_name "std_port_test")

project(${target_name}
    LANGUAGES CXX
)

conan_basic_setup()

find_package(StdPort CONFIG REQUIRED)
find_package(GTest REQUIRED)
find_package(GMock REQUIRED)

include(GoogleTest)

set(HEADER_FILES
    details/iterator/vector.hpp
    details/type_traits/same_type_fixture.hpp
    details/type_traits/value_fixture.hpp
)
    
set(SOURCE_FILES
    details/functional/bit_and.cpp
    details/functional/bit_not.cpp
    details/functional/bit_or.cpp
    details/functional/bit_xor.cpp
    details/functional/divides.cpp
    details/functional/equal_to.cpp
    details/functional/greater.cpp
    details/functional/greater_equal.cpp
    details/functional/invoke_.cpp
    details/functional/less.cpp
    details/functional/less_equal.cpp
    details/functional/logical_and.cpp
    details/functional/logical_not.cpp
    details/functional/logical_or.cpp
    details/functional/minus.cpp
    details/functional/modulus.cpp
    details/functional/multiplies.cpp
    details/functional/negate.cpp
    details/functional/not_equal_to.cpp
    details/functional/not_fn.cpp
    details/functional/plus.cpp
    details/initializer_list/initializer_list.cpp
    details/iterator/begin.cpp
    details/iterator/cbegin.cpp
    details/iterator/cend.cpp
    details/iterator/crbegin.cpp
    details/iterator/crend.cpp
    details/iterator/data.cpp
    details/iterator/empty.cpp
    details/iterator/end.cpp
    details/iterator/rbegin.cpp
    details/iterator/rend.cpp
    details/iterator/size.cpp
    details/memory/addressof.cpp
    details/type_traits/add_const.cpp
    details/type_traits/add_cv.cpp
    details/type_traits/add_lvalue_reference.cpp
    details/type_traits/add_pointer.cpp
    details/type_traits/add_rvalue_reference.cpp
    details/type_traits/add_volatile.cpp
    details/type_traits/alignment_of.cpp
    details/type_traits/bool_constant.cpp
    details/type_traits/common_type.cpp
    details/type_traits/conditional.cpp
    details/type_traits/conjunction.cpp
    details/type_traits/decay.cpp
    details/type_traits/disjunction.cpp
    details/type_traits/enable_if.cpp
    details/type_traits/extent.cpp
    details/type_traits/false_type.cpp
    details/type_traits/has_unique_object_representations.cpp
    details/type_traits/has_virtual_destructor.cpp
    details/type_traits/integral_constant.cpp
    details/type_traits/invoke_result.cpp
    details/type_traits/is_abstract.cpp
    details/type_traits/is_aggregate.cpp
    details/type_traits/is_arithmetic.cpp
    details/type_traits/is_array.cpp
    details/type_traits/is_assignable.cpp
    details/type_traits/is_base_of.cpp
    details/type_traits/is_bounded_array.cpp
    details/type_traits/is_class.cpp
    details/type_traits/is_compound.cpp
    details/type_traits/is_const.cpp
    details/type_traits/is_constructible.cpp
    details/type_traits/is_convertible.cpp
    details/type_traits/is_copy_assignable.cpp
    details/type_traits/is_copy_constructible.cpp
    details/type_traits/is_default_constructible.cpp
    details/type_traits/is_destructible.cpp
    details/type_traits/is_empty.cpp
    details/type_traits/is_enum.cpp
    details/type_traits/is_final.cpp
    details/type_traits/is_floating_point.cpp
    details/type_traits/is_function.cpp
    details/type_traits/is_fundamental.cpp
    details/type_traits/is_integral.cpp
    details/type_traits/is_invocable.cpp
    details/type_traits/is_invocable_r.cpp
    details/type_traits/is_literal_type.cpp
    details/type_traits/is_lvalue_reference.cpp
    details/type_traits/is_member_function_pointer.cpp
    details/type_traits/is_member_object_pointer.cpp
    details/type_traits/is_member_pointer.cpp
    details/type_traits/is_move_assignable.cpp
    details/type_traits/is_move_constructible.cpp
    details/type_traits/is_nothrow_assignable.cpp
    details/type_traits/is_nothrow_constructible.cpp
    details/type_traits/is_nothrow_copy_assignable.cpp
    details/type_traits/is_nothrow_copy_constructible.cpp
    details/type_traits/is_nothrow_default_constructible.cpp
    details/type_traits/is_nothrow_destructible.cpp
    details/type_traits/is_nothrow_invocable.cpp
    details/type_traits/is_nothrow_invocable_r.cpp
    details/type_traits/is_nothrow_move_assignable.cpp
    details/type_traits/is_nothrow_move_constructible.cpp
    details/type_traits/is_nothrow_swappable.cpp
    details/type_traits/is_nothrow_swappable_with.cpp
    details/type_traits/is_null_pointer.cpp
    details/type_traits/is_object.cpp
    details/type_traits/is_pod.cpp
    details/type_traits/is_pointer.cpp
    details/type_traits/is_polymorphic.cpp
    details/type_traits/is_reference.cpp
    details/type_traits/is_rvalue_reference.cpp
    details/type_traits/is_same.cpp
    details/type_traits/is_scalar.cpp
    details/type_traits/is_signed.cpp
    details/type_traits/is_standard_layout.cpp
    details/type_traits/is_swappable.cpp
    details/type_traits/is_swappable_with.cpp
    details/type_traits/is_trivial.cpp
    details/type_traits/is_trivially_assignable.cpp
    details/type_traits/is_trivially_constructible.cpp
    details/type_traits/is_trivially_copy_assignable.cpp
    details/type_traits/is_trivially_copy_constructible.cpp
    details/type_traits/is_trivially_copyable.cpp
    details/type_traits/is_trivially_default_constructible.cpp
    details/type_traits/is_trivially_destructible.cpp
    details/type_traits/is_trivially_move_assignable.cpp
    details/type_traits/is_trivially_move_constructible.cpp
    details/type_traits/is_unbounded_array.cpp
    details/type_traits/is_union.cpp
    details/type_traits/is_unsigned.cpp
    details/type_traits/is_void.cpp
    details/type_traits/is_volatile.cpp
    details/type_traits/negation.cpp
    details/type_traits/rank.cpp
    details/type_traits/remove_all_extents.cpp
    details/type_traits/remove_const.cpp
    details/type_traits/remove_cv.cpp
    details/type_traits/remove_cvref.cpp
    details/type_traits/remove_extent.cpp
    details/type_traits/remove_pointer.cpp
    details/type_traits/remove_reference.cpp
    details/type_traits/remove_volatile.cpp
    details/type_traits/result_of.cpp
    details/type_traits/true_type.cpp
    details/type_traits/underlying_type.cpp
    details/type_traits/void_t.cpp
    details/utility/pair.cpp
)

add_executable(${target_name}
	${SOURCE_FILES}
	${HEADER_FILES}
)

target_link_libraries(${target_name}
PUBLIC
	jaf::std_port
	GTest::GTest
    GMock::GMock
	GTest::Main
)

target_include_directories(${target_name}
PRIVATE
	${CMAKE_CURRENT_SOURCE_DIR}
)

target_precompile_headers(${target_name}
PRIVATE
	pch.hpp
)

gtest_discover_tests(
	${target_name}
)
