#include <std_port/details/type_traits/is_destructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_destructible :
        value_fixture<
            ::jaf::std_port::is_destructible
        >
    {
    };

    struct generated_foo
    {
    };

    struct defaulted_foo
    {
        ~defaulted_foo() = default;
    };

    struct virtual_foo
    {
        virtual ~virtual_foo() = default;
    };

    struct defined_foo
    {
        ~defined_foo()
        {
        }
    };

    struct deleted_foo
    {
        ~deleted_foo() = delete;
    };

    TEST_F(is_destructible, generated)
    {
        validate<true, generated_foo>();
    }

    TEST_F(is_destructible, default)
    {
        validate<true, defaulted_foo>();
    }

    TEST_F(is_destructible, defined)
    {
        validate<true, defined_foo>();
    }

    TEST_F(is_destructible, virtual)
    {
        validate<true, virtual_foo>();
    }

    TEST_F(is_destructible, deleted)
    {
        validate<false, deleted_foo>();
    }
}
