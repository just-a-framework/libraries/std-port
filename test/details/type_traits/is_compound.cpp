#include <std_port/details/type_traits/is_compound.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_compound :
        value_fixture<
            ::jaf::std_port::is_compound
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_compound, class_value)
    {
        validate<true, foo>();
    }

    TEST_F(is_compound, class_reference)
    {
        validate<true, foo&>();
    }

    TEST_F(is_compound, int_value)
    {
        validate<false, int>();
    }

    TEST_F(is_compound, const_int_value)
    {
        validate<false, const int>();
    }

    TEST_F(is_compound, int_reference)
    {
        validate<true, int&>();
    }

    TEST_F(is_compound, float_value)
    {
        validate<false, float>();
    }

    TEST_F(is_compound, const_float_value)
    {
        validate<false, const float>();
    }

    TEST_F(is_compound, float_reference)
    {
        validate<true, float&>();
    }

    TEST_F(is_compound, void)
    {
        validate<false, void>();
    }

    TEST_F(is_compound, null_pointer)
    {
        validate<false, nullptr_t>();
    }
}
