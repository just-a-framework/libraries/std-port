#include <std_port/details/type_traits/is_nothrow_invocable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_invocable :
        value_fixture<
            ::jaf::std_port::is_nothrow_invocable
        >
    {
    };

    int f(bool) noexcept;

    struct foo
    {
    };

    int g(foo) noexcept;
    
    foo g(int);

    TEST_F(is_nothrow_invocable, callable)
    {
        validate<true, decltype(static_cast<int(*)(foo) noexcept>(&g)), foo>();

        validate<true, decltype(&f), bool>();
        validate<false, decltype(static_cast<foo(*)(int)>(&g)), int>();
    }

    TEST_F(is_nothrow_invocable, wrong_number_args)
    {
        validate<false, decltype(&f)>();
        validate<false, decltype(&f), bool, foo>();

        validate<false, decltype(&f)>();
        validate<false, decltype(&f), bool, foo>();
    }

    TEST_F(is_nothrow_invocable, wrong_arg_type)
    {
        validate<false, decltype(&f), foo>();
        
        validate<false, decltype(&f), foo>();
    }
}
