#include <std_port/details/type_traits/conditional.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct conditional
        : ::testing::Test
    {
        template<class T, bool B, class U, class Y>
        void validate()
        {
            static_assert(std::is_same_v<T, typename ::jaf::std_port::conditional<B, U, Y>::type>);
            EXPECT_TRUE((std::is_same_v<T, typename ::jaf::std_port::conditional<B, U, Y>::type>));

            static_assert(std::is_same_v<T, ::jaf::std_port::conditional_t<B, U, Y>>);
            EXPECT_TRUE((std::is_same_v<T, ::jaf::std_port::conditional_t<B, U, Y>>));
        }
    };

    struct foo
    {
    };

    TEST_F(conditional, true)
    {
        validate<foo, true, foo, int>();
        validate<int, true, int, foo>();
    }

    TEST_F(conditional, false)
    {
        validate<int, false, foo, int>();
        validate<foo, false, int, foo>();
    }
}
