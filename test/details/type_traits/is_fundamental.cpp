#include <std_port/details/type_traits/is_fundamental.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_fundamental :
        value_fixture<
            ::jaf::std_port::is_fundamental
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_fundamental, class_value)
    {
        validate<false, foo>();
    }

    TEST_F(is_fundamental, class_reference)
    {
        validate<false, foo&>();
    }

    TEST_F(is_fundamental, int_value)
    {
        validate<true, int>();
    }

    TEST_F(is_fundamental, const_int_value)
    {
        validate<true, const int>();
    }

    TEST_F(is_fundamental, int_reference)
    {
        validate<false, int&>();
    }

    TEST_F(is_fundamental, float_value)
    {
        validate<true, float>();
    }

    TEST_F(is_fundamental, const_float_value)
    {
        validate<true, const float>();
    }

    TEST_F(is_fundamental, float_reference)
    {
        validate<false, float&>();
    }

    TEST_F(is_fundamental, void)
    {
        validate<true, void>();
    }

    TEST_F(is_fundamental, null_pointer)
    {
        validate<true, nullptr_t>();
    }
}
