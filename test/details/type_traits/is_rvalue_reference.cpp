#include <std_port/details/type_traits/is_rvalue_reference.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_rvalue_reference :
        value_fixture<
            ::jaf::std_port::is_rvalue_reference
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_rvalue_reference, class_value)
    {
        validate<false, foo>();
    }

    TEST_F(is_rvalue_reference, class_lvalue)
    {
        validate<false, foo&>();
    }

    TEST_F(is_rvalue_reference, class_rvalue)
    {
        validate<true, foo&&>();
    }

    TEST_F(is_rvalue_reference, literal_value)
    {
        validate<false, int>();
    }

    TEST_F(is_rvalue_reference, literal_lvalue)
    {
        validate<false, int&>();
    }

    TEST_F(is_rvalue_reference, literal_rvalue)
    {
        validate<true, int&&>();
    }
}
