#include <std_port/details/type_traits/add_rvalue_reference.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct add_rvalue_reference :
        same_type_fixture<
            ::jaf::std_port::add_rvalue_reference,
            ::jaf::std_port::add_rvalue_reference_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(add_rvalue_reference, add_rvalue_reference_class)
    {
        validate<foo&&, foo>();
    }

    TEST_F(add_rvalue_reference, do_nothing_class)
    {
        validate<foo&&, foo&&>();
    }

    TEST_F(add_rvalue_reference, add_rvalue_reference_literal)
    {
        validate<int&&, int>();
    }

    TEST_F(add_rvalue_reference, do_nothing_literal)
    {
        validate<int&&, int&&>();
    }
}
