#pragma once

namespace jaf::std_port::test
{
    template<template<typename...> typename Type, template<typename...> typename Alias>
    struct same_type_fixture : ::testing::Test
    {
        template<class T, class... U>
        void validate()
        {
            static_assert(std::is_same_v<T, typename Type<U...>::type>);
            EXPECT_TRUE((std::is_same_v<T, typename Type<U...>::type>));

            static_assert(std::is_same_v<T, Alias<U...>>);
            EXPECT_TRUE((std::is_same_v<T, Alias<U...>>));
        }
    };
}
