#include <std_port/details/type_traits/is_pointer.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_pointer :
        value_fixture<
            ::jaf::std_port::is_pointer
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_pointer, class_pointer)
    {
        validate<true, foo*>();
    }

    TEST_F(is_pointer, class_const_pointer)
    {
        validate<true, foo * const>();
    }

    TEST_F(is_pointer, class_volatile_pointer)
    {
        validate<true, foo * volatile>();
    }

    TEST_F(is_pointer, class_value)
    {
        validate<false, foo>();
    }

    TEST_F(is_pointer, class_lvalue)
    {
        validate<false, foo&>();
    }

    TEST_F(is_pointer, class_rvalue)
    {
        validate<false, foo&&>();
    }

    TEST_F(is_pointer, literal_pointer)
    {
        validate<true, int*>();
    }

    TEST_F(is_pointer, literal_const_pointer)
    {
        validate<true, int * const>();
    }

    TEST_F(is_pointer, literal_volatile_pointer)
    {
        validate<true, int * volatile>();
    }

    TEST_F(is_pointer, literal_value)
    {
        validate<false, int>();
    }

    TEST_F(is_pointer, literal_lvalue)
    {
        validate<false, int&>();
    }

    TEST_F(is_pointer, literal_rvalue)
    {
        validate<false, int&&>();
    }
}
