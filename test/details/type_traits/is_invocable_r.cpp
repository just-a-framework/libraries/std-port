#include <std_port/details/type_traits/is_invocable_r.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_invocable_r :
        value_fixture<
            ::jaf::std_port::is_invocable_r
        >
    {
    };

    int f(bool);

    struct foo
    {
    };

    TEST_F(is_invocable_r, callable)
    {
        validate<true, int, decltype(&f), bool>();
    }

    TEST_F(is_invocable_r, ignore_void_ret)
    {
        validate<true, void, decltype(&f), bool>();
    }

    TEST_F(is_invocable_r, wrong_ret)
    {
        validate<false, foo, decltype(&f), bool>();
    }

    TEST_F(is_invocable_r, wrong_number_args)
    {
        validate<false, int, decltype(&f)>();
        validate<false, int, decltype(&f), bool, foo>();

        validate<false, void, decltype(&f)>();
        validate<false, void, decltype(&f), bool, foo>();
    }

    TEST_F(is_invocable_r, wrong_arg_type)
    {
        validate<false, int, decltype(&f), foo>();
        
        validate<false, void, decltype(&f), foo>();
    }
}
