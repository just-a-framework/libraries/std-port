#include <std_port/details/type_traits/is_polymorphic.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_polymorphic :
        value_fixture<
            ::jaf::std_port::is_polymorphic
        >
    {
    };

    struct foo
    {
    };

    struct virtual_foo
    {
        virtual ~virtual_foo();
    };

    struct inherit_foo : virtual_foo
    {
    };

    TEST_F(is_polymorphic, empty)
    {
        validate<false, foo>();
    }

    TEST_F(is_polymorphic, virtual)
    {
        validate<true, virtual_foo>();
    }

    TEST_F(is_polymorphic, inherit)
    {
        validate<true, inherit_foo>();
    }
}
