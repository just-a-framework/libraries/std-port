#include <std_port/details/type_traits/is_copy_assignable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_copy_assignable :
        value_fixture<
            ::jaf::std_port::is_copy_assignable
        >
    {
    };

    struct bar
    {
        bar& operator=(const bar&) = delete;
    };

    struct foo
    {
        foo& operator=(const foo&) = default;
    };


    TEST_F(is_copy_assignable, assignable)
    {
        validate<true, foo>();
    }

    TEST_F(is_copy_assignable, not_assignable)
    {
        validate<false, bar>();
    }
}
