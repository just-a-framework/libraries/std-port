#include <std_port/details/type_traits/is_signed.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_signed :
        value_fixture<
            ::jaf::std_port::is_signed
        >
    {
    };

    struct foo
    {
    };


    TEST_F(is_signed, int)
    {
        validate<true, int>();
        validate<true, signed int>();
        validate<false, unsigned int>();
    }

    TEST_F(is_signed, float)
    {
        validate<false, float>();
    }

    TEST_F(is_signed, class)
    {
        validate<false, foo>();
    }
}
