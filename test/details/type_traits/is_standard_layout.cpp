#include <std_port/details/type_traits/is_standard_layout.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_standard_layout :
        value_fixture<
            ::jaf::std_port::is_standard_layout
        >
    {
    };

    struct foo
    {
    };

    struct multiple_foo
    {
        int m;
        int n;
    };

    struct private_foo
    {
        int m;
    private:
        int n;
    };

    struct virtual_foo
    {
        virtual void f();
    };

    TEST_F(is_standard_layout, empty)
    {
        validate<true, foo>();
    }

    TEST_F(is_standard_layout, multiple_members)
    {
        validate<true, multiple_foo>();
    }

    TEST_F(is_standard_layout, private_member)
    {
        validate<false, private_foo>();
    }

    TEST_F(is_standard_layout, virtual_function)
    {
        validate<false, virtual_foo>();
    }
}
