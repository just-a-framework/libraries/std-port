#include <std_port/details/type_traits/remove_pointer.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct remove_pointer
        : same_type_fixture<
            ::jaf::std_port::remove_pointer,
            ::jaf::std_port::remove_pointer_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(remove_pointer, remove_pointer_class)
    {
        validate<foo, foo*>();
    }

    TEST_F(remove_pointer, remove_one_pointer_class)
    {
        validate<foo*, foo**>();
    }

    TEST_F(remove_pointer, remove_const_pointer_class)
    {
        validate<foo, foo* const>();
    }

    TEST_F(remove_pointer, remove_volatile_pointer_class)
    {
        validate<foo, foo* volatile>();
    }

    TEST_F(remove_pointer, remove_cv_pointer_class)
    {
        validate<foo, foo* const volatile>();
    }

    TEST_F(remove_pointer, remove_pointer_literal)
    {
        validate<int, int*>();
    }

    TEST_F(remove_pointer, remove_one_pointer_literal)
    {
        validate<int*, int**>();
    }

    TEST_F(remove_pointer, remove_const_pointer_literal)
    {
        validate<int, int* const>();
    }

    TEST_F(remove_pointer, remove_volatile_pointer_literal)
    {
        validate<int, int* volatile>();
    }

    TEST_F(remove_pointer, remove_cv_pointer_literal)
    {
        validate<int, int* const volatile>();
    }
}
