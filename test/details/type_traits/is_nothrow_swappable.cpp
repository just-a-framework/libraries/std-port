namespace jaf::std_port::test
{
    struct bar
    {
    };

    struct foo
    {
        foo(foo&&);
    };

    struct fee
    {
        fee(fee&&) = delete;
    };

    struct boo
    {
    };

    struct baa
    {
    };
}

namespace jaf::std_port
{
    void swap(::jaf::std_port::test::boo&, ::jaf::std_port::test::boo&);

    void swap(::jaf::std_port::test::baa&, ::jaf::std_port::test::baa&) noexcept;
}

#include <std_port/details/utility/swap.hpp>
#include <std_port/details/type_traits/is_nothrow_swappable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_swappable :
        value_fixture<
            ::jaf::std_port::is_nothrow_swappable
        >
    {
    };

    TEST_F(is_nothrow_swappable, swappable)
    {
        validate<true, bar>();
        validate<true, baa>();
    }

    TEST_F(is_nothrow_swappable, not_swappable)
    {
        validate<false, fee>();
        validate<false, void>();
        validate<false, foo>();
    }
}
