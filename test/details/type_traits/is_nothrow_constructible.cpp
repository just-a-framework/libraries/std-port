#include <std_port/details/type_traits/is_nothrow_constructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_constructible :
        value_fixture<
            ::jaf::std_port::is_nothrow_constructible
        >
    {
    };

    struct bar
    {
    };

    enum class fee
    {
    };

    struct foo
    {
        foo() = default;

        foo(fee, int = 0) noexcept
        {
        }

        foo(bar)
        {
        }
    };


    TEST_F(is_nothrow_constructible, constructible)
    {
        validate<true, foo>();
        validate<true, foo, fee>();
        validate<true, foo, fee, int>();
    }

    TEST_F(is_nothrow_constructible, not_constructible)
    {
        validate<false, foo, bar>();
        validate<false, foo, bar, bar>();
        validate<false, foo, bool>();
        validate<false, foo, char, fee>();
    }
}
