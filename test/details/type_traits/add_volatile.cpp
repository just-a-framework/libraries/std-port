#include <std_port/details/type_traits/add_volatile.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct add_volatile 
        : same_type_fixture<
            ::jaf::std_port::add_volatile,
            ::jaf::std_port::add_volatile_t
        >
    {
    };

    struct foo
    {
        bool is_volatile()
        {
            return false;
        }

        bool is_volatile() volatile
        {
            return true;
        }
    };

    TEST_F(add_volatile, make_volatile_class)
    {
        validate<volatile foo, foo>();
    }

    TEST_F(add_volatile, do_nothing_class)
    {
        validate<volatile foo, volatile foo>();
    }

    TEST_F(add_volatile, make_volatile_literal)
    {
        validate<volatile int, int>();
    }

    TEST_F(add_volatile, do_nothing_literal)
    {
        validate<volatile int, volatile int>();
    }

    TEST_F(add_volatile, call_volatile)
    {
        EXPECT_FALSE(foo{}.is_volatile());
        EXPECT_TRUE(typename ::jaf::std_port::add_volatile<foo>::type{}.is_volatile());
        EXPECT_TRUE(::jaf::std_port::add_volatile_t<foo>{}.is_volatile());
    }
}
