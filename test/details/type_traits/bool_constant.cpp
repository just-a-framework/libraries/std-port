#include <std_port/details/type_traits/bool_constant.hpp>

namespace jaf::std_port::test
{
    struct bool_constant : ::testing::Test
    {
    };

    TEST_F(bool_constant, is_true)
    {
        using t_t = ::jaf::std_port::bool_constant<true>;
        const auto t = t_t{};

        static_assert(t_t::value);
        EXPECT_TRUE(t_t::value);
        EXPECT_TRUE(static_cast<bool>(t));
        EXPECT_TRUE(t());
    }

    TEST_F(bool_constant, is_false)
    {
        using f_t = ::jaf::std_port::bool_constant<false>;
        const auto f = f_t{};

        static_assert(!f_t::value);
        EXPECT_FALSE(f_t::value);
        EXPECT_FALSE(static_cast<bool>(f));
        EXPECT_FALSE(f());
    }
}
