#include <std_port/details/type_traits/is_literal_type.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_literal_type :
        value_fixture<
            ::jaf::std_port::is_literal_type
        >
    {
    };

    struct foo
    {
        int m;
    };

    struct virtual_foo
    {
        virtual ~virtual_foo();
    };

    TEST_F(is_literal_type, simple)
    {
        validate<true, foo>();
    }

    TEST_F(is_literal_type, virtual_class)
    {
        validate<false, virtual_foo>();
    }
}
