#include <std_port/details/type_traits/remove_extent.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct remove_extent
        : same_type_fixture<
            ::jaf::std_port::remove_extent,
            ::jaf::std_port::remove_extent_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(remove_extent, unbounded_array_class)
    {
        validate<foo, foo[]>();
    }

    TEST_F(remove_extent, bounded_array_class)
    {
        validate<foo, foo[10]>();
    }

    TEST_F(remove_extent, multi_array_class)
    {
        validate<foo[5], foo[10][5]>();
    }

    TEST_F(remove_extent, unbounded_array_literal)
    {
        validate<int, int[]>();
    }

    TEST_F(remove_extent, bounded_array_literal)
    {
        validate<int, int[10]>();
    }

    TEST_F(remove_extent, multi_array_literal)
    {
        validate<int[5], int[10][5]>();
    }
}
