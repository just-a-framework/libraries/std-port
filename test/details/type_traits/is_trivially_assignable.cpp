#include <std_port/details/type_traits/is_trivially_assignable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_trivially_assignable :
        value_fixture<
            ::jaf::std_port::is_trivially_assignable
        >
    {
    };

    enum class fee
    {
    };

    struct foo
    {
        foo& operator=(const foo&);
        foo& operator=(fee);
    };

    struct bar
    {
        bar& operator=(fee);
    };


    TEST_F(is_trivially_assignable, assignable)
    {
        validate<true, bar, const bar&>();
    }

    TEST_F(is_trivially_assignable, not_assignable)
    {
        validate<false, bar, fee>();
        validate<false, foo, fee>();
        validate<false, foo, bool>();
        validate<false, foo, bar>();
    }
}
