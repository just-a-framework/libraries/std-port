#include <std_port/details/type_traits/is_member_object_pointer.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_member_object_pointer :
        value_fixture<
            ::jaf::std_port::is_member_object_pointer
        >
    {
    };

    struct foo
    {
        int m;
        int f();
    };

    TEST_F(is_member_object_pointer, class_member)
    {
        validate<true, decltype(&foo::m)>();
    }

    TEST_F(is_member_object_pointer, class_function)
    {
        validate<false, decltype(&foo::f)>();
    }

    TEST_F(is_member_object_pointer, class)
    {
        validate<false, foo>();
    }

    TEST_F(is_member_object_pointer, literal)
    {
        validate<false, int>();
    }
}
