#include <std_port/details/type_traits/enable_if.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct enable_if
        : ::testing::Test
    {
        template<class T, bool B, class U>
        void validate()
        {
            static_assert(std::is_same_v<T, typename ::jaf::std_port::enable_if<B, U>::type>);
            EXPECT_TRUE((std::is_same_v<T, typename ::jaf::std_port::enable_if<B, U>::type>));

            static_assert(std::is_same_v<T, ::jaf::std_port::enable_if_t<B, U>>);
            EXPECT_TRUE((std::is_same_v<T, ::jaf::std_port::enable_if_t<B, U>>));
        }
    };

    struct foo
    {
    };

    TEST_F(enable_if, true)
    {
        validate<foo, true, foo>();
        validate<int, true, int>();
    }
}
