#include <std_port/details/type_traits/add_pointer.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct add_pointer :
        same_type_fixture<
            ::jaf::std_port::add_pointer,
            ::jaf::std_port::add_pointer_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(add_pointer, add_pointer_class)
    {
        validate<foo*, foo>();
    }

    TEST_F(add_pointer, add_pointer_again_class)
    {
        validate<foo**, foo*>();
    }

    TEST_F(add_pointer, add_pointer_ref_class)
    {
        validate<foo*, foo&>();
    }

    TEST_F(add_pointer, add_pointer_literal)
    {
        validate<int*, int>();
    }

    TEST_F(add_pointer, add_pointer_again_literal)
    {
        validate<int**, int*>();
    }

    TEST_F(add_pointer, add_pointer_ref_literal)
    {
        validate<int*, int&>();
    }
}
