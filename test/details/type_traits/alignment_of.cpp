#include <std_port/details/type_traits/alignment_of.hpp>

namespace jaf::std_port::test
{
    struct alignment_of : ::testing::Test
    {
    };

    struct foo
    {
    };

    template<class T, size_t N>
    void test_type()
    {
        static_assert(::jaf::std_port::alignment_of<T>::value == N);
        EXPECT_TRUE((::jaf::std_port::alignment_of<T>::value == N));

        static_assert(::jaf::std_port::alignment_of_v<T> == N);
        EXPECT_TRUE((::jaf::std_port::alignment_of_v<T> == N));
    }

    TEST_F(alignment_of, int)
    {
        test_type<int, alignof(int)>();
    }

    TEST_F(alignment_of, foo)
    {
        test_type<foo, alignof(foo)>();
    }
}
