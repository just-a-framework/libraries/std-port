#include <std_port/details/type_traits/is_final.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_final :
        value_fixture<
            ::jaf::std_port::is_final
        >
    {
    };

    struct foo
    {
    };

    struct final_foo final
    {
    };

    TEST_F(is_final, class)
    {
        validate<false, foo>();
    }

    TEST_F(is_final, final_class)
    {
        validate<true, final_foo>();
    }
}
