#include <std_port/details/type_traits/is_nothrow_move_assignable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_move_assignable :
        value_fixture<
            ::jaf::std_port::is_nothrow_move_assignable
        >
    {
    };

    struct deleted_foo
    {
        deleted_foo& operator=(deleted_foo&&) = delete;
    };

    struct defaulted_foo
    {
        defaulted_foo& operator=(defaulted_foo&&) = default;
    };

    struct defined_foo
    {
        defined_foo& operator=(defined_foo&&);
    };

    struct nothrow_defined_foo
    {
        nothrow_defined_foo& operator=(nothrow_defined_foo&&) noexcept;
    };

    TEST_F(is_nothrow_move_assignable, delete)
    {
        validate<false, deleted_foo>();
    }

    TEST_F(is_nothrow_move_assignable, default)
    {
        validate<true, defaulted_foo>();
    }

    TEST_F(is_nothrow_move_assignable, defined)
    {
        validate<false, defined_foo>();
    }

    TEST_F(is_nothrow_move_assignable, nothrow_defined)
    {
        validate<true, nothrow_defined_foo>();
    }
}
