#include <std_port/details/type_traits/false_type.hpp>

namespace jaf::std_port::test
{
    struct false_type : ::testing::Test
    {
    };

    TEST_F(false_type, is_false)
    {
        const auto f = ::jaf::std_port::false_type{};

        static_assert(!::jaf::std_port::false_type::value);
        EXPECT_FALSE(::jaf::std_port::false_type::value);
        EXPECT_FALSE(static_cast<bool>(f));
        EXPECT_FALSE(f());
    }
}
