#include <std_port/details/type_traits/is_arithmetic.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_arithmetic :
        value_fixture<
            ::jaf::std_port::is_arithmetic
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_arithmetic, class_value)
    {
        validate<false, foo>();
    }

    TEST_F(is_arithmetic, class_reference)
    {
        validate<false, foo&>();
    }

    TEST_F(is_arithmetic, int_value)
    {
        validate<true, int>();
    }

    TEST_F(is_arithmetic, const_int_value)
    {
        validate<true, const int>();
    }

    TEST_F(is_arithmetic, int_reference)
    {
        validate<false, int&>();
    }

    TEST_F(is_arithmetic, float_value)
    {
        validate<true, float>();
    }

    TEST_F(is_arithmetic, const_float_value)
    {
        validate<true, const float>();
    }

    TEST_F(is_arithmetic, float_reference)
    {
        validate<false, float&>();
    }
}
