#include <std_port/details/type_traits/is_lvalue_reference.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_lvalue_reference :
        value_fixture<
            ::jaf::std_port::is_lvalue_reference
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_lvalue_reference, class_value)
    {
        validate<false, foo>();
    }

    TEST_F(is_lvalue_reference, class_lvalue)
    {
        validate<true, foo&>();
    }

    TEST_F(is_lvalue_reference, class_rvalue)
    {
        validate<false, foo&&>();
    }

    TEST_F(is_lvalue_reference, literal_value)
    {
        validate<false, int>();
    }

    TEST_F(is_lvalue_reference, literal_lvalue)
    {
        validate<true, int&>();
    }

    TEST_F(is_lvalue_reference, literal_rvalue)
    {
        validate<false, int&&>();
    }
}
