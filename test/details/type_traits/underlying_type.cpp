#include <std_port/details/type_traits/underlying_type.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct underlying_type
        : same_type_fixture<
            ::jaf::std_port::underlying_type,
            ::jaf::std_port::underlying_type_t
        >
    {
    };

    enum class foo
    {
    };

    enum class bar : uint8_t
    {
    };

    TEST_F(underlying_type, underlying_type_default)
    {
        validate<int, foo>();
    }

    TEST_F(underlying_type, underlying_type_uint8_t)
    {
        validate<uint8_t, bar>();
    }
}
