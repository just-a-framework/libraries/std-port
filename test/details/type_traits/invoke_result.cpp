#include <std_port/details/type_traits/invoke_result.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct invoke_result :
        same_type_fixture<
            ::jaf::std_port::invoke_result,
            ::jaf::std_port::invoke_result_t
        >
    {
    };

    void f();
    int g();
    int h(char);
    void h(bool);

    struct foo
    {
        bool f();
    };

    TEST_F(invoke_result, global)
    {
        validate<void, decltype(f)>();
        validate<int, decltype(g)>();
    }

    TEST_F(invoke_result, member)
    {
        validate<bool, decltype(&foo::f), foo>();
    }

    TEST_F(invoke_result, overload)
    {
        validate<int, decltype(static_cast<int(*)(char)>(&h)), char>();
        validate<void, decltype(static_cast<void(*)(bool)>(&h)), bool>();
    }
}
