#include <std_port/details/type_traits/is_array.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_array :
        value_fixture<
            ::jaf::std_port::is_array
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_array, class)
    {
        validate<false, foo>();
    }

    TEST_F(is_array, bounded_class)
    {
        validate<true, foo[4]>();
    }

    TEST_F(is_array, unbounded_class)
    {
        validate<true, foo[]>();
    }

    TEST_F(is_array, literal)
    {
        validate<false, int>();
    }

    TEST_F(is_array, bounded_literal)
    {
        validate<true, int[4]>();
    }

    TEST_F(is_array, unbounded_literal)
    {
        validate<true, int[]>();
    }
}
