#include <std_port/details/type_traits/remove_all_extents.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct remove_all_extents
        : same_type_fixture<
            ::jaf::std_port::remove_all_extents,
            ::jaf::std_port::remove_all_extents_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(remove_all_extents, unbounded_array_class)
    {
        validate<foo, foo[]>();
    }

    TEST_F(remove_all_extents, bounded_array_class)
    {
        validate<foo, foo[10]>();
    }

    TEST_F(remove_all_extents, multi_array_class)
    {
        validate<foo, foo[10][5]>();
    }

    TEST_F(remove_all_extents, unbounded_array_literal)
    {
        validate<int, int[]>();
    }

    TEST_F(remove_all_extents, bounded_array_literal)
    {
        validate<int, int[10]>();
    }

    TEST_F(remove_all_extents, multi_array_literal)
    {
        validate<int, int[10][5]>();
    }
}
