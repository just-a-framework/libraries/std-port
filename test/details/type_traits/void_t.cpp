#include <std_port/details/type_traits/void_t.hpp>

namespace jaf::std_port::test
{
    struct void_t : ::testing::Test
    {
        template<class... T>
        void validate()
        {
            static_assert(std::is_same_v<void, ::jaf::std_port::void_t<T...>>);
            EXPECT_TRUE((std::is_same_v<void, ::jaf::std_port::void_t<T...>>));
        }
    };

    struct foo
    {
    };

    TEST_F(void_t, always_void)
    {
        validate<int>();
        validate<foo>();
        validate<bool, foo, int>();
        validate<foo, foo>();
        validate<int, int, foo, foo, bool>();
    }
}
