#include <std_port/details/type_traits/is_constructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_constructible :
        value_fixture<
            ::jaf::std_port::is_constructible
        >
    {
    };

    struct bar
    {
    };

    enum class fee
    {
    };

    struct foo
    {
        foo(fee, int = 0)
        {
        }

        foo(bar)
        {
        }
    };


    TEST_F(is_constructible, constructible)
    {
        validate<true, foo, fee, int>();
        validate<true, foo, bar>();
        validate<true, foo, fee>();
    }

    TEST_F(is_constructible, not_constructible)
    {
        validate<false, foo, bar, bar>();
        validate<false, foo, bool>();
        validate<false, foo, char, fee>();
    }
}
