#include <std_port/details/type_traits/is_move_assignable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_move_assignable :
        value_fixture<
            ::jaf::std_port::is_move_assignable
        >
    {
    };

    struct bar
    {
        bar& operator=(bar&&) = delete;
    };

    struct foo
    {
        foo& operator=(foo&&) = default;
    };


    TEST_F(is_move_assignable, assignable)
    {
        validate<true, foo>();
    }

    TEST_F(is_move_assignable, not_assignable)
    {
        validate<false, bar>();
    }
}
