#include <std_port/details/type_traits/is_nothrow_copy_assignable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_copy_assignable :
        value_fixture<
            ::jaf::std_port::is_nothrow_copy_assignable
        >
    {
    };

    struct delete_foo
    {
        delete_foo& operator=(const delete_foo&) = delete;
    };

    struct default_foo
    {
        default_foo& operator=(const default_foo&) = default;
    };

    struct defined_noexcept_foo
    {
        defined_noexcept_foo& operator=(const defined_noexcept_foo&) noexcept;
    };

    struct defined_foo
    {
        defined_foo& operator=(const defined_foo&);
    };

    TEST_F(is_nothrow_copy_assignable, delete)
    {
        validate<false, delete_foo>();
    }

    TEST_F(is_nothrow_copy_assignable, default)
    {
        validate<true, default_foo>();
    }

    TEST_F(is_nothrow_copy_assignable, defined_noexcept)
    {
        validate<true, defined_noexcept_foo>();
    }

    TEST_F(is_nothrow_copy_assignable, defined)
    {
        validate<false, defined_foo>();
    }
}
