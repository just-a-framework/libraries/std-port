#include <std_port/details/type_traits/is_trivially_copyable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_trivially_copyable :
        value_fixture<
            ::jaf::std_port::is_trivially_copyable
        >
    {
    };

    struct foo
    {
        int n;
    };

    struct ctor_foo
    {
        ctor_foo(const ctor_foo&)
        {
        }
    };

    struct default_ctor_foo
    {
        int m;
 
        default_ctor_foo(default_ctor_foo const&) = default;

        default_ctor_foo(int n)
            : m{n + 1}
        {}
    };

    struct virtual_foo
    {
        virtual void f();
    };

    TEST_F(is_trivially_copyable, generated_copy_ctor)
    {
        validate<true, foo>();
    }

    TEST_F(is_trivially_copyable, defined_copy_ctor)
    {
        validate<false, ctor_foo>();
    }

    TEST_F(is_trivially_copyable, default_copy_ctor)
    {
        validate<true, default_ctor_foo>();
    }

    TEST_F(is_trivially_copyable, virtual_function)
    {
        validate<false, virtual_foo>();
    }
}
