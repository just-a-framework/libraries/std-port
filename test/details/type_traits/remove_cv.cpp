#include <std_port/details/type_traits/remove_cv.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct remove_cv 
        : same_type_fixture<
            ::jaf::std_port::remove_cv,
            ::jaf::std_port::remove_cv_t
        >
    {
    };

    struct foo
    {
        bool is_const_volatile()
        {
            return false;
        }

        bool is_const_volatile() const volatile
        {
            return true;
        }
    };

    TEST_F(remove_cv, remove_const_volatile_class)
    {
        validate<foo, const volatile foo>();
    }

    TEST_F(remove_cv, remove_volatile_class)
    {
        validate<foo, volatile foo>();
    }

    TEST_F(remove_cv, remove_const_class)
    {
        validate<foo, const foo>();
    }

    TEST_F(remove_cv, do_nothing_class)
    {
        validate<foo, foo>();
    }

    TEST_F(remove_cv, remove_const_volatile_literal)
    {
        validate<int, const volatile int>();
    }

    TEST_F(remove_cv, remove_volatile_literal)
    {
        validate<int, volatile int>();
    }

    TEST_F(remove_cv, remove_const_literal)
    {
        validate<int, const int>();
    }

    TEST_F(remove_cv, do_nothing_literal)
    {
        validate<int, int>();
    }

    TEST_F(remove_cv, call_const_volatile)
    {
        const volatile foo f{};
        EXPECT_TRUE(f.is_const_volatile());
        EXPECT_FALSE(typename ::jaf::std_port::remove_cv<const volatile foo>::type{}.is_const_volatile());
        EXPECT_FALSE(::jaf::std_port::remove_cv_t<const volatile foo>{}.is_const_volatile());
    }
}
