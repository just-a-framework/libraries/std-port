#include <std_port/details/type_traits/is_volatile.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_volatile :
        value_fixture<
            ::jaf::std_port::is_volatile
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_volatile, class)
    {
        validate<true, volatile foo>();
        validate<false, foo>();
    }

    TEST_F(is_volatile, literal)
    {
        validate<true, volatile int>();
        validate<false, int>();
    }
}
