#include <std_port/details/type_traits/conjunction.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct conjunction :
        value_fixture<
            ::jaf::std_port::conjunction
        >
    {
    };

    template<bool B>
    struct foo : std::bool_constant<B>
    {
    };

    TEST_F(conjunction, empty)
    {
        validate<true>();
    }

    TEST_F(conjunction, one)
    {
        validate<true, foo<true>>();
    }

    TEST_F(conjunction, first)
    {
        validate<false, foo<true>, foo<false>, foo<false>>();
    }

    TEST_F(conjunction, middle)
    {
        validate<false, foo<true>, foo<false>, foo<true>>();
    }

    TEST_F(conjunction, end)
    {
        validate<false, foo<true>, foo<true>, foo<false>>();
    }

    TEST_F(conjunction, all)
    {
        validate<true, foo<true>, foo<true>, foo<true>>();
    }
}
