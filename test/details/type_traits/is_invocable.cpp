#include <std_port/details/type_traits/is_invocable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_invocable :
        value_fixture<
            ::jaf::std_port::is_invocable
        >
    {
    };

    int f(bool);

    struct foo
    {
    };

    TEST_F(is_invocable, callable)
    {
        validate<true, decltype(&f), bool>();
    }

    TEST_F(is_invocable, wrong_number_args)
    {
        validate<false, decltype(&f)>();
        validate<false, decltype(&f), bool, foo>();

        validate<false, decltype(&f)>();
        validate<false, decltype(&f), bool, foo>();
    }

    TEST_F(is_invocable, wrong_arg_type)
    {
        validate<false, decltype(&f), foo>();
        
        validate<false, decltype(&f), foo>();
    }
}
