namespace jaf::std_port::test
{
    struct bar
    {
    };

    struct foo
    {
    };

    struct fee
    {
        fee(fee&&) = delete;
    };
}

namespace jaf::std_port
{
    void swap(::jaf::std_port::test::foo&, ::jaf::std_port::test::fee&);

    void swap(::jaf::std_port::test::fee&, ::jaf::std_port::test::foo&);

    void swap(::jaf::std_port::test::bar&, ::jaf::std_port::test::foo&);
}

#include <std_port/details/utility/swap.hpp>
#include <std_port/details/type_traits/is_swappable_with.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_swappable_with :
        value_fixture<
            ::jaf::std_port::is_swappable_with
        >
    {
    };

    TEST_F(is_swappable_with, swappable_with)
    {
        validate<true, bar&, bar&>();
        validate<true, foo&, foo&>();

        validate<true, fee&, foo&>();
        validate<true, foo&, fee&>();
    }

    TEST_F(is_swappable_with, not_swappable_with)
    {
        validate<false, fee&, fee&>();

        validate<false, foo&, bar&>();
        validate<false, bar&, foo&>();
    }
}
