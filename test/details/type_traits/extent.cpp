#include <std_port/details/type_traits/extent.hpp>

namespace jaf::std_port::test
{
    struct extent : ::testing::Test
    {
    };

    struct foo
    {
    };

    template<class T, size_t M>
    void test_default_type()
    {
        static_assert(::jaf::std_port::extent<T>::value == M);
        EXPECT_TRUE((::jaf::std_port::extent<T>::value == M));

        static_assert(::jaf::std_port::extent_v<T> == M);
        EXPECT_TRUE((::jaf::std_port::extent_v<T> == M));
    }

    template<class T, unsigned N, size_t M>
    void test_type()
    {
        static_assert(::jaf::std_port::extent<T, N>::value == M);
        EXPECT_TRUE((::jaf::std_port::extent<T, N>::value == M));

        static_assert(::jaf::std_port::extent_v<T, N> == M);
        EXPECT_TRUE((::jaf::std_port::extent_v<T, N> == M));
    }

    TEST_F(extent, default_int)
    {
        test_default_type<int[10], 10>();
    }

    TEST_F(extent, bounded_int)
    {
        test_type<int[6], 0, 6>();
    }

    TEST_F(extent, unbounded_int)
    {
        test_type<int[], 0, 0>();
    }

    TEST_F(extent, multi_int)
    {
        test_type<int[2][6][10], 2, 10>();
        test_type<int[2][6][2], 1, 6>();
    }

    TEST_F(extent, out_of_range_int)
    {
        test_type<int[2][6][10], 4, 0>();
    }

    TEST_F(extent, default_foo)
    {
        test_default_type<foo[10], 10>();
    }

    TEST_F(extent, bounded_foo)
    {
        test_type<foo[6], 0, 6>();
    }

    TEST_F(extent, unbounded_foo)
    {
        test_type<foo[], 0, 0>();
    }

    TEST_F(extent, multi_foo)
    {
        test_type<foo[2][6][10], 2, 10>();
        test_type<foo[2][6][2], 1, 6>();
    }

    TEST_F(extent, out_of_range_foo)
    {
        test_type<foo[2][6][10], 4, 0>();
    }
}
