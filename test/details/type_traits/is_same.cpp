#include <std_port/details/type_traits/is_same.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_same :
        value_fixture<
            ::jaf::std_port::is_same
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_same, same_literal)
    {
        validate<true, int, int>();
    }

    TEST_F(is_same, const_literal)
    {
        validate<false, int, const int>();
    }

    TEST_F(is_same, signed_literal)
    {
        validate<true, int, signed int>();
    }

    TEST_F(is_same, same_class)
    {
        validate<true, foo, foo>();
    }

    TEST_F(is_same, const_class)
    {
        validate<false, foo, const foo>();
    }

    TEST_F(is_same, mixed_types)
    {
        validate<false, int, foo>();
    }
}
