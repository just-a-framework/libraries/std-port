#include <std_port/details/utility/swap.hpp>
#include <std_port/details/type_traits/is_swappable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_swappable :
        value_fixture<
            ::jaf::std_port::is_swappable
        >
    {
    };

    struct bar
    {
    };

    struct foo
    {
    };

    struct fee
    {
        fee(fee&&) = delete;
    };

    TEST_F(is_swappable, swappable)
    {
        validate<true, bar>();
        validate<true, foo>();
    }

    TEST_F(is_swappable, not_swappable)
    {
        validate<false, fee>();
        validate<false, void>();
    }
}
