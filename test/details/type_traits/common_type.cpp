#include <std_port/details/type_traits/common_type.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct common_type
        : same_type_fixture<
            ::jaf::std_port::common_type,
            ::jaf::std_port::common_type_t
        >
    {
    };

    template<class AlwaysVoid, bool B, class... T>
    struct check_type
    {
        static void test()
        {
            static_assert(!B);
            EXPECT_TRUE(!B);
        }
    };

    template<bool B, class... T>
    struct check_type<void_t<::jaf::std_port::common_type_t<T...>>, B, T...>
    {
        static void test()
        {
            static_assert(B);
            EXPECT_TRUE(B);
        }
    };

    struct foo
    {
    };

    struct bar
    {
    };
    
    TEST_F(common_type, first_case)
    {
        check_type<void, false>::test();
    }

    TEST_F(common_type, second_case)
    {
        check_type<void, true, int>::test();
        check_type<void, true, foo>::test();
    }

    TEST_F(common_type, third_case_first_form)
    {
        check_type<void, true, int&&, short&>::test();
        check_type<void, true, foo&, const foo>::test();
    }

    TEST_F(common_type, third_case_third_form)
    {
        check_type<void, true, int, short>::test();
        check_type<void, true, foo, foo>::test();
    }

    TEST_F(common_type, third_case_forth_form)
    {
        check_type<void, false, int, foo>::test();
        check_type<void, false, foo, bar>::test();
    }

    TEST_F(common_type, forth_case)
    {
        check_type<void, true, bar&, bar&&, const bar&&>::test();
        check_type<void, true, int, short&, const long>::test();

        check_type<void, false, foo, bar, bar>::test();
        check_type<void, false, int, foo, bar>::test();
    }
}
