#include <std_port/details/type_traits/remove_const.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct remove_const
        : same_type_fixture<
            ::jaf::std_port::remove_const,
            ::jaf::std_port::remove_const_t
        >
    {
    };

    struct foo
    {
        bool is_const()
        {
            return false;
        }

        bool is_const() const
        {
            return true;
        }
    };

    TEST_F(remove_const, remove_const_class)
    {
        validate<foo, const foo>();
    }

    TEST_F(remove_const, do_nothing_class)
    {
        validate<foo, foo>();
    }

    TEST_F(remove_const, remove_const_literal)
    {
        validate<int, const int>();
    }

    TEST_F(remove_const, do_nothing_literal)
    {
        validate<int, int>();
    }

    TEST_F(remove_const, call_const)
    {
        const foo f{};
        EXPECT_TRUE(f.is_const());
        EXPECT_FALSE(typename ::jaf::std_port::remove_const<const foo>::type{}.is_const());
        EXPECT_FALSE(::jaf::std_port::remove_const_t<const foo>{}.is_const());
    }
}
