#include <std_port/details/type_traits/is_scalar.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_scalar :
        value_fixture<
            ::jaf::std_port::is_scalar
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_scalar, class_value)
    {
        validate<false, foo>();
    }

    TEST_F(is_scalar, class_reference)
    {
        validate<false, foo&>();
    }

    TEST_F(is_scalar, class_pointer)
    {
        validate<true, foo*>();
    }

    TEST_F(is_scalar, int_value)
    {
        validate<true, int>();
    }

    TEST_F(is_scalar, const_int_value)
    {
        validate<true, const int>();
    }

    TEST_F(is_scalar, int_reference)
    {
        validate<false, int&>();
    }

    TEST_F(is_scalar, float_value)
    {
        validate<true, float>();
    }

    TEST_F(is_scalar, const_float_value)
    {
        validate<true, const float>();
    }

    TEST_F(is_scalar, float_reference)
    {
        validate<false, float&>();
    }

    TEST_F(is_scalar, void)
    {
        validate<false, void>();
    }

    TEST_F(is_scalar, null_pointer)
    {
        validate<true, nullptr_t>();
    }
}
