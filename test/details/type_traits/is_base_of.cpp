#include <std_port/details/type_traits/is_base_of.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_base_of :
        value_fixture<
            ::jaf::std_port::is_base_of
        >
    {
    };

    struct foo
    {
    };

    struct bar
    {
    };

    struct foobar
        : foo
        , bar
    {
    };

    struct foo2
        : foo
    {
    };

    struct foo3
        : foo2
    {
    };

    TEST_F(is_base_of, diff_types)
    {
        validate<false, foo, bar>();
        validate<false, bar, foo>();
        validate<false, int, foo>();
        validate<false, bar, int>();
    }

    TEST_F(is_base_of, multiple_inherit)
    {
        validate<true, foo, foobar>();
        validate<true, bar, foobar>();
    }

    TEST_F(is_base_of, simple_inherit)
    {
        validate<true, foo, foo2>();
    }
    
    TEST_F(is_base_of, transitive_inherit)
    {
        validate<true, foo, foo3>();
    }
}
