#include <std_port/details/type_traits/is_member_function_pointer.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_member_function_pointer :
        value_fixture<
            ::jaf::std_port::is_member_function_pointer
        >
    {
    };

    struct foo
    {
        int m;
        int f();
    };

    TEST_F(is_member_function_pointer, class_member)
    {
        validate<false, decltype(&foo::m)>();
    }

    TEST_F(is_member_function_pointer, class_function)
    {
        validate<true, decltype(&foo::f)>();
    }

    TEST_F(is_member_function_pointer, class)
    {
        validate<false, foo>();
    }

    TEST_F(is_member_function_pointer, literal)
    {
        validate<false, int>();
    }
}
