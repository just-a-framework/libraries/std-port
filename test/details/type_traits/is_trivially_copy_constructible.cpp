#include <std_port/details/type_traits/is_trivially_copy_constructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_trivially_copy_constructible :
        value_fixture<
            ::jaf::std_port::is_trivially_copy_constructible
        >
    {
    };

    struct generated_foo
    {
    };

    struct defaulted_foo
    {
        defaulted_foo(const defaulted_foo&) = default;
    };

    struct defined_foo
    {
        defined_foo(const defined_foo&)
        {
        }
    };

    struct deleted_foo
    {
        deleted_foo(const deleted_foo&) = delete;
    };

    struct other_foo
    {
        other_foo(int)
        {
        }
    };


    TEST_F(is_trivially_copy_constructible, generated)
    {
        validate<true, generated_foo>();
    }

    TEST_F(is_trivially_copy_constructible, default)
    {
        validate<true, defaulted_foo>();
    }

    TEST_F(is_trivially_copy_constructible, defined)
    {
        validate<false, defined_foo>();
    }

    TEST_F(is_trivially_copy_constructible, delete)
    {
        validate<false, deleted_foo>();
    }

    TEST_F(is_trivially_copy_constructible, other)
    {
        validate<true, other_foo>();
    }
}
