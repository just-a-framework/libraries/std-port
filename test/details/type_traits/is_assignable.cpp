#include <std_port/details/type_traits/is_assignable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_assignable :
        value_fixture<
            ::jaf::std_port::is_assignable
        >
    {
    };

    struct bar
    {
    };

    enum class fee
    {
    };

    struct foo
    {
        foo& operator=(const foo&);
        foo& operator=(fee);
    };


    TEST_F(is_assignable, assignable)
    {
        validate<true, foo, fee>();
        validate<true, foo, foo>();
    }

    TEST_F(is_assignable, not_assignable)
    {
        validate<false, foo, bar>();
        validate<false, foo, bool>();
    }
}
