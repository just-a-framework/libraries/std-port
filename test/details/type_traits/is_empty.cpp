#include <std_port/details/type_traits/is_empty.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_empty :
        value_fixture<
            ::jaf::std_port::is_empty
        >
    {
    };

    struct empty_foo
    {
    };

    struct foo
    {
        int n;
    };

    struct static_foo
    {
        static int n;
    };

    struct virtual_foo
    {
        virtual ~virtual_foo();
    };

    TEST_F(is_empty, empty)
    {
        validate<true, empty_foo>();
    }

    TEST_F(is_empty, simple)
    {
        validate<false, foo>();
    }

    TEST_F(is_empty, static)
    {
        validate<true, static_foo>();
    }

    TEST_F(is_empty, virtual)
    {
        validate<false, virtual_foo>();
    }
}
