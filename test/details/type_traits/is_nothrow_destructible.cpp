#include <std_port/details/type_traits/is_nothrow_destructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_destructible :
        value_fixture<
            ::jaf::std_port::is_nothrow_destructible
        >
    {
    };

    struct generated_foo
    {
    };

    struct defaulted_foo
    {
        ~defaulted_foo() = default;
    };

    struct virtual_foo
    {
        virtual ~virtual_foo() = default;
    };

    struct defined_foo
    {
        ~defined_foo()
        {
        }
    };

    struct nothrow_defined_foo
    {
        ~nothrow_defined_foo() noexcept
        {
        }
    };

    struct deleted_foo
    {
        ~deleted_foo() = delete;
    };

    TEST_F(is_nothrow_destructible, generated)
    {
        validate<true, generated_foo>();
    }

    TEST_F(is_nothrow_destructible, default)
    {
        validate<true, defaulted_foo>();
    }

    TEST_F(is_nothrow_destructible, defined)
    {
        validate<true, defined_foo>();
    }

    TEST_F(is_nothrow_destructible, nothrow_defined)
    {
        validate<true, nothrow_defined_foo>();
    }

    TEST_F(is_nothrow_destructible, virtual)
    {
        validate<true, virtual_foo>();
    }

    TEST_F(is_nothrow_destructible, deleted)
    {
        validate<false, deleted_foo>();
    }
}
