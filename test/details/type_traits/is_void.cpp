#include <std_port/details/type_traits/is_void.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_void :
        value_fixture<
            ::jaf::std_port::is_void
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_void, void)
    {
        validate<true, void>();
    }

    TEST_F(is_void, literal)
    {
        validate<false, int>();
    }

    TEST_F(is_void, class)
    {
        validate<false, foo>();
    }
}
