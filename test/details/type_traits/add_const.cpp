#include <std_port/details/type_traits/add_const.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct add_const
        : same_type_fixture<
            ::jaf::std_port::add_const,
            ::jaf::std_port::add_const_t
        >
    {
    };

    struct foo
    {
        bool is_const()
        {
            return false;
        }

        bool is_const() const
        {
            return true;
        }
    };

    TEST_F(add_const, make_const_class)
    {
        validate<const foo, foo>();
    }

    TEST_F(add_const, do_nothing_class)
    {
        validate<const foo, const foo>();
    }

    TEST_F(add_const, make_const_literal)
    {
        validate<const int, int>();
    }

    TEST_F(add_const, do_nothing_literal)
    {
        validate<const int, const int>();
    }

    TEST_F(add_const, call_const)
    {
        EXPECT_FALSE(foo{}.is_const());
        EXPECT_TRUE(typename ::jaf::std_port::add_const<foo>::type{}.is_const());
        EXPECT_TRUE(::jaf::std_port::add_const_t<foo>{}.is_const());
    }
}
