#include <std_port/details/type_traits/result_of.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct result_of :
        same_type_fixture<
            ::jaf::std_port::result_of,
            ::jaf::std_port::result_of_t
        >
    {
    };

    void f();
    int g();
    int h(char);
    void h(bool);

    struct foo
    {
        bool f();
    };

    TEST_F(result_of, global)
    {
        validate<void, decltype(&f)()>();
        validate<int, decltype(&g)()>();
    }

    TEST_F(result_of, member)
    {
        validate<bool, decltype(&foo::f)(foo)>();
    }

    TEST_F(result_of, overload)
    {
        validate<int, decltype(static_cast<int(*)(char)>(&h))(char)>();
        validate<void, decltype(static_cast<void(*)(bool)>(&h))(bool)>();
    }
}
