#include <std_port/details/type_traits/is_enum.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_enum :
        value_fixture<
            ::jaf::std_port::is_enum
        >
    {
    };

    struct foo
    {
    };

    enum class bar
    {
    };

    enum fee
    {
    };

    TEST_F(is_enum, literal)
    {
        validate<false, int>();
    }

    TEST_F(is_enum, struct)
    {
        validate<false, foo>();
    }

    TEST_F(is_enum, enum_class)
    {
        validate<true, bar>();
    }

    TEST_F(is_enum, enum)
    {
        validate<true, fee>();
    }
}
