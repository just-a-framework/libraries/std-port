#include <std_port/details/type_traits/is_const.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_const :
        value_fixture<
            ::jaf::std_port::is_const
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_const, class)
    {
        validate<true, const foo>();
        validate<false, foo>();
    }

    TEST_F(is_const, literal)
    {
        validate<true, const int>();
        validate<false, int>();
    }
}
