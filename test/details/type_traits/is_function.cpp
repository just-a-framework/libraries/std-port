#include <std_port/details/type_traits/is_function.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_function :
        value_fixture<
            ::jaf::std_port::is_function
        >
    {
    };

    struct foo
    {
    };

    void f();

    TEST_F(is_function, value)
    {
        validate<false, foo>();
    }

    TEST_F(is_function, global_function)
    {
        validate<true, decltype(f)>();
    }

    TEST_F(is_function, function_type)
    {
        validate<true, int(bool)>();
    }
}
