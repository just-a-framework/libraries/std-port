#include <std_port/details/type_traits/is_bounded_array.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_bounded_array :
        value_fixture<
            ::jaf::std_port::is_bounded_array
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_bounded_array, class_value)
    {
        validate<false, foo>();        
    }

    TEST_F(is_bounded_array, class_ptr)
    {
        validate<false, foo[]>();        
    }

    TEST_F(is_bounded_array, class_array)
    {
        validate<true, foo[9]>();        
    }

    TEST_F(is_bounded_array, literal_value)
    {
        validate<false, int>();        
    }

    TEST_F(is_bounded_array, literal_ptr)
    {
        validate<false, int[]>();        
    }

    TEST_F(is_bounded_array, literal_array)
    {
        validate<true, int[10]>();        
    }
}
