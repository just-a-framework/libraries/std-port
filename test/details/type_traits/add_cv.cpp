#include <std_port/details/type_traits/add_cv.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct add_cv :
        same_type_fixture<
            ::jaf::std_port::add_cv,
            ::jaf::std_port::add_cv_t
        >
    {
    };

    struct foo
    {
        bool is_const_volatile()
        {
            return false;
        }

        bool is_const_volatile() const volatile
        {
            return true;
        }
    };

    TEST_F(add_cv, make_const_volatile_class)
    {
        validate<const volatile foo, foo>();
    }

    TEST_F(add_cv, make_volatile_class)
    {
        validate<const volatile foo, const foo>();
    }

    TEST_F(add_cv, make_const_class)
    {
        validate<const volatile foo, volatile foo>();
    }

    TEST_F(add_cv, do_nothing_class)
    {
        validate<const volatile foo, const volatile foo>();
    }

    TEST_F(add_cv, make_const_volatile_literal)
    {
        validate<const volatile int, int>();
    }

    TEST_F(add_cv, make_volatile_literal)
    {
        validate<const volatile int, const int>();
    }

    TEST_F(add_cv, make_const_literal)
    {
        validate<const volatile int, volatile int>();
    }

    TEST_F(add_cv, do_nothing_literal)
    {
        validate<const volatile int, const volatile int>();
    }

    TEST_F(add_cv, call_const_volatile)
    {
        EXPECT_FALSE(foo{}.is_const_volatile());
        EXPECT_TRUE(typename ::jaf::std_port::add_cv<foo>::type{}.is_const_volatile());
        EXPECT_TRUE(::jaf::std_port::add_cv_t<foo>{}.is_const_volatile());
    }
}
