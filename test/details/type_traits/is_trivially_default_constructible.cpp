#include <std_port/details/type_traits/is_trivially_default_constructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_trivially_default_constructible :
        value_fixture<
            ::jaf::std_port::is_trivially_default_constructible
        >
    {
    };

    struct generated_foo
    {
    };

    struct defaulted_foo
    {
        defaulted_foo() = default;
    };

    struct defined_foo
    {
        defined_foo()
        {
        }
    };

    struct deleted_foo
    {
        deleted_foo() = delete;
    };

    struct other_foo
    {
        other_foo(int)
        {
        }
    };


    TEST_F(is_trivially_default_constructible, generated)
    {
        validate<true, generated_foo>();
    }

    TEST_F(is_trivially_default_constructible, default)
    {
        validate<true, defaulted_foo>();
    }

    TEST_F(is_trivially_default_constructible, defined)
    {
        validate<false, defined_foo>();
    }

    TEST_F(is_trivially_default_constructible, delete)
    {
        validate<false, deleted_foo>();
    }

    TEST_F(is_trivially_default_constructible, other)
    {
        validate<false, other_foo>();
    }
}
