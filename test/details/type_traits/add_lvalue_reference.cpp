#include <std_port/details/type_traits/add_lvalue_reference.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct add_lvalue_reference :
        same_type_fixture<
            ::jaf::std_port::add_lvalue_reference,
            ::jaf::std_port::add_lvalue_reference_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(add_lvalue_reference, add_lvalue_reference_class)
    {
        validate<foo&, foo>();
    }

    TEST_F(add_lvalue_reference, do_nothing_class)
    {
        validate<foo&, foo&>();
    }

    TEST_F(add_lvalue_reference, add_lvalue_reference_literal)
    {
        validate<int&, int>();
    }

    TEST_F(add_lvalue_reference, do_nothing_literal)
    {
        validate<int&, int&>();
    }
}
