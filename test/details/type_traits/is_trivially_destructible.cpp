#include <std_port/details/type_traits/is_trivially_destructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_trivially_destructible :
        value_fixture<
            ::jaf::std_port::is_trivially_destructible
        >
    {
    };

    struct generated_foo
    {
    };

    struct defaulted_foo
    {
        ~defaulted_foo() = default;
    };

    struct virtual_foo
    {
        virtual ~virtual_foo() = default;
    };

    struct defined_foo
    {
        ~defined_foo()
        {
        }
    };

    struct trivially_defined_foo
    {
        ~trivially_defined_foo() noexcept
        {
        }
    };

    struct deleted_foo
    {
        ~deleted_foo() = delete;
    };

    TEST_F(is_trivially_destructible, generated)
    {
        validate<true, generated_foo>();
    }

    TEST_F(is_trivially_destructible, default)
    {
        validate<true, defaulted_foo>();
    }

    TEST_F(is_trivially_destructible, defined)
    {
        validate<false, defined_foo>();
    }

    TEST_F(is_trivially_destructible, trivially_defined)
    {
        validate<false, trivially_defined_foo>();
    }

    TEST_F(is_trivially_destructible, virtual)
    {
        validate<false, virtual_foo>();
    }

    TEST_F(is_trivially_destructible, deleted)
    {
        validate<false, deleted_foo>();
    }
}
