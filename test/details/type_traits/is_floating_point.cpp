#include <std_port/details/type_traits/is_floating_point.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_floating_point :
        value_fixture<
            ::jaf::std_port::is_floating_point
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_floating_point, double)
    {
        validate<true, double>();
    }

    TEST_F(is_floating_point, int)
    {
        validate<false, int>();
    }

    TEST_F(is_floating_point, class)
    {
        validate<false, foo>();
    }
}
