#include <std_port/details/type_traits/is_union.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_union :
        value_fixture<
            ::jaf::std_port::is_union
        >
    {
    };

    struct foo
    {
    };

    using bar = union
    {
        bool b;
        float f;
    };

    TEST_F(is_union, literal)
    {
        validate<false, int>();
    }

    TEST_F(is_union, class)
    {
        validate<false, foo>();
    }

    TEST_F(is_union, union)
    {
        validate<true, bar>();
    }
}
