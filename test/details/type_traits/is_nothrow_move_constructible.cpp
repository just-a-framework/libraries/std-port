#include <std_port/details/type_traits/is_nothrow_move_constructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_move_constructible :
        value_fixture<
            ::jaf::std_port::is_nothrow_move_constructible
        >
    {
    };

    struct generated_foo
    {
    };

    struct defaulted_foo
    {
        defaulted_foo(defaulted_foo&&) = default;
    };

    struct defined_foo
    {
        defined_foo(defined_foo&&)
        {
        }
    };

    struct defined_nothrow_foo
    {
        defined_nothrow_foo(defined_nothrow_foo&&) noexcept
        {
        }
    };

    struct deleted_foo
    {
        deleted_foo(deleted_foo&&) = delete;
    };

    struct other_foo
    {
        other_foo(int)
        {
        }
    };


    TEST_F(is_nothrow_move_constructible, generated)
    {
        validate<true, generated_foo>();
    }

    TEST_F(is_nothrow_move_constructible, default)
    {
        validate<true, defaulted_foo>();
    }

    TEST_F(is_nothrow_move_constructible, defined)
    {
        validate<false, defined_foo>();
    }

    TEST_F(is_nothrow_move_constructible, defined_noexecpt)
    {
        validate<true, defined_nothrow_foo>();
    }

    TEST_F(is_nothrow_move_constructible, delete)
    {
        validate<false, deleted_foo>();
    }

    TEST_F(is_nothrow_move_constructible, other)
    {
        validate<true, other_foo>();
    }
}
