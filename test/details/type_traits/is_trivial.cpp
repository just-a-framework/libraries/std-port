#include <std_port/details/type_traits/is_trivial.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_trivial :
        value_fixture<
            ::jaf::std_port::is_trivial
        >
    {
    };

    struct foo
    {
        int m;
    };

    struct bar
    {
        bar()
        {
        }
    };


    TEST_F(is_trivial, trivial)
    {
        validate<true, foo>();
    }

    TEST_F(is_trivial, not_trivial)
    {
        validate<false, bar>();
    }
}
