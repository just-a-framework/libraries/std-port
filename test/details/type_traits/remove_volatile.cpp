#include <std_port/details/type_traits/remove_volatile.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct remove_volatile
        : same_type_fixture<
            ::jaf::std_port::remove_volatile,
            ::jaf::std_port::remove_volatile_t
        >
    {
    };

    struct foo
    {
        bool is_volatile()
        {
            return false;
        }

        bool is_volatile() volatile
        {
            return true;
        }
    };

    TEST_F(remove_volatile, remove_volatile_class)
    {
        validate<foo, volatile foo>();
    }

    TEST_F(remove_volatile, do_nothing_class)
    {
        validate<foo, foo>();
    }

    TEST_F(remove_volatile, remove_volatile_literal)
    {
        validate<int, volatile int>();
    }

    TEST_F(remove_volatile, do_nothing_literal)
    {
        validate<int, volatile int>();
    }

    TEST_F(remove_volatile, call_volatile)
    {
        volatile foo f{};
        EXPECT_TRUE(f.is_volatile());
        EXPECT_FALSE(typename ::jaf::std_port::remove_volatile<volatile foo>::type{}.is_volatile());
        EXPECT_FALSE(::jaf::std_port::remove_volatile_t<volatile foo>{}.is_volatile());
    }
}
