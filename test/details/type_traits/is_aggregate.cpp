#include <std_port/details/type_traits/is_aggregate.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_aggregate :
        value_fixture<
            ::jaf::std_port::is_aggregate
        >
    {
    };

    struct foo
    {
        int m;
        char n;
    };

    struct bar
    {
        bar(int, char)
        {
        }
    };

    TEST_F(is_aggregate, generated_copy_ctor)
    {
        validate<true, foo>();
    }

    TEST_F(is_aggregate, defined_copy_ctor)
    {
        validate<false, bar>();
    }
}
