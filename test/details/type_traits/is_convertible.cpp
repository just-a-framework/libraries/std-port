#include <std_port/details/type_traits/is_convertible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_convertible :
        value_fixture<
            ::jaf::std_port::is_convertible
        >
    {
    };

    struct foo
    {
    };

    struct inherit_foo : foo
    {
    };

    struct bar
    {
    };

    struct operator_bar
    {
        operator bar()
        {
            return bar{};
        }
    };

    struct any
    {
        template<class T>
        any(T&&)
        {
        }
    };

    TEST_F(is_convertible, inherit)
    {
        validate<true, inherit_foo*, foo*>();
        validate<false, foo*, inherit_foo*>();
    }

    TEST_F(is_convertible, operator)
    {
        validate<true, operator_bar, bar>();
        validate<false, bar, operator_bar>();
    }

    TEST_F(is_convertible, any)
    {
        validate<true, bar, any>();
        validate<true, foo, any>();
    }

    TEST_F(is_convertible, mixed)
    {
        validate<false, bar, foo>();
        validate<false, foo, bar>();
    }
}
