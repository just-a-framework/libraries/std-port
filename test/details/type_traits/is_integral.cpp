#include <std_port/details/type_traits/is_integral.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_integral :
        value_fixture<
            ::jaf::std_port::is_integral
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_integral, bool)
    {
        validate<true, bool>();
    }

    TEST_F(is_integral, int)
    {
        validate<true, int>();
    }

    TEST_F(is_integral, class)
    {
        validate<false, foo>();
    }
}
