#include <std_port/details/type_traits/true_type.hpp>

namespace jaf::std_port::test
{
    struct true_type : ::testing::Test
    {
    };

    TEST_F(true_type, is_true)
    {
        const auto t = ::jaf::std_port::true_type{};

        static_assert(::jaf::std_port::true_type::value);
        EXPECT_TRUE(::jaf::std_port::true_type::value);
        EXPECT_TRUE(static_cast<bool>(t));
        EXPECT_TRUE(t());
    }
}
