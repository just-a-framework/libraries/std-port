#include <std_port/details/type_traits/disjunction.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct disjunction :
        value_fixture<
            ::jaf::std_port::disjunction
        >
    {
    };

    template<bool B>
    struct foo : std::bool_constant<B>
    {
    };

    TEST_F(disjunction, empty)
    {
        validate<false>();
    }

    TEST_F(disjunction, one)
    {
        validate<true, foo<true>>();
    }

    TEST_F(disjunction, first)
    {
        validate<true, foo<true>, foo<false>, foo<false>>();
    }

    TEST_F(disjunction, middle)
    {
        validate<true, foo<false>, foo<true>, foo<false>>();
    }

    TEST_F(disjunction, end)
    {
        validate<true, foo<false>, foo<false>, foo<true>>();
    }

    TEST_F(disjunction, none)
    {
        validate<false, foo<false>, foo<false>, foo<false>>();
    }
}
