#include <std_port/details/type_traits/has_unique_object_representations.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct has_unique_object_representations :
        value_fixture<
            ::jaf::std_port::has_unique_object_representations
        >
    {
    };

    struct foo
    {
        int a;
    };
    
    struct bar
    {
        int a;
        float b;
    };

    TEST_F(has_unique_object_representations, test1)
    {
        validate<true, foo>();
    }

    TEST_F(has_unique_object_representations, test2)
    {
        validate<false, bar>();
    }
}
