#include <std_port/details/type_traits/is_pod.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_pod :
        value_fixture<
            ::jaf::std_port::is_pod
        >
    {
    };

    struct foo
    {
    };

    struct multiple_foo
    {
        int m;
        int n;
    };

    struct private_foo
    {
        int m;
    private:
        int n;
    };

    struct virtual_foo
    {
        virtual void f();
    };

    TEST_F(is_pod, simple)
    {
        validate<true, foo>();
    }

    TEST_F(is_pod, multiple_member)
    {
        validate<true, multiple_foo>();
    }

    TEST_F(is_pod, private_member)
    {
        validate<false, private_foo>();
    }

    TEST_F(is_pod, virtual_function)
    {
        validate<false, virtual_foo>();
    }
}
