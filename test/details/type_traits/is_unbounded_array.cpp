#include <std_port/details/type_traits/is_unbounded_array.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_unbounded_array :
        value_fixture<
            ::jaf::std_port::is_unbounded_array
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_unbounded_array, class)
    {
        validate<false, foo>();        
    }

    TEST_F(is_unbounded_array, class_ptr)
    {
        validate<true, foo[]>();        
    }

    TEST_F(is_unbounded_array, class_array)
    {
        validate<false, foo[9]>();        
    }

    TEST_F(is_unbounded_array, literal)
    {
        validate<false, int>();        
    }

    TEST_F(is_unbounded_array, literal_ptr)
    {
        validate<true, int[]>();        
    }

    TEST_F(is_unbounded_array, literal_array)
    {
        validate<false, int[10]>();        
    }
}
