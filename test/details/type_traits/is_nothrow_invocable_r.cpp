#include <std_port/details/type_traits/is_nothrow_invocable_r.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_invocable_r :
        value_fixture<
            ::jaf::std_port::is_nothrow_invocable_r
        >
    {
    };

    int f(bool);

    struct foo
    {
    };

    int g(foo&) noexcept;
    
    foo g(int);

    TEST_F(is_nothrow_invocable_r, callable)
    {
        validate<true, int, decltype(static_cast<int(*)(foo&) noexcept>(&g)), foo&>();

        validate<false, int, decltype(&f), bool>();
        validate<false, foo, decltype(static_cast<foo(*)(int)>(&g)), int>();
    }

    TEST_F(is_nothrow_invocable_r, ignore_void_ret)
    {
        validate<false, void, decltype(&f), bool>();
        validate<true, void, decltype(static_cast<int(*)(foo&) noexcept>(&g)), foo&>();
    }

    TEST_F(is_nothrow_invocable_r, wrong_ret)
    {
        validate<false, foo, decltype(&f), bool>();

        validate<false, foo, decltype(static_cast<int(*)(foo&) noexcept>(&g)), foo&>();
    }

    TEST_F(is_nothrow_invocable_r, wrong_number_args)
    {
        validate<false, int, decltype(&f)>();
        validate<false, int, decltype(&f), bool, foo>();

        validate<false, void, decltype(&f)>();
        validate<false, void, decltype(&f), bool, foo>();
    }

    TEST_F(is_nothrow_invocable_r, wrong_arg_type)
    {
        validate<false, int, decltype(&f), foo>();
        
        validate<false, void, decltype(&f), foo>();
    }
}
