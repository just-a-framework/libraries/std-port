#include <std_port/details/type_traits/integral_constant.hpp>

namespace jaf::std_port::test
{
    struct integral_constant : ::testing::Test
    {
    };

    TEST_F(integral_constant, integer)
    {
        using two_t = ::jaf::std_port::integral_constant<int, 2>;
        const auto two = two_t{};

        static_assert(two_t::value == 2);
        EXPECT_EQ(two_t::value, 2);
        EXPECT_EQ(static_cast<int>(two), 2);
        EXPECT_EQ(two(), 2);
    }

    enum class dummy
    {
        one,
        two
    };

    TEST_F(integral_constant, enum)
    {
        using dummy_t = ::jaf::std_port::integral_constant<dummy, dummy::two>;
        const auto d = dummy_t{};

        static_assert(dummy_t::value == dummy::two);
        EXPECT_EQ(dummy_t::value, dummy::two);
        EXPECT_EQ(static_cast<dummy_t>(d), dummy::two);
        EXPECT_EQ(d(), dummy::two);
    }

    TEST_F(integral_constant, different_types)
    {
        using int1_t = ::jaf::std_port::integral_constant<int, 0>;
        using int2_t = ::jaf::std_port::integral_constant<int, 100>;

        static_assert(!std::is_same_v<int1_t, int2_t>);
        EXPECT_FALSE((std::is_same_v<int1_t, int2_t>));
    }
}
