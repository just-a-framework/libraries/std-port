#include <std_port/details/type_traits/rank.hpp>

namespace jaf::std_port::test
{
    struct rank : ::testing::Test
    {
    };

    struct foo
    {
    };

    template<class T, size_t N>
    void test_type()
    {
        static_assert(::jaf::std_port::rank<T>::value == N);
        EXPECT_TRUE((::jaf::std_port::rank<T>::value == N));

        static_assert(::jaf::std_port::rank_v<T> == N);
        EXPECT_TRUE((::jaf::std_port::rank_v<T> == N));
    }

    TEST_F(rank, int)
    {
        test_type<int, 0>();
    }

    TEST_F(rank, bounded_int)
    {
        test_type<int[6], 1>();
    }

    TEST_F(rank, unbounded_int)
    {
        test_type<int[], 1>();
    }

    TEST_F(rank, mixed_int)
    {
        test_type<int[][10][2], 3>();
    }

    TEST_F(rank, foo)
    {
        test_type<foo, 0>();
    }

    TEST_F(rank, bounded_foo)
    {
        test_type<foo[6], 1>();
    }

    TEST_F(rank, unbounded_foo)
    {
        test_type<foo[], 1>();
    }

    TEST_F(rank, mixed_foo)
    {
        test_type<foo[][10][2], 3>();
    }
}
