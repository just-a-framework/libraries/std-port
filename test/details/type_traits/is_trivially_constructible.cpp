#include <std_port/details/type_traits/is_trivially_constructible.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_trivially_constructible :
        value_fixture<
            ::jaf::std_port::is_trivially_constructible
        >
    {
    };

    struct bar
    {
    };

    enum class fee
    {
    };

    struct foo
    {
        foo() = default;

        foo(const foo&) = default;

        foo(bar)
        {
        }
    };


    TEST_F(is_trivially_constructible, constructible)
    {
        validate<true, foo>();
        validate<true, foo, const foo&>();
    }

    TEST_F(is_trivially_constructible, not_constructible)
    {
        validate<false, foo, bar>();
        validate<false, foo, bar, bar>();
        validate<false, foo, bool>();
        validate<false, foo, char, fee>();
    }
}
