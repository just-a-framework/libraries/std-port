#include <std_port/details/type_traits/is_class.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_class :
        value_fixture<
            ::jaf::std_port::is_class
        >
    {
    };

    struct foo
    {
    };

    class bar
    {
    };

    enum class fee
    {
    };

    TEST_F(is_class, literal)
    {
        validate<false, int>();
    }

    TEST_F(is_class, struct)
    {
        validate<true, foo>();
    }

    TEST_F(is_class, class)
    {
        validate<true, bar>();
    }

    TEST_F(is_class, enum_class)
    {
        validate<false, fee>();
    }
}
