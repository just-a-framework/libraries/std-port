#include <std_port/details/type_traits/decay.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct decay :
        same_type_fixture<
            ::jaf::std_port::decay,
            ::jaf::std_port::decay_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(decay, class)
    {
        validate<foo, foo>();
    }

    TEST_F(decay, reference)
    {
        validate<foo, foo&>();
        validate<foo, foo&&>();
        validate<foo, const foo&>();
    }

    TEST_F(decay, function)
    {
        validate<foo*, foo[4]>();
        validate<foo*, foo[]>();
    }

    TEST_F(decay, array)
    {
        validate<int(*)(bool), int(bool)>();
    }
}
