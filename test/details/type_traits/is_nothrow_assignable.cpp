#include <std_port/details/type_traits/is_nothrow_assignable.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_nothrow_assignable :
        value_fixture<
            ::jaf::std_port::is_nothrow_assignable
        >
    {
    };

    enum class fee
    {
    };

    struct foo
    {
        foo& operator=(const foo&);
        foo& operator=(fee) noexcept;
    };

    struct bar
    {
        bar& operator=(fee);
    };


    TEST_F(is_nothrow_assignable, assignable)
    {
        validate<true, bar, const bar&>();
        validate<true, foo, fee>();
    }

    TEST_F(is_nothrow_assignable, not_assignable)
    {
        validate<false, bar, fee>();
        validate<false, foo, bool>();
        validate<false, foo, bar>();
    }
}
