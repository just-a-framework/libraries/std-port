#include <std_port/details/type_traits/is_unsigned.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_unsigned :
        value_fixture<
            ::jaf::std_port::is_unsigned
        >
    {
    };

    struct foo
    {
    };


    TEST_F(is_unsigned, int)
    {
        validate<false, int>();
        validate<false, signed int>();
        validate<true, unsigned int>();
    }

    TEST_F(is_unsigned, float)
    {
        validate<false, float>();
    }

    TEST_F(is_unsigned, class)
    {
        validate<false, foo>();
    }
}
