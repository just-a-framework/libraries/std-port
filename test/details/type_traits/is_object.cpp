#include <std_port/details/type_traits/is_object.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_object :
        value_fixture<
            ::jaf::std_port::is_object
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_object, class_value)
    {
        validate<true, foo>();
    }

    TEST_F(is_object, class_reference)
    {
        validate<false, foo&>();
    }

    TEST_F(is_object, class_pointer)
    {
        validate<true, foo*>();
    }

    TEST_F(is_object, int_value)
    {
        validate<true, int>();
    }

    TEST_F(is_object, const_int_value)
    {
        validate<true, const int>();
    }

    TEST_F(is_object, int_reference)
    {
        validate<false, int&>();
    }

    TEST_F(is_object, float_value)
    {
        validate<true, float>();
    }

    TEST_F(is_object, const_float_value)
    {
        validate<true, const float>();
    }

    TEST_F(is_object, float_reference)
    {
        validate<false, float&>();
    }

    TEST_F(is_object, void)
    {
        validate<false, void>();
    }

    TEST_F(is_object, null_pointer)
    {
        validate<true, nullptr_t>();
    }
}
