#pragma once

namespace jaf::std_port::test
{
    template<template<typename...> typename Type/*, template<typename...> typename Alias*/>
    struct value_fixture : ::testing::Test
    {
        template<bool B, class... T>
        void validate()
        {
            if constexpr(B)
            {
                validate_true<T...>();
            }
            else
            {
                validate_false<T...>();
            }
        }

    private:
        template<class... T>
        void validate_true()
        {
            static_assert(Type<T...>::value);
            EXPECT_TRUE((Type<T...>::value));

            //static_assert(Alias<T...>);
            //EXPECT_TRUE((Alias<T...>));
            
            const auto f = Type<T...>{};
            EXPECT_TRUE(static_cast<bool>(f));
            EXPECT_TRUE(f());
        }

        template<class... T>
        void validate_false()
        {
            static_assert(!Type<T...>::value);
            EXPECT_FALSE((Type<T...>::value));

            //static_assert(!Alias<T...>);
            //EXPECT_FALSE((Alias<T...>));

            const auto f = Type<T...>{};
            EXPECT_FALSE(static_cast<bool>(f));
            EXPECT_FALSE(f());
        }
    };
}
