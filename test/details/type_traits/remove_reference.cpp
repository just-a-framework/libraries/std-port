#include <std_port/details/type_traits/remove_reference.hpp>

#include <details/type_traits/same_type_fixture.hpp>

namespace jaf::std_port::test
{
    struct remove_reference
        : same_type_fixture<
            ::jaf::std_port::remove_reference,
            ::jaf::std_port::remove_reference_t
        >
    {
    };

    struct foo
    {
    };

    TEST_F(remove_reference, remove_lvalue_reference_class)
    {
        validate<foo, foo&>();
    }

    TEST_F(remove_reference, remove_rvalue_reference_class)
    {
        validate<foo, foo&&>();
    }

    TEST_F(remove_reference, do_nothing_class)
    {
        validate<foo, foo>();
    }

    TEST_F(remove_reference, remove_lvalue_reference_literal)
    {
        validate<int, int&>();
    }

    TEST_F(remove_reference, remove_rvalue_reference_literal)
    {
        validate<int, int&&>();
    }

    TEST_F(remove_reference, do_nothing_literal)
    {
        validate<int, int>();
    }
}
