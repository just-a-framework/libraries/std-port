#include <std_port/details/type_traits/negation.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct negation :
        value_fixture<
            ::jaf::std_port::negation
        >
    {
    };

    template<bool B>
    struct foo
    {
        static constexpr bool value = B;
    };

    TEST_F(negation, true)
    {
        validate<false, foo<true>>();
    }

    TEST_F(negation, false)
    {
        validate<true, foo<false>>();
    }
}
