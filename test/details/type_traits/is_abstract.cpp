#include <std_port/details/type_traits/is_abstract.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_abstract :
        value_fixture<
            ::jaf::std_port::is_abstract
        >
    {
    };

    struct foo
    {
        int m;
    };

    struct virtual_foo
    {
        virtual void f();
    };

    struct pure_foo
    {
        virtual void f() = 0;
    };

    struct inherit_foo : pure_foo
    {
    };

    TEST_F(is_abstract, simple)
    {
        validate<false, foo>();
    }

    TEST_F(is_abstract, virtual)
    {
        validate<false, virtual_foo>();
    }

    TEST_F(is_abstract, pure)
    {
        validate<true, pure_foo>();
    }

    TEST_F(is_abstract, inherit)
    {
        validate<true, inherit_foo>();
    }
}
