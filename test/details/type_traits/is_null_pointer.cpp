#include <std_port/details/type_traits/is_null_pointer.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct is_null_pointer :
        value_fixture<
            ::jaf::std_port::is_null_pointer
        >
    {
    };

    struct foo
    {
    };

    TEST_F(is_null_pointer, nullptr_t)
    {
        validate<true, nullptr_t>();
    }

    TEST_F(is_null_pointer, literal)
    {
        validate<false, int>();
    }

    TEST_F(is_null_pointer, class)
    {
        validate<false, foo>();
    }
}
