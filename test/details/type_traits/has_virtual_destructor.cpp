#include <std_port/details/type_traits/has_virtual_destructor.hpp>

#include <details/type_traits/value_fixture.hpp>

namespace jaf::std_port::test
{
    struct has_virtual_destructor :
        value_fixture<
            ::jaf::std_port::has_virtual_destructor
        >
    {
    };

    struct generated_foo
    {
    };

    struct default_foo
    {
        ~default_foo() = default;
    };

    struct default_virtual_foo
    {
        virtual ~default_virtual_foo() = default;
    };

    struct defined_foo
    {
        ~defined_foo()
        {
        }
    };

    struct defined_virtual_foo
    {
        virtual ~defined_virtual_foo()
        {
        }
    };

    TEST_F(has_virtual_destructor, generated)
    {
        validate<false, generated_foo>();
    }

    TEST_F(has_virtual_destructor, default)
    {
        validate<false, default_foo>();
    }

    TEST_F(has_virtual_destructor, default_virtual)
    {
        validate<true, default_virtual_foo>();
    }

    TEST_F(has_virtual_destructor, defined)
    {
        validate<false, defined_foo>();
    }

    TEST_F(has_virtual_destructor, defined_virtual)
    {
        validate<true, defined_virtual_foo>();
    }
}
