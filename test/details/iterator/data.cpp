#include <std_port/details/iterator/data.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct data
        : ::testing::Test
    {
    };

    TEST_F(data, vector)
    {
        using ::testing::Return;

        mock::vector<int> v;

        int i = 6;;

        EXPECT_CALL(v, data())
            .Times(1)
            .WillRepeatedly(Return(&i));
        
        EXPECT_EQ(&i, ::jaf::std_port::data(v));
    }

    TEST_F(data, array)
    {
        int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
                
        EXPECT_EQ(a, ::jaf::std_port::data(a));
    }
}
