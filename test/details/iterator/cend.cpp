#include <std_port/details/iterator/cend.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct cend
        : ::testing::Test
    {
    };

    TEST_F(cend, vector)
    {
        using ::testing::Return;

        const mock::vector<int> v;
        auto it = mock::vector<int>::const_iterator{10};

        EXPECT_CALL(v, end())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::cend(v));
    }

    TEST_F(cend, array)
    {
        int a[10];
        
        EXPECT_EQ(a + 10, ::jaf::std_port::cend(a));
    }
}
