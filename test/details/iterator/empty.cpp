#include <std_port/details/iterator/empty.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct empty
        : ::testing::Test
    {
    };

    TEST_F(empty, vector)
    {
        using ::testing::Return;

        mock::vector<int> v;

        EXPECT_CALL(v, empty())
            .Times(1)
            .WillRepeatedly(Return(false));
        
        EXPECT_EQ(false, ::jaf::std_port::empty(v));

        EXPECT_CALL(v, empty())
            .Times(1)
            .WillRepeatedly(Return(true));
        
        EXPECT_EQ(true, ::jaf::std_port::empty(v));
    }

    TEST_F(empty, array)
    {
        int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
                
        EXPECT_EQ(false, ::jaf::std_port::empty(a));
    }
}
