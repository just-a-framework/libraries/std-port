#include <std_port/details/iterator/size.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct size
        : ::testing::Test
    {
    };

    TEST_F(size, vector)
    {
        using ::testing::Return;

        mock::vector<int> v;

        EXPECT_CALL(v, size())
            .Times(1)
            .WillRepeatedly(Return(5));
        
        EXPECT_EQ(5, ::jaf::std_port::size(v));
    }

    TEST_F(size, array)
    {
        int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
                
        EXPECT_EQ(10, ::jaf::std_port::size(a));
    }
}
