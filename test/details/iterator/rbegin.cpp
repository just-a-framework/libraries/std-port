#include <std_port/details/iterator/rbegin.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct rbegin
        : ::testing::Test
    {
    };

    TEST_F(rbegin, vector)
    {
        using ::testing::Return;

        mock::vector<int> v;
        auto it = mock::vector<int>::iterator{10};

        EXPECT_CALL(v, rbegin())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::rbegin(v));
    }

    TEST_F(rbegin, const_vector)
    {
        using ::testing::Return;

        const mock::vector<int> v;
        auto it = mock::vector<int>::const_iterator{13};

        EXPECT_CALL(v, rbegin())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::rbegin(v));
    }

    TEST_F(rbegin, array)
    {
        int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
                
        EXPECT_EQ((a + 10), ::jaf::std_port::rbegin(a).base());
    }
}
