#include <std_port/details/iterator/crend.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct crend
        : ::testing::Test
    {
    };

    TEST_F(crend, vector)
    {
        using ::testing::Return;

        const mock::vector<int> v;
        auto it = mock::vector<int>::const_iterator{10};

        EXPECT_CALL(v, rend())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::crend(v));
    }

    TEST_F(crend, array)
    {
        int a[10];
        
        EXPECT_EQ(a, ::jaf::std_port::crend(a).base());
    }
}
