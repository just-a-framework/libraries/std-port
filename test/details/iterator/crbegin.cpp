#include <std_port/details/iterator/crbegin.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct crbegin
        : ::testing::Test
    {
    };

    TEST_F(crbegin, vector)
    {
        using ::testing::Return;

        const mock::vector<int> v;
        auto it = mock::vector<int>::const_iterator{10};

        EXPECT_CALL(v, rbegin())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::crbegin(v));
    }

    TEST_F(crbegin, array)
    {
        int a[10];
        
        EXPECT_EQ(a + 10, ::jaf::std_port::crbegin(a).base());
    }
}
