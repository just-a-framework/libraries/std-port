#include <std_port/details/iterator/begin.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct begin
        : ::testing::Test
    {
    };

    TEST_F(begin, vector)
    {
        using ::testing::Return;

        mock::vector<int> v;
        auto it = mock::vector<int>::iterator{10};

        EXPECT_CALL(v, begin())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::begin(v));
    }

    TEST_F(begin, const_vector)
    {
        using ::testing::Return;

        const mock::vector<int> v;
        auto it = mock::vector<int>::const_iterator{13};

        EXPECT_CALL(v, begin())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::begin(v));
    }

    TEST_F(begin, array)
    {
        int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
                
        EXPECT_EQ(a, ::jaf::std_port::begin(a));
    }
}
