namespace jaf::std_port::mock
{
    template<class T>
    struct vector
    {
        struct iterator
        {
            int tag = 0;

            friend bool operator==(const iterator& a, const iterator& b)
            {
                return a.tag == b.tag;
            }
        };

        struct const_iterator
        {
            int tag = 0;

            friend bool operator==(const const_iterator& a, const const_iterator& b)
            {
                return a.tag == b.tag;
            }
        };

        MOCK_METHOD(iterator, begin, (), ());
        MOCK_METHOD(const_iterator, begin, (), (const));

        MOCK_METHOD(iterator, end, (), ());
        MOCK_METHOD(const_iterator, end, (), (const));
        
        MOCK_METHOD(iterator, rbegin, (), ());
        MOCK_METHOD(const_iterator, rbegin, (), (const));

        MOCK_METHOD(iterator, rend, (), ());
        MOCK_METHOD(const_iterator, rend, (), (const));

        MOCK_METHOD(T*, data, (), (const));
        MOCK_METHOD(size_t, size, (), (const));
        MOCK_METHOD(bool, empty, (), (const));
    };
}
