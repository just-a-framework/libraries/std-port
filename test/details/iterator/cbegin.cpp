#include <std_port/details/iterator/cbegin.hpp>

#include <details/iterator/vector.hpp>

namespace jaf::std_port::test
{
    struct cbegin
        : ::testing::Test
    {
    };

    TEST_F(cbegin, vector)
    {
        using ::testing::Return;

        const mock::vector<int> v;
        auto it = mock::vector<int>::const_iterator{10};

        EXPECT_CALL(v, begin())
            .Times(1)
            .WillRepeatedly(Return(it));
        
        EXPECT_EQ(it, ::jaf::std_port::cbegin(v));
    }

    TEST_F(cbegin, array)
    {
        int a[10];
        
        EXPECT_EQ(a, ::jaf::std_port::cbegin(a));
    }
}
