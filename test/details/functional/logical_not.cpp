#include <std_port/details/functional/logical_not.hpp>

namespace jaf::std_port::test
{
    struct logical_not
        : ::testing::Test
    {
    };

    TEST_F(logical_not, bool)
    {
        auto p = ::jaf::std_port::logical_not<bool>{};

        EXPECT_EQ(p(false), true);
        EXPECT_EQ(p(true), false);
    }

    TEST_F(logical_not, void)
    {
        auto p = ::jaf::std_port::logical_not<>{};

        EXPECT_EQ(p(false), true);
        EXPECT_EQ(p(true), false);
    }
}
