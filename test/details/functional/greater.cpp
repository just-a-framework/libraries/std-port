#include <std_port/details/functional/greater.hpp>

namespace jaf::std_port::test
{
    struct greater
        : ::testing::Test
    {
    };

    TEST_F(greater, int)
    {
        auto p = ::jaf::std_port::greater<int>{};

        EXPECT_EQ(p(2, 2), false);
        EXPECT_EQ(p(2, 3), false);
        EXPECT_EQ(p(6, 2), true);
    }

    TEST_F(greater, void)
    {
        auto p = ::jaf::std_port::greater<>{};

        EXPECT_EQ(p(2, 2), false);
        EXPECT_EQ(p(2, 3), false);
        EXPECT_EQ(p(6, 2), true);
    }
}
