#include <std_port/details/functional/logical_or.hpp>

namespace jaf::std_port::test
{
    struct logical_or
        : ::testing::Test
    {
    };

    TEST_F(logical_or, bool)
    {
        auto p = ::jaf::std_port::logical_or<bool>{};

        EXPECT_EQ(p(true, false), true);
        EXPECT_EQ(p(false, true), true);
        EXPECT_EQ(p(true, true), true);
        EXPECT_EQ(p(false, false), false);
    }

    TEST_F(logical_or, void)
    {
        auto p = ::jaf::std_port::logical_or<>{};

        EXPECT_EQ(p(true, false), true);
        EXPECT_EQ(p(false, true), true);
        EXPECT_EQ(p(true, true), true);
        EXPECT_EQ(p(false, false), false);
    }
}
