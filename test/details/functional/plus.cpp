#include <std_port/details/functional/plus.hpp>

namespace jaf::std_port::test
{
    struct plus
        : ::testing::Test
    {
    };

    TEST_F(plus, int)
    {
        auto p = ::jaf::std_port::plus<int>{};

        EXPECT_EQ(p(1, 2), 3);
    }

    TEST_F(plus, void)
    {
        auto p = ::jaf::std_port::plus<>{};

        EXPECT_EQ(p(1, 2), 3);
    }
}
