#include <std_port/details/functional/not_equal_to.hpp>

namespace jaf::std_port::test
{
    struct not_equal_to
        : ::testing::Test
    {
    };

    TEST_F(not_equal_to, int)
    {
        auto p = ::jaf::std_port::not_equal_to<int>{};

        EXPECT_EQ(p(2, 2), false);
        EXPECT_EQ(p(6, 2), true);
    }

    TEST_F(not_equal_to, void)
    {
        auto p = ::jaf::std_port::not_equal_to<>{};

        EXPECT_EQ(p(2, 2), false);
        EXPECT_EQ(p(6, 2), true);
    }
}
