#include <std_port/details/functional/bit_and.hpp>

namespace jaf::std_port::test
{
    struct bit_and
        : ::testing::Test
    {
    };

    TEST_F(bit_and, int)
    {
        auto p = ::jaf::std_port::bit_and<int>{};

        EXPECT_EQ(p(0b01, 0b10), 0b00);
        EXPECT_EQ(p(0b11, 0b10), 0b10);
        EXPECT_EQ(p(0b10, 0b10), 0b10);
    }

    TEST_F(bit_and, void)
    {
        auto p = ::jaf::std_port::bit_and<>{};

        EXPECT_EQ(p(0b01, 0b10), 0b00);
        EXPECT_EQ(p(0b11, 0b10), 0b10);
        EXPECT_EQ(p(0b10, 0b10), 0b10);
    }
}
