#include <std_port/details/functional/less.hpp>

namespace jaf::std_port::test
{
    struct less
        : ::testing::Test
    {
    };

    TEST_F(less, int)
    {
        auto p = ::jaf::std_port::less<int>{};

        EXPECT_EQ(p(2, 2), false);
        EXPECT_EQ(p(2, 3), true);
        EXPECT_EQ(p(6, 2), false);
    }

    TEST_F(less, void)
    {
        auto p = ::jaf::std_port::less<>{};

        EXPECT_EQ(p(2, 2), false);
        EXPECT_EQ(p(2, 3), true);
        EXPECT_EQ(p(6, 2), false);
    }
}
