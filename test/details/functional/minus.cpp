#include <std_port/details/functional/minus.hpp>

namespace jaf::std_port::test
{
    struct minus
        : ::testing::Test
    {
    };

    TEST_F(minus, int)
    {
        auto p = ::jaf::std_port::minus<int>{};

        EXPECT_EQ(p(6, 2), 4);
    }

    TEST_F(minus, void)
    {
        auto p = ::jaf::std_port::minus<>{};

        EXPECT_EQ(p(6, 2), 4);
    }
}
