#include <std_port/details/functional/logical_and.hpp>

namespace jaf::std_port::test
{
    struct logical_and
        : ::testing::Test
    {
    };

    TEST_F(logical_and, bool)
    {
        auto p = ::jaf::std_port::logical_and<bool>{};

        EXPECT_EQ(p(true, false), false);
        EXPECT_EQ(p(false, true), false);
        EXPECT_EQ(p(true, true), true);
        EXPECT_EQ(p(false, false), false);
    }

    TEST_F(logical_and, void)
    {
        auto p = ::jaf::std_port::logical_and<>{};

        EXPECT_EQ(p(true, false), false);
        EXPECT_EQ(p(false, true), false);
        EXPECT_EQ(p(true, true), true);
        EXPECT_EQ(p(false, false), false);
    }
}
