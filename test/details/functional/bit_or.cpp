#include <std_port/details/functional/bit_or.hpp>

namespace jaf::std_port::test
{
    struct bit_or
        : ::testing::Test
    {
    };

    TEST_F(bit_or, int)
    {
        auto p = ::jaf::std_port::bit_or<int>{};

        EXPECT_EQ(p(0b01, 0b10), 0b11);
        EXPECT_EQ(p(0b11, 0b10), 0b11);
        EXPECT_EQ(p(0b10, 0b10), 0b10);
    }

    TEST_F(bit_or, void)
    {
        auto p = ::jaf::std_port::bit_or<>{};

        EXPECT_EQ(p(0b01, 0b10), 0b11);
        EXPECT_EQ(p(0b11, 0b10), 0b11);
        EXPECT_EQ(p(0b10, 0b10), 0b10);
    }
}
