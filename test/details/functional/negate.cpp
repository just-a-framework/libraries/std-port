#include <std_port/details/functional/negate.hpp>

namespace jaf::std_port::test
{
    struct negate
        : ::testing::Test
    {
    };

    TEST_F(negate, int)
    {
        auto p = ::jaf::std_port::negate<int>{};

        EXPECT_EQ(p(5), -5);
    }

    TEST_F(negate, void)
    {
        auto p = ::jaf::std_port::negate<>{};

        EXPECT_EQ(p(5), -5);
    }
}
