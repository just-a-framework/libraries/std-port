#include <std_port/details/functional/divides.hpp>

namespace jaf::std_port::test
{
    struct divides
        : ::testing::Test
    {
    };

    TEST_F(divides, int)
    {
        auto p = ::jaf::std_port::divides<int>{};

        EXPECT_EQ(p(6, 2), 3);
    }

    TEST_F(divides, void)
    {
        auto p = ::jaf::std_port::divides<>{};

        EXPECT_EQ(p(6, 2), 3);
    }
}
