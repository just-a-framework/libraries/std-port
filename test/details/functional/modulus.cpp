#include <std_port/details/functional/modulus.hpp>

namespace jaf::std_port::test
{
    struct modulus
        : ::testing::Test
    {
    };

    TEST_F(modulus, int)
    {
        auto p = ::jaf::std_port::modulus<int>{};

        EXPECT_EQ(p(6, 4), 2);
    }

    TEST_F(modulus, void)
    {
        auto p = ::jaf::std_port::modulus<>{};

        EXPECT_EQ(p(6, 4), 2);
    }
}
