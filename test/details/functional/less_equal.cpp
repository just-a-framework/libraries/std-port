#include <std_port/details/functional/less_equal.hpp>

namespace jaf::std_port::test
{
    struct less_equal
        : ::testing::Test
    {
    };

    TEST_F(less_equal, int)
    {
        auto p = ::jaf::std_port::less_equal<int>{};

        EXPECT_EQ(p(2, 2), true);
        EXPECT_EQ(p(2, 3), true);
        EXPECT_EQ(p(6, 2), false);
    }

    TEST_F(less_equal, void)
    {
        auto p = ::jaf::std_port::less_equal<>{};

        EXPECT_EQ(p(2, 2), true);
        EXPECT_EQ(p(2, 3), true);
        EXPECT_EQ(p(6, 2), false);
    }
}
