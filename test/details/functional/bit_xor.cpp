#include <std_port/details/functional/bit_xor.hpp>

namespace jaf::std_port::test
{
    struct bit_xor
        : ::testing::Test
    {
    };

    TEST_F(bit_xor, int)
    {
        auto p = ::jaf::std_port::bit_xor<int>{};

        EXPECT_EQ(p(0b01, 0b10), 0b11);
        EXPECT_EQ(p(0b11, 0b10), 0b01);
        EXPECT_EQ(p(0b10, 0b10), 0b00);
    }

    TEST_F(bit_xor, void)
    {
        auto p = ::jaf::std_port::bit_xor<>{};

        EXPECT_EQ(p(0b01, 0b10), 0b11);
        EXPECT_EQ(p(0b11, 0b10), 0b01);
        EXPECT_EQ(p(0b10, 0b10), 0b00);
    }
}
