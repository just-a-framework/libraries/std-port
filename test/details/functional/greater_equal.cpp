#include <std_port/details/functional/greater_equal.hpp>

namespace jaf::std_port::test
{
    struct greater_equal
        : ::testing::Test
    {
    };

    TEST_F(greater_equal, int)
    {
        auto p = ::jaf::std_port::greater_equal<int>{};

        EXPECT_EQ(p(2, 2), true);
        EXPECT_EQ(p(2, 3), false);
        EXPECT_EQ(p(6, 2), true);
    }

    TEST_F(greater_equal, void)
    {
        auto p = ::jaf::std_port::greater_equal<>{};

        EXPECT_EQ(p(2, 2), true);
        EXPECT_EQ(p(2, 3), false);
        EXPECT_EQ(p(6, 2), true);
    }
}
