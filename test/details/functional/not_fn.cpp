#include <std_port/details/functional/not_fn.hpp>

namespace jaf::std_port::test
{
    struct not_fn
        : ::testing::Test
    {
    };

    bool foo()
    {
        return true;
    }

    bool bar(int i)
    {
        return i == 9;
    }

    TEST_F(not_fn, global_noargs)
    {
        auto f = ::jaf::std_port::not_fn( &foo );

        EXPECT_FALSE(f());
    }

    TEST_F(not_fn, const_global_noargs)
    {
        const auto f = ::jaf::std_port::not_fn( &foo );
        
        EXPECT_FALSE(f());
    }

    TEST_F(not_fn, global_args)
    {
        auto f = ::jaf::std_port::not_fn( &bar );

        EXPECT_FALSE(f(9));
        EXPECT_TRUE(f(0));
    }

    TEST_F(not_fn, const_global_args)
    {
        const auto f = ::jaf::std_port::not_fn( &bar );
        
        EXPECT_FALSE(f(9));
        EXPECT_TRUE(f(0));
    }

    TEST_F(not_fn, lambda_noargs)
    {
        auto f = ::jaf::std_port::not_fn( [](){ return true; } );

        EXPECT_FALSE(f());
    }

    TEST_F(not_fn, const_lambda_noargs)
    {
        const auto f = ::jaf::std_port::not_fn( [](){ return true; } );
        
        EXPECT_FALSE(f());
    }

    TEST_F(not_fn, lambda_args)
    {
        auto f = ::jaf::std_port::not_fn( [](int a){ return a == 4; } );

        EXPECT_FALSE(f(4));
        EXPECT_TRUE(f(1));
    }

    TEST_F(not_fn, const_lambda_args)
    {
        const auto f = ::jaf::std_port::not_fn( [](int a){ return a == 4; } );
        
        EXPECT_FALSE(f(4));
        EXPECT_TRUE(f(1));
    }
}
