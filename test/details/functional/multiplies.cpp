#include <std_port/details/functional/multiplies.hpp>

namespace jaf::std_port::test
{
    struct multiplies
        : ::testing::Test
    {
    };

    TEST_F(multiplies, int)
    {
        auto p = ::jaf::std_port::multiplies<int>{};

        EXPECT_EQ(p(6, 3), 18);
    }

    TEST_F(multiplies, void)
    {
        auto p = ::jaf::std_port::multiplies<>{};

        EXPECT_EQ(p(6, 3), 18);
    }
}
