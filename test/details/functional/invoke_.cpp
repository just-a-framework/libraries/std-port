#include <std_port/details/functional/invoke_.hpp>

namespace jaf::std_port::test
{
    struct INVOKE
        : ::testing::Test
    {
    };

    struct foo
    {
        int n;

        void f(bool, char)
        {
        }

        foo() = default;
        foo(const foo&) = delete;
        foo(foo&&) = delete;
    };

    void bar(foo&, bool)
    {
    }

    TEST_F(INVOKE, first_form)
    {
        foo f;

        ::jaf::std_port::details::INVOKE(&foo::f, f, true, 'a');
    }

    TEST_F(INVOKE, third_form)
    {
        auto f = std::make_unique<foo>();

        ::jaf::std_port::details::INVOKE(&foo::f, f, true, 'a');
    }

    TEST_F(INVOKE, forth_form)
    {
        foo f;

        ::jaf::std_port::details::INVOKE(&foo::n, f);
    }

    TEST_F(INVOKE, sixth_form)
    {
        auto f = std::make_unique<foo>();

        ::jaf::std_port::details::INVOKE(&foo::n, f);
    }

    TEST_F(INVOKE, seventh_form)
    {
        foo f;

        ::jaf::std_port::details::INVOKE(&bar, f, true);
    }
}
