#include <std_port/details/memory/addressof.hpp>

namespace jaf::std_port::test
{
    struct addressof
        : ::testing::Test
    {
    };

    struct foo
    {
    };

    struct bar
    {
        bar* operator&()
        {
            return nullptr;
        }
    };

    TEST_F(addressof, int)
    {
        int k;
        
        EXPECT_EQ(&k, ::jaf::std_port::addressof(k));
    }

    TEST_F(addressof, class)
    {
        foo* k = new foo{};

        EXPECT_EQ(k, ::jaf::std_port::addressof(*k));
        EXPECT_EQ(&(*k), ::jaf::std_port::addressof(*k));

        delete k;
    }

    TEST_F(addressof, operator_defined)
    {
        bar* k = new bar{};

        EXPECT_EQ(k, ::jaf::std_port::addressof(*k));
        EXPECT_NE(&(*k), ::jaf::std_port::addressof(*k));

        delete k;
    }
}
