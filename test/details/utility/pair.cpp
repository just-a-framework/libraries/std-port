#include <std_port/details/utility/pair.hpp>

namespace jaf::std_port::test
{
    struct pair
        : ::testing::Test
    {
    };

    struct foo
    {
        explicit foo() = default;
        explicit foo(const foo&) = default;
        explicit foo(foo&&) = default;

        explicit foo(int i)
            : i_{i}
        {
        }

        foo& operator=(int i)
        {
            i_ = i;
            return *this;
        }

        foo& operator=(const foo& o)
        {
            i_ = o.i_;
            return *this;
        }

        foo& operator=(foo&& o)
        {
            i_ = std::move(o.i_);
            return *this;
        }

        int i_ = 0;
    };

    bool operator==(const foo& a, const foo& b)
    {
        return a.i_ == b.i_;
    }

    struct bar
    {
        bar() = default;
        bar(const bar&) = default;
        bar(bar&&) = default;

        bar(bool b)
            : b_{b}
        {
        }

        bar& operator=(bool b)
        {
            b_ = b;
            return *this;
        }

        bar& operator=(const bar& o)
        {
            b_ = o.b_;
            return *this;
        }

        bar& operator=(bar&& o)
        {
            b_ = std::move(o.b_);
            return *this;
        }

        bool b_ = false;
    };

    bool operator==(const bar& a, const bar& b)
    {
        return a.b_ == b.b_;
    }

    template<class T1, class T2>
    void f(const ::jaf::std_port::pair<T1, T2>)
    {
    }

    template<class T1, class T2, class = void, class... Args>
    struct tester
        : false_type
    {
    };

    template<class T1, class T2, class... Args>
    struct tester<T1, T2, decltype(f<T1, T2>({std::declval<Args>()...})), Args...>
        : true_type
    {
    };

    TEST_F(pair, explicit_ctor_1)
    {
        EXPECT_FALSE((tester<foo, bar, void>::value));

        auto p = ::jaf::std_port::pair<foo, bar>{};
        EXPECT_EQ(p.first, foo{0});
        EXPECT_EQ(p.second, bar{false});
    }

    TEST_F(pair, implicit_ctor_1)
    {
        EXPECT_TRUE((tester<bar, bar, void>::value));
        
        auto p = ::jaf::std_port::pair<bar, bar>{};
        EXPECT_EQ(p.first, bar{false});
        EXPECT_EQ(p.second, bar{false});
    }

    TEST_F(pair, explicit_ctor_2)
    {
        EXPECT_FALSE((tester<foo, bar, void, const foo&, const bar&>::value));

        auto f = foo{5};
        auto b = bar{true};

        auto p = ::jaf::std_port::pair<foo, bar>{f, b};
        EXPECT_EQ(p.first, f);
        EXPECT_EQ(p.second, b);
    }

    TEST_F(pair, implicit_ctor_2)
    {
        EXPECT_TRUE((tester<bar, bar, void, const bar&, const bar&>::value));

        auto b1 = bar{false};
        auto b2 = bar{true};

        auto p = ::jaf::std_port::pair<bar, bar>{b1, b2};
        EXPECT_EQ(p.first, b1);
        EXPECT_EQ(p.second, b2);

    }

    TEST_F(pair, explicit_ctor_3)
    {
        EXPECT_FALSE((tester<foo, bar, void, foo&&, bar&&>::value));

        auto f = foo{5};
        auto b = bar{true};

        auto p = ::jaf::std_port::pair<foo, bar>{std::move(f), std::move(b)};
        EXPECT_EQ(p.first, foo{5});
        EXPECT_EQ(p.second, bar{true});
    }

    TEST_F(pair, implicit_ctor_3)
    {
        EXPECT_TRUE((tester<bar, bar, void, bar&&, bar&&>::value));

        auto b1 = bar{false};
        auto b2 = bar{true};

        auto p = ::jaf::std_port::pair<bar, bar>{std::move(b1), std::move(b2)};
        EXPECT_EQ(p.first, bar{false});
        EXPECT_EQ(p.second, bar{true});
    }

    TEST_F(pair, explicit_ctor_4)
    {
        EXPECT_FALSE((tester<foo, bar, void, const ::jaf::std_port::pair<int, bool>&>::value));

        auto o = ::jaf::std_port::pair<int, bool>{5, true};

        auto p = ::jaf::std_port::pair<foo, bar>{o};
        EXPECT_EQ(p.first, foo{o.first});
        EXPECT_EQ(p.second, bar{o.second});
    }

    TEST_F(pair, implicit_ctor_4)
    {
        EXPECT_TRUE((tester<bar, bar, void, const ::jaf::std_port::pair<bool, bool>&>::value));

        auto o = ::jaf::std_port::pair<bool, bool>{false, true};

        auto p = ::jaf::std_port::pair<bar, bar>{o};
        EXPECT_EQ(p.first, bar{o.first});
        EXPECT_EQ(p.second, bar{o.second});
    }

    TEST_F(pair, explicit_ctor_5)
    {
        EXPECT_FALSE((tester<foo, bar, void, ::jaf::std_port::pair<int, bool>&&>::value));

        auto o = ::jaf::std_port::pair<int, bool>{5, true};

        auto p = ::jaf::std_port::pair<foo, bar>{std::move(o)};
        EXPECT_EQ(p.first, foo{5});
        EXPECT_EQ(p.second, bar{true});
    }

    TEST_F(pair, implicit_ctor_5)
    {
        EXPECT_TRUE((tester<bar, bar, void, ::jaf::std_port::pair<bool, bool>&&>::value));

        auto o = ::jaf::std_port::pair<bool, bool>{false, true};

        auto p = ::jaf::std_port::pair<bar, bar>{std::move(o)};
        EXPECT_EQ(p.first, bar{false});
        EXPECT_EQ(p.second, bar{true});
    }

    TEST_F(pair, assign_1)
    {
        auto p1 = ::jaf::std_port::pair<foo, bar>{foo{5}, bar{false}};
        auto p2 = ::jaf::std_port::pair<foo, bar>{foo{9}, bar{true}};

        p1 = p2;

        EXPECT_EQ(p1.first, p2.first);
        EXPECT_EQ(p1.second, p2.second);
    }

    TEST_F(pair, assign_2)
    {
        auto p1 = ::jaf::std_port::pair<foo, bar>{foo{5}, bar{false}};
        auto p2 = ::jaf::std_port::pair<foo, bar>{foo{9}, bar{true}};

        p1 = std::move(p2);

        EXPECT_EQ(p1.first, foo{9});
        EXPECT_EQ(p1.second, bar{true});
    }

    TEST_F(pair, assign_3)
    {
        auto p1 = ::jaf::std_port::pair<foo, bar>{foo{5}, bar{false}};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};

        p1 = p2;

        EXPECT_EQ(p1.first, foo{p2.first});
        EXPECT_EQ(p1.second, bar{p2.second});
    }

    TEST_F(pair, assign_4)
    {
        auto p1 = ::jaf::std_port::pair<foo, bar>{foo{5}, bar{false}};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};

        p1 = std::move(p2);

        EXPECT_EQ(p1.first, foo{9});
        EXPECT_EQ(p1.second, bar{true});
    }

    TEST_F(pair, member_swap)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};

        p1.swap(p2);

        EXPECT_EQ(p1.first, 9);
        EXPECT_EQ(p1.second, true);

        EXPECT_EQ(p2.first, 5);
        EXPECT_EQ(p2.second, false);
    }

    TEST_F(pair, make_pair_1)
    {
        auto p = ::jaf::std_port::make_pair(int{5}, bool{false});

        static_assert(std::is_same_v<typename decltype(p)::first_type, int>);
        static_assert(std::is_same_v<typename decltype(p)::second_type, bool>);

        EXPECT_TRUE((std::is_same_v<typename decltype(p)::first_type, int>));
        EXPECT_TRUE((std::is_same_v<typename decltype(p)::second_type, bool>));

        EXPECT_EQ(p.first, 5);
        EXPECT_EQ(p.second, false);
    }

    TEST_F(pair, DISABLED_make_pair_2)
    {
        // TODO for reference wrapper
    }

    TEST_F(pair, equals_1)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{5, false};

        EXPECT_TRUE(p1 == p2);
    }

    TEST_F(pair, equals_2)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{9, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};
        
        EXPECT_FALSE(p1 == p2);
    }

    TEST_F(pair, equals_3)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, true};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};
        
        EXPECT_FALSE(p1 == p2);
    }

    TEST_F(pair, equals_4)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};
        
        EXPECT_FALSE(p1 == p2);
    }

    TEST_F(pair, not_equals_1)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{5, false};

        EXPECT_FALSE(p1 != p2);
    }

    TEST_F(pair, not_equals_2)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{9, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};
        
        EXPECT_TRUE(p1 != p2);
    }

    TEST_F(pair, not_equals_3)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, true};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};
        
        EXPECT_TRUE(p1 != p2);
    }

    TEST_F(pair, not_equals_4)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};
        
        EXPECT_TRUE(p1 != p2);
    }

    TEST_F(pair, less_1)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 4};
        
        EXPECT_TRUE(p1 < p2);
    }

    TEST_F(pair, less_2)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 3};
        
        EXPECT_TRUE(p1 < p2);
    }

    TEST_F(pair, less_3)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 1};
        
        EXPECT_TRUE(p1 < p2);
    }

    TEST_F(pair, less_4)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 4};
        
        EXPECT_TRUE(p1 < p2);
    }

    TEST_F(pair, less_5)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 3};
        
        EXPECT_FALSE(p1 < p2);
    }

    TEST_F(pair, less_6)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 1};
        
        EXPECT_FALSE(p1 < p2);
    }

    TEST_F(pair, less_7)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 4};
        
        EXPECT_FALSE(p1 < p2);
    }

    TEST_F(pair, less_8)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 3};
        
        EXPECT_FALSE(p1 < p2);
    }

    TEST_F(pair, less_9)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 1};
        
        EXPECT_FALSE(p1 < p2);
    }

    TEST_F(pair, less_equal_1)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 4};
        
        EXPECT_TRUE(p1 <= p2);
    }

    TEST_F(pair, less_equal_2)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 3};
        
        EXPECT_TRUE(p1 <= p2);
    }

    TEST_F(pair, less_equal_3)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 1};
        
        EXPECT_TRUE(p1 <= p2);
    }

    TEST_F(pair, less_equal_4)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 4};
        
        EXPECT_TRUE(p1 <= p2);
    }

    TEST_F(pair, less_equal_5)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 3};
        
        EXPECT_TRUE(p1 <= p2);
    }

    TEST_F(pair, less_equal_6)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 1};
        
        EXPECT_FALSE(p1 <= p2);
    }

    TEST_F(pair, less_equal_7)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 4};
        
        EXPECT_FALSE(p1 <= p2);
    }

    TEST_F(pair, less_equal_8)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 3};
        
        EXPECT_FALSE(p1 <= p2);
    }

    TEST_F(pair, less_equal_9)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 1};
        
        EXPECT_FALSE(p1 <= p2);
    }

    TEST_F(pair, greater_1)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 4};
        
        EXPECT_FALSE(p1 > p2);
    }

    TEST_F(pair, greater_2)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 3};
        
        EXPECT_FALSE(p1 > p2);
    }

    TEST_F(pair, greater_3)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 1};
        
        EXPECT_FALSE(p1 > p2);
    }

    TEST_F(pair, greater_4)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 4};
        
        EXPECT_FALSE(p1 > p2);
    }

    TEST_F(pair, greater_5)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 3};
        
        EXPECT_FALSE(p1 > p2);
    }

    TEST_F(pair, greater_6)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 1};
        
        EXPECT_TRUE(p1 > p2);
    }

    TEST_F(pair, greater_7)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 4};
        
        EXPECT_TRUE(p1 > p2);
    }

    TEST_F(pair, greater_8)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 3};
        
        EXPECT_TRUE(p1 > p2);
    }

    TEST_F(pair, greater_9)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 1};
        
        EXPECT_TRUE(p1 > p2);
    }

    TEST_F(pair, greater_equal_1)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 4};
        
        EXPECT_FALSE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_2)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 3};
        
        EXPECT_FALSE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_3)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{9, 1};
        
        EXPECT_FALSE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_4)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 4};
        
        EXPECT_FALSE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_5)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 3};
        
        EXPECT_TRUE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_6)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{5, 1};
        
        EXPECT_TRUE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_7)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 4};
        
        EXPECT_TRUE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_8)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 3};
        
        EXPECT_TRUE(p1 >= p2);
    }

    TEST_F(pair, greater_equal_9)
    {
        auto p1 = ::jaf::std_port::pair<int, int>{5, 3};
        auto p2 = ::jaf::std_port::pair<int, int>{4, 1};
        
        EXPECT_TRUE(p1 >= p2);
    }

    TEST_F(pair, global_swap)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{9, true};

        ::jaf::std_port::swap(p1, p2);

        EXPECT_EQ(p1.first, 9);
        EXPECT_EQ(p1.second, true);

        EXPECT_EQ(p2.first, 5);
        EXPECT_EQ(p2.second, false);
    }

    TEST_F(pair, get_1)
    {
        auto p = ::jaf::std_port::pair<int, bool>{5, false};

        auto e1 = ::jaf::std_port::get<0>(p);
        auto e2 = ::jaf::std_port::get<1>(p);

        static_assert((std::is_same_v<decltype(e1), int>));
        static_assert((std::is_same_v<decltype(e2), bool>));

        EXPECT_TRUE((std::is_same_v<decltype(e1), int>));
        EXPECT_TRUE((std::is_same_v<decltype(e2), bool>));

        EXPECT_EQ(e1, 5);
        EXPECT_EQ(e2, false);
    }

    TEST_F(pair, get_2)
    {
        const auto p = ::jaf::std_port::pair<int, bool>{5, false};

        auto e1 = ::jaf::std_port::get<0>(p);
        auto e2 = ::jaf::std_port::get<1>(p);

        static_assert((std::is_same_v<decltype(e1), int>));
        static_assert((std::is_same_v<decltype(e2), bool>));

        EXPECT_TRUE((std::is_same_v<decltype(e1), int>));
        EXPECT_TRUE((std::is_same_v<decltype(e2), bool>));

        EXPECT_EQ(e1, 5);
        EXPECT_EQ(e2, false);
    }

    TEST_F(pair, get_3)
    {
        auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        auto p2 = ::jaf::std_port::pair<int, bool>{5, false};

        auto e1 = ::jaf::std_port::get<0>(std::move(p1));
        auto e2 = ::jaf::std_port::get<1>(std::move(p2));

        static_assert((std::is_same_v<decltype(e1), int>));
        static_assert((std::is_same_v<decltype(e2), bool>));

        EXPECT_TRUE((std::is_same_v<decltype(e1), int>));
        EXPECT_TRUE((std::is_same_v<decltype(e2), bool>));

        EXPECT_EQ(e1, 5);
        EXPECT_EQ(e2, false);
    }

    TEST_F(pair, get_4)
    {
        const auto p1 = ::jaf::std_port::pair<int, bool>{5, false};
        const auto p2 = ::jaf::std_port::pair<int, bool>{5, false};

        auto e1 = ::jaf::std_port::get<0>(std::move(p1));
        auto e2 = ::jaf::std_port::get<1>(std::move(p2));

        static_assert((std::is_same_v<decltype(e1), int>));
        static_assert((std::is_same_v<decltype(e2), bool>));

        EXPECT_TRUE((std::is_same_v<decltype(e1), int>));
        EXPECT_TRUE((std::is_same_v<decltype(e2), bool>));

        EXPECT_EQ(e1, 5);
        EXPECT_EQ(e2, false);
    }

    TEST_F(pair, tuple_size)
    {
        using pt = ::jaf::std_port::pair<int, bool>;

        static_assert(::jaf::std_port::tuple_size_v<pt> == 2);
        EXPECT_EQ(::jaf::std_port::tuple_size_v<pt>, 2);
    }

    TEST_F(pair, tuple_element_1)
    {
        using pt = ::jaf::std_port::pair<int, bool>;

        static_assert(std::is_same_v<::jaf::std_port::tuple_element_t<0, pt>, int>);
        EXPECT_TRUE((std::is_same_v<::jaf::std_port::tuple_element_t<0, pt>, int>));
    }

    TEST_F(pair, tuple_element_2)
    {
        using pt = ::jaf::std_port::pair<int, bool>;

        static_assert(std::is_same_v<::jaf::std_port::tuple_element_t<1, pt>, bool>);
        EXPECT_TRUE((std::is_same_v<::jaf::std_port::tuple_element_t<1, pt>, bool>));
    }

    TEST_F(pair, deduction)
    {
        auto p = ::jaf::std_port::pair{foo{5}, bar{true}};

        static_assert(std::is_same_v<typename decltype(p)::first_type, foo>);
        static_assert(std::is_same_v<typename decltype(p)::second_type, bar>);

        EXPECT_TRUE((std::is_same_v<typename decltype(p)::first_type, foo>));
        EXPECT_TRUE((std::is_same_v<typename decltype(p)::second_type, bar>));
    }
}
