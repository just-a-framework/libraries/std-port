from conans import ConanFile, CMake

class StdPort(ConanFile):
    name = "std_port"
    version = "0.0.0"
    description = "C++17 compatible std-port for arduino."
    url = "https://gitlab.com/just-a-framework/libraries/std-port"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "src*"
    build_requires = "cmake/3.17.3"

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure(source_folder="src")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
    
    def package_info(self):
        self.cpp_info.builddirs = ["lib/jaf/cmake"]
